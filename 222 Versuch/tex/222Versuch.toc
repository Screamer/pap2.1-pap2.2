\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{2}{}%
\contentsline {section}{\numberline {4}Durchführung}{3}{}%
\contentsline {subsection}{\numberline {4.1}Quantitative Bestimmung der Kälteleistung}{3}{}%
\contentsline {subsection}{\numberline {4.2}Betrieb als Kältemaschiene und Wärmepumpe}{4}{}%
\contentsline {subsection}{\numberline {4.3}Betrieb als Wärmekraftmaschiene}{4}{}%
\contentsline {section}{\numberline {5}Protokoll}{4}{}%
\contentsline {section}{\numberline {6}Auswertung}{9}{}%
\contentsline {subsection}{\numberline {6.1}Betrieb als Kältemaschiene}{9}{}%
\contentsline {subsection}{\numberline {6.2}Betrieb als Kältemaschiene und Wärmepumpe}{10}{}%
\contentsline {subsection}{\numberline {6.3}Betrieb als Wärmekraftmaschiene}{13}{}%
\contentsline {section}{\numberline {7}Diskussion}{16}{}%
\contentsline {section}{\numberline {8}Anhang}{18}{}%
\contentsline {subsection}{\numberline {8.1}Source Code}{18}{}%
