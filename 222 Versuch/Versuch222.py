import numpy as np
import scipy.constants as const
import matplotlib.pyplot as plt
import uncertainties as unc
import uncertainties.unumpy as unp
from scipy.optimize import curve_fit

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def linear_fit(x, a, b):
    return a*x+b


## 1. Quantitative Bestimmung der Kälteleistung
print('\n1. Quantitative Bestimmung der Kälteleistung')
U_H = unc.ufloat(5.62, 0.01)
I_H = 5 * unc.ufloat(1.09, 0.01)
P_K = U_H * I_H

f_M = unc.ufloat(320, 2) / const.minute
Q_2 = P_K / f_M

c_W = 4180
rho_W = 998.2
dT = unc.ufloat(2.5, 0.2)
V = unc.ufloat(253, 2) * const.milli * const.liter / const.minute
Q_1 = c_W * rho_W * dT * V / f_M
W_theo = Q_1 - Q_2

#Messwerte aus 2
U_M = unc.ufloat(24.1, 0.1)
I_M = unc.ufloat(2.90, 0.1)
f_M = unc.ufloat(305, 0.2) / const.minute
W_M = U_M * I_M / f_M

eta = Q_2 / W_M
eta2 = Q_2 / W_theo
eta3 = unc.ufloat(18.4, 0.1) / (unc.ufloat(21.3, 0.1) - unc.ufloat(18.4, 0.1))
## Feedback
print('Die Kälteleistung der Kältemaschiene beträgt '+str(P_K))
print('Die entzogene Wärme Q2 entspricht '+str(Q_2))
print('Die abgegebene Wärme Q1 entspricht '+str(Q_1))
print('Das ergibt eine theoretische Energie W von '+str(W_theo))
print('Die tatsächliche Energie W beträgt '+str(W_M))
print('Das ergibt eine Abweichung von '+str(calc_sigma(W_M, W_theo)))
print('eta ist '+str(eta))
print('eta_theo ist '+str(eta2))
print('eta aus dT ist '+str(eta3))

# 2. Betrieb als Kältemaschiene und Wärmepumpe
print('\n2. Betrieb als Kältemaschiene und Wärmepumpe')
m = unc.ufloat(1, 0.05) * const.gram
Q_1 = 335 / const.gram * m
t = unc.ufloat(319, 1) - unc.ufloat(199, 1)
P_K = Q_1 / t

##Feedback
print('Die Kälteleistung der Kältemaschiene beträgt '+str(P_K))

# 3. Betrieb als Wärmekraftmaschiene
print('\n3. Betrieb als Wärmekraftmaschiene')
f = unc.ufloat(312, 2) / const.minute
U_H = unc.ufloat(13.02, 0.01)
I_H = unc.ufloat(2.08, 0.13) * 5
V = unc.ufloat(269, 2) * const.gram * const.liter / const.minute
T_zu = unc.ufloat(18.3, 0.1)
T_ab = unc.ufloat(24, 0.1)
Q_pV = unc.ufloat(np.mean(np.array([27510, 27760, 27850])) * 1e-4, np.std(np.array([27510, 27760, 27850])) * 1e-4)

P_el = U_H * I_H
Q_el = P_el / f
P_ab = c_W * rho_W * (T_ab - T_zu) * V
Q_ab = P_ab / f
P_pV = Q_pV * f
eta = Q_pV / Q_el
Q_V = Q_el - Q_ab - Q_pV
F = unp.uarray([0.8, 0.58, 0.4, 0.22], [0.03, 0.01, 0.01, 0.01])
W_pV = np.array([[35030, 34190, 34380], [33190, 31960, 32440], [31330, 30840, 30930], [29790, 29330, 29590]]) * 1e-4
W_pV = unp.uarray(np.mean(W_pV, axis=1), np.std(W_pV, axis=1))
f = unp.uarray([218, 247, 278, 313], [2, 2, 2, 2]) / const.minute
I_H = unp.uarray([2.85, 2.85, 2.85, 2.86], 0.01) * 5
U_H = unp.uarray([13.03, 13, 12.97, 13], 0.01)
Q_el = I_H * U_H / f
eta_th = W_pV / Q_el
W_D = 2 * const.pi * F * 0.25
eta_eff = W_D / Q_el

popt, pcov = curve_fit(linear_fit, unp.nominal_values(f), unp.nominal_values(eta_th), sigma=unp.std_devs(eta_th))
popt2, pcov2 = curve_fit(linear_fit, unp.nominal_values(f), unp.nominal_values(eta_eff), sigma=unp.std_devs(eta_eff))

##Feedback
print('P_el ist '+str(P_el))
print('Q_el ist '+str(Q_el))
print('P_ab ist '+str(P_ab))
print('Q_ab ist '+str(Q_ab))
print('P_pV ist '+str(P_pV))
print('Q_pV ist '+str(Q_pV))
print('eta ist '+str(eta))
print('W_D ist '+str(W_D))
print('W_pV ist '+str(W_pV))
print('Q_V ist '+str(Q_V))
print('eta_th ist '+str(eta_th))
print('eta_eff ist '+str(eta_eff))

plt.clf()
plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2
plt.grid(ls='dotted')
plt.title('Betrieb als Wärmekraftmaschiene', size=20)
plt.xlabel('Frequenz [Hz]', size=18)
plt.ylabel('Wirkungsgrad')
plt.errorbar(unp.nominal_values(f), unp.nominal_values(eta_th), xerr=unp.std_devs(f), yerr=unp.std_devs(eta_th), color='darkred', fmt='none', marker='.', label=r'$\eta_{th}$')
plt.errorbar(unp.nominal_values(f), unp.nominal_values(eta_eff), xerr=unp.std_devs(f), yerr=unp.std_devs(eta_eff), color='darkblue', fmt='none', marker='.', label=r'$\eta_{eff}$')
plt.plot(unp.nominal_values(f), linear_fit(unp.nominal_values(f), *popt), color='black', marker=None, ls='--', label=r'Ausgleichsgerade $\eta_{th}$')
plt.plot(unp.nominal_values(f), linear_fit(unp.nominal_values(f), *popt2), color='green', marker=None, ls='--', label=r'Ausgleichsgerade $\eta_{eff}$')
plt.tight_layout()
plt.legend(frameon=True, loc='best')
#plt.savefig('results/V222Diagramm1.pdf', format='PDF')