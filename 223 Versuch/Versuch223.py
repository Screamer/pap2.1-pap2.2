import uncertainties as unc
import uncertainties.unumpy as unp
import numpy as np
import scipy.constants as const
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import curve_fit

plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2



def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))


def comma_to_float(valstr):
    return float(valstr.decode('utf-8').replace(',', '.'))

def linear_fit(x, a, b):
    return a*x + b

def plotdata(arg):
    plt.clf()
    plt.grid(ls='dotted')
    if arg == 1:
        plt.plot(x, y, marker='s', color='darkred', linewidth=1, label='Messwerte')
        plt.xlabel(r'x [$\mu m$]', size=17)
        plt.ylabel(r'y [$\mu m$]', size=17)
        plt.title('Brownsche Bewegung', size=20)
        plt.savefig('results/V223Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.title('Histogramm der Verschiebungen', size=20)
        plt.ylabel('rel. Häufigkeit', size=17)
        plt.xlabel(r'Verschiebung [$\mu m$]', size=17)
        plt.hist(all_data, bins=12, facecolor='green', alpha = 0.5, density=True, stacked=True, label='Messwerte')
        gauss = norm.pdf(np.linspace(-4, 4), mu, sigma)
        plt.plot(np.linspace(-4, 4), gauss, 'r--', label='Gauss', linewidth=2)
        plt.text(-3.5, 0.4, r'$\mu$ = {0} $[\mu m]$'.format(round(mu, 3)), fontsize=15)
        plt.text(-3.5, 0.35, r'$\sigma$ = {0} $[\mu m]$'.format(round(sigma, 3)), fontsize=15)
        plt.savefig('results/V223Diagramm2.pdf', format='PDF')
    if arg == 3:
        plt.plot(t[:-1], r_kumm, marker='.', color='darkred', label='Messwerte')
        plt.plot(t[:-1], linear_fit(t[:-1], *popt), color='darkblue', marker=None, label='Ausgleichsgerade')
        plt.text(8, 280, r'$a$ = {0} $[\mu m^2 / s]$'.format(round(popt[0], 3)), fontsize=15)
        plt.text(8, 250, r'$b$ = {0} $[\mu m^2 / s]$'.format(round(popt[1], 3)), fontsize=15)
        plt.xlabel('Zeit / s', size=17)
        plt.ylabel(r'Summe $r_i^2 / \mu m^2$', size=17)
        plt.title('Kummultative Verschiebung', size=20)
        plt.savefig('results/V223Diagramm3.pdf', format='PDF')

    plt.tight_layout()
    plt.legend(frameon=True, loc='best')
    plt.show()

# 1. Importieren
t, x, y = np.loadtxt('data/messdaten.txt', skiprows=1, usecols=(1, 2, 3), converters={1: comma_to_float, 2: comma_to_float, 3: comma_to_float}, unpack=True)
plotdata(1)

# 2. Berechnung
dt = np.array([])
dx = np.array([])
dy = np.array([])
i = 0
while i < len(t) - 1:
    dt = np.append(dt, t[i+1] - t[i])
    dx = np.append(dx, x[i+1] - x[i])
    dy = np.append(dy, y[i+1] - y[i])
    i = i + 1
r_square = dx**2 + dy**2
r_squared = unc.ufloat(np.mean(r_square), np.std(r_square) / np.sqrt(len(r_square))) * 1e-12
dt_mean = np.mean(dt)

D_1 = r_squared / 4 / dt_mean
eta = unc.ufloat(9.8e-4, 0.1e-4)
T = unc.ufloat(21.5 + 273.15, 0.2)
a = unc.ufloat(0.5 * 755e-9, 0.5 * 30e-9)
k_1 = 6 * const.pi * eta * a * r_squared / (4 * T * dt_mean)

## Feedback
print('\n2. Berechnung der Werte')
print('r_square = ' + str(r_squared))
print('dt = ' + str(dt_mean))
print('D_1 ist ' + str(D_1))
print('k_1 ist ' + str(k_1))

# 3. Kontrollverteilung
all_data = np.append(dx, dy)
mu = np.mean(all_data)
sigma = np.std(all_data)
plotdata(2)


# Feedback
print('\n3. Kontrollverteilung')
print('mu = ' + str(mu))
print('sigma = ' + str(sigma))

# 4. Kumultative Verteilung der Verschiebungsquadrate
r_kumm = np.cumsum(r_square)
popt, pcov = curve_fit(linear_fit, t[:-1], r_kumm)
plotdata(3)
a2 = unc.ufloat(popt[0], np.sqrt(pcov[0, 0]))
b = unc.ufloat(popt[1], np.sqrt(pcov[1, 1]))
D_2 = a2 / 4e12
k_2 = 6 * D_2 * const.pi * eta * a / T

## Feedback
print('\n4. Kummultative Verteilung')
print('D beträgt ' + str(D_2) + ' m^2/s')
print('k_2 beträgt ' + str(k_2) + 'J/K')
print('Abweichung der Ds ' + str(calc_sigma(D_2, D_1)))
print('Abweichung der ks ' + str(calc_sigma(k_2, k_1)))
print('Abweichung der k_1 zu k_lit ' + str(calc_sigma(const.k, k_1)))
print('Abweichung der k_2 zu k_lit ' + str(calc_sigma(const.k, k_2)))