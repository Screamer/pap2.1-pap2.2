\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{2}{}%
\contentsline {section}{\numberline {4}Durchführung}{3}{}%
\contentsline {subsection}{\numberline {4.1}Präparation der Partikelsuspension}{3}{}%
\contentsline {subsection}{\numberline {4.2}Aufnahme einer Bildfolge}{3}{}%
\contentsline {subsection}{\numberline {4.3}Eichung des Abbildungsmaßstabs}{3}{}%
\contentsline {subsection}{\numberline {4.4}Vermessung der Partikelposition}{3}{}%
\contentsline {section}{\numberline {5}Auswertung}{3}{}%
\contentsline {section}{\numberline {6}Diskussion}{5}{}%
\contentsline {section}{\numberline {7}Anhang}{5}{}%
\contentsline {subsection}{\numberline {7.1}Source Code}{5}{}%
\contentsline {subsection}{\numberline {7.2}Plots}{8}{}%
