\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {subsection}{\numberline {1.1}Aufbau und Durchführung}{1}{}%
\contentsline {section}{\numberline {2}Theorie}{2}{}%
\contentsline {section}{\numberline {3}Durchführung}{3}{}%
\contentsline {subsection}{\numberline {3.1}Sonnenspektrum}{3}{}%
\contentsline {subsection}{\numberline {3.2}Qualitativer Vergleich einfacher Lichtquellenspektren}{3}{}%
\contentsline {subsection}{\numberline {3.3}Natriumspektrum}{3}{}%
\contentsline {section}{\numberline {4}Auswertung}{3}{}%
\contentsline {subsection}{\numberline {4.1}Auswertung des Sonnenspektrums}{3}{}%
\contentsline {subsection}{\numberline {4.2}Vergleich einfacher Lichtquellenspektren}{4}{}%
\contentsline {subsection}{\numberline {4.3}Auswertung des Natriumspektrums}{5}{}%
\contentsline {section}{\numberline {5}Diskussion}{8}{}%
\contentsline {section}{\numberline {6}Anhang}{10}{}%
