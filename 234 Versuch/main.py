import pandas as pd
import numpy as np
import uncertainties as unc
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt
import scipy as sp
from scipy import signal
from scipy.optimize import curve_fit
from scipy.stats import chi2
from scipy.constants import constants as const
from scipy.signal import argrelextrema

# Einstellungen
plt.rcParams['font.family'] = 'serif'
plt.rcParams['legend.borderaxespad'] = 0.1
plt.rcParams['errorbar.capsize'] = 2
plt.style.use('classic')


def fit_func(m, E_ryd, E3p1, delta_d):
    return hc / (E_ryd / (m - delta_d) ** 2 - E3p1)


def fit_func2(m, E_ryd2, E3p2, delta_s):
    return hc / (E_ryd2 / (m - delta_s) ** 2 - E3p2)


def plot_data(arg):
    plt.clf()
    plt.grid(ls='dotted')
    if arg == 1:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')

        plt.title('Messwerte der verschiedenen LEDs + Laser')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.xlim(300, 800)
        plt.legend(frameon=True, fontsize=7, loc=4)
        plt.savefig('results/V234Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')
        plt.plot(led_weiss_1['lambda'], led_weiss_1['I'], linewidth=1, marker=None, ls='solid', color='gray',
                 label='Messwerte der weißen LED')

        plt.title('Messwerte der verschiedenen LEDs + weiße Lampe')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.xlim(300, 800)
        plt.legend(frameon=True, fontsize='small', loc=3)
        plt.savefig('results/V234Diagramm2.pdf', format='PDF')

    if arg == 3:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')
        plt.plot(led_warm_weiss_groß['lambda'], led_warm_weiss_groß['I'], linewidth=1, marker=None, ls='solid',
                 color='black', label='Messwerte der warmweißen großen LED')

        plt.title('Messwerte der verschiedenen LEDs + große warmweiße Lampe')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.xlim(300, 800)
        plt.legend(frameon=True, fontsize='small', loc=4)
        plt.savefig('results/V234Diagramm3.pdf', format='PDF')
    if arg == 4:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')
        plt.plot(led_warm_weiss_klein['lambda'], led_warm_weiss_klein['I'], linewidth=1, marker=None, ls='solid',
                 color='black', label='Messwerte der warmweißen kleinen LED')

        plt.title('Messwerte der verschiedenen LEDs + kleine weiße Lampe')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.xlim(300, 800)
        plt.legend(frameon=True, fontsize=8, loc=4)
        plt.savefig('results/V234Diagramm4.pdf', format='PDF')
    if arg == 5:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')
        plt.plot(led_warm_weiss_klein2['lambda'], led_warm_weiss_klein2['I'], linewidth=1, marker=None, ls='solid',
                 color='black', label='Messwerte der warmweißen kleinen 2te LED')

        plt.title('Messwerte der verschiedenen LEDs + kleine 2te weiße Lampe')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.legend(frameon=True, fontsize=8, loc=4)
        plt.savefig('results/V234Diagramm5.pdf', format='PDF')

    if arg == 6:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')
        plt.plot(glühlampe['lambda'], glühlampe['I'], linewidth=1, marker=None, ls='solid', color='black',
                 label='Messwerte der Glühlampe')

        plt.title('Messwerte der verschiedenen LEDs + Glühlampe')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.legend(frameon=True, fontsize=7, loc=3)
        plt.savefig('results/V234Diagramm6.pdf', format='PDF')

    if arg == 7:
        plt.plot(led_blue['lambda'], led_blue['I'], linewidth=1, marker=None, ls='solid', color='blue',
                 label='Messwerte der blauen LED')
        plt.plot(led_orange['lambda'], led_orange['I'], linewidth=1, marker=None, ls='solid', color='orange',
                 label='Messwerte der orangen LED')
        plt.plot(led_rot['lambda'], led_rot['I'], linewidth=1, marker=None, ls='solid', color='red',
                 label='Messwerte der roten LED')
        plt.plot(led_rot_orange['lambda'], led_rot_orange['I'], linewidth=1, marker=None, ls='solid', color='purple',
                 label='Messwerte der rot/orangen LED')
        plt.plot(laser['lambda'], laser['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte des Lasers')
        plt.plot(energiesparlampe['lambda'], energiesparlampe['I'], linewidth=1, marker=None, ls='solid', color='black',
                 label='Messwerte der Energiesparlampe')

        plt.title('Messwerte der verschiedenen LEDs + Energiesparlampe')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylabel('Normierte Intensität')
        plt.ylim(0, 1.1)
        plt.legend(frameon=True, fontsize=8, loc=2)
        plt.savefig('results/V234Diagramm7.pdf', format='PDF')

    if arg == 8:
        plt.plot(sonne['lambda'], sonne['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='reine Sonnenlicht')
        plt.plot(sonne_glass['lambda'], sonne_glass['I'], linewidth=1, marker=None, ls='solid', color='darkblue',
                 label='Sonnenlicht hinter Glass')

        plt.title('Messwerte der Sonnenwellenlängen')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.legend(frameon=True, loc=2)
        plt.savefig('results/V234Diagramm8.pdf', format='PDF')

    if arg == 9:
        plt.plot(sonne['lambda'], A_glass, linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Berechnete Absorption')

        plt.title('Absorpion von Glass')
        plt.ylabel('Absorption b.E.')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.xlim(320, 800)
        plt.legend(frameon=True, loc=2)
        plt.savefig('results/V234Diagramm9.pdf', format='PDF')
    if arg == 10:
        plt.plot(sonne['lambda'], sonne['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='reine Sonnenlicht')
        plt.vlines(balmer_series, 0, 1.1, color='darkblue', label='Balmerserie')
        plt.vlines(helium_yellow, 0, 1.1, color='yellow', label='gelbe Linie von He')
        plt.vlines(telluric_oxygen, 0, 1.1, color='black', label='telluric O2')
        plt.vlines(hydrogen, 0, 1.1, color='magenta', label='H2')
        plt.vlines(sodium, 0, 1.1, color='green', label='Sodium')
        plt.vlines(iron_and_calcium, 0, 1.1, color='#71797E', label='Fe and Ca')
        plt.vlines(magnesium, 0, 1.1, color='#2A802A', label='Mg')
        plt.vlines(calcium, 0, 1.1, color='#808090', label='Mg')

        plt.xlim(350, 800)
        plt.title('Messwerte der Sonnenwellenlängen')
        plt.ylabel('Normierte Intensität')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.ylim(0, 1.1)
        plt.legend(frameon=True, fontsize='small', loc=1)
        plt.savefig('results/V234Diagramm10.pdf', format='PDF')
    if arg == 11:
        plt.plot(natrium1['lambda'], natrium1['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte')
        # plt.plot(natrium2['lambda'], natrium2['I'], linewidth=1, marker=None, ls='solid', color='darkblue', label='reine Sonnenlicht')
        # plt.plot(natrium3['lambda'], natrium3['I'], linewidth=1, marker=None, ls='solid', color='darkgreen', label='reine Sonnenlicht')
        # plt.plot(natrium4['lambda'], natrium4['I'], linewidth=1, marker=None, ls='solid', color='black', label='reine Sonnenlicht')

        plt.title('Natriumspektrum hoher Intensität')
        plt.ylabel('Intensität b.E.')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.yscale('log')
        plt.xlim(600, 850)
        plt.ylim(10 ** -3, 1)
        plt.legend(frameon=True, loc=2)
        plt.savefig('results/V234Diagramm16.pdf', format='PDF')
    if arg == 12:
        plt.plot(natrium4['lambda'], natrium4['I'], linewidth=1, marker=None, ls='solid', color='darkred',
                 label='Messwerte')
        # plt.plot(natrium3['lambda'], natrium3['I'], linewidth=1, marker=None, ls='solid', color='darkgreen', label='reine Sonnenlicht')
        # plt.plot(natrium4['lambda'], natrium4['I'], linewidth=1, marker=None, ls='solid', color='black', label='reine Sonnenlicht')

        plt.title('Natriumspektrum mittlere Intensiät')
        plt.ylabel('normierte Intensität b.E.')
        plt.xlabel('Wellenlänge ' + r'$\lambda$ ' + r' in [nm]')
        plt.yscale('log')
        plt.legend(frameon=True, loc=2)
        plt.savefig('results/V234Diagramm12.pdf', format='PDF')
    if arg == 13:
        plt.errorbar(quantenzahlen, unp.nominal_values(wellenl), yerr=unp.std_devs(wellenl), fmt='.', color='darkred',
                     label='zugeordnete Linien')
        plt.xlabel('Quantenzahl N')
        plt.ylabel('Wellenlänge [$nm$]')
        plt.title('1. Nebenserie des Na-Atoms')
        x = np.linspace(2.8, 12.2, 100)
        plt.plot(x, fit_func(x, *popt), color='black', label='Fit der Messwerte')
        plt.legend(frameon=True, loc=1)
        plt.tight_layout()
        plt.savefig('results/V234Diagramm13.pdf', format='PDF')
    if arg == 14:
        plt.errorbar(np.arange(4, 10), neben2_theory, fmt='.', color='darkred', label='berechnete theoretische Werte')
        plt.xlabel('Qantenzahl N')
        plt.ylabel('Wellenlänge [$nm$]')
        plt.title('2 Nebenserie des Na-Atoms')
        x = np.linspace(4, 12.2, 100)
        plt.plot(x, fit_func2(x, *popt2), color='black', label='Fit der theoretischen Werte')
        plt.plot(quantenzahlen, unp.nominal_values(wellenl), color='blue', marker='x', markersize=12, linewidth=0, label='zugeordnete Linien ')
        plt.tight_layout()
        plt.legend()
        plt.savefig('results/V234Diagramm14.pdf', format='PDF')
    plt.show()
    return

num = 7 # Plot
# Daten aus den Messungen - LEDs
led_blue = pd.read_csv('data/Lampen/LED_Blau.txt', skiprows=14, delimiter='\s+', decimal=',')
led_weiss_1 = pd.read_csv('data/Lampen/LED_KaltWeiß.txt', skiprows=14, delimiter='\s+', decimal=',')
led_orange = pd.read_csv('data/Lampen/LED_Orange.txt', skiprows=14, delimiter='\s+', decimal=',')
led_rot = pd.read_csv('data/Lampen/LED_Rot.txt', skiprows=14, delimiter='\s+', decimal=',')
led_rot_orange = pd.read_csv('data/Lampen/Led_Rot_Orange.txt', skiprows=14, delimiter='\s+', decimal=',')
led_warm_weiss_groß = pd.read_csv('data/Lampen/LED_Warmweiß_groß.txt', skiprows=14, delimiter='\s+', decimal=',')
led_warm_weiss_klein = pd.read_csv('data/Lampen/LED_warmweißklein.txt', skiprows=14, delimiter='\s+', decimal=',')
led_warm_weiss_klein2 = pd.read_csv('data/Lampen/LED_warmweißklein_2.txt', skiprows=14, delimiter='\s+', decimal=',')
# Andere
energiesparlampe = pd.read_csv('data/Lampen/energiesparlampe.txt', skiprows=14, delimiter='\s+', decimal=',')
glühlampe = pd.read_csv('data/Lampen/glühlampe.txt', skiprows=14, delimiter='\s+', decimal=',')
laser = pd.read_csv('data/Lampen/laser.txt', skiprows=14, delimiter='\s+', decimal=',')
sonne_glass = pd.read_csv('data/Sonne/hinterlicht.txt', skiprows=14, delimiter='\s+', decimal=',')
sonne = pd.read_csv('data/Sonne/sonnenlicht.txt', skiprows=14, delimiter='\s+', decimal=',')
natrium1 = pd.read_csv('data/Lampen/Natriumlampe_1.txt', skiprows=14, delimiter='\s+', decimal=',')
natrium2 = pd.read_csv('data/Lampen/Natriumlampe_2.txt', skiprows=14, delimiter='\s+', decimal=',')
natrium3 = pd.read_csv('data/Lampen/Natriumlampe_3.txt', skiprows=14, delimiter='\s+', decimal=',')
natrium4 = pd.read_csv('data/Lampen/Natriumlampe_4.txt', skiprows=14, delimiter='\s+', decimal=',')

# Sortieren
led_blue.columns = ['lambda', 'I']
led_weiss_1.columns = ['lambda', 'I']
led_orange.columns = ['lambda', 'I']
led_rot.columns = ['lambda', 'I']
led_rot_orange.columns = ['lambda', 'I']
led_warm_weiss_groß.columns = ['lambda', 'I']
led_warm_weiss_klein.columns = ['lambda', 'I']
led_warm_weiss_klein2.columns = ['lambda', 'I']
energiesparlampe.columns = ['lambda', 'I']
glühlampe.columns = ['lambda', 'I']
laser.columns = ['lambda', 'I']
sonne.columns = ['lambda', 'I']
sonne_glass.columns = ['lambda', 'I']
natrium1.columns = ['lambda', 'I']
natrium2.columns = ['lambda', 'I']
natrium3.columns = ['lambda', 'I']
natrium4.columns = ['lambda', 'I']

# Normierung
print(np.amax(led_orange['I']))
led_blue['I'] = led_blue['I'] / np.amax(led_blue['I'])
led_weiss_1['I'] = led_weiss_1['I'] / np.amax(led_weiss_1['I'])
led_orange['I'] = led_orange['I'] / np.amax(led_orange['I'])
led_rot['I'] = led_rot['I'] / np.amax(led_rot['I'])
led_rot_orange['I'] = led_rot_orange['I'] / np.amax(led_rot_orange['I'])
led_warm_weiss_groß['I'] = led_warm_weiss_groß['I'] / np.amax(led_warm_weiss_groß['I'])
led_warm_weiss_klein['I'] = led_warm_weiss_klein['I'] / np.amax(led_warm_weiss_klein['I'])
led_warm_weiss_klein2['I'] = led_warm_weiss_klein2['I'] / np.amax(led_warm_weiss_klein2['I'])
energiesparlampe['I'] = energiesparlampe['I'] / np.amax(energiesparlampe['I'])
glühlampe['I'] = glühlampe['I'] / np.amax(glühlampe['I'])
laser['I'] = laser['I'] / np.amax(laser['I'])
sonne['I'] = sonne['I'] / np.amax(sonne['I'])
sonne_glass['I'] = sonne_glass['I'] / np.amax(sonne_glass['I'])
natrium1['I'] = natrium1['I'] / np.amax(natrium1['I'])
natrium2['I'] = natrium2['I'] / np.amax(natrium2['I'])
natrium3['I'] = natrium3['I'] / np.amax(natrium3['I'])
natrium4['I'] = natrium4['I'] / np.amax(natrium4['I'])

# Teil 1
print('\n 1. Sonnenspektrum')
A_glass = 1 - sonne_glass['I'] / sonne['I']
balmer_series = np.array([656.3, 486.2, 434.1, 410.2])
balmer_series_exp = unp.uarray([658, 488, 432, 412], 2)
helium_yellow = 589.4
helium_yellow_exp = unc.ufloat(590, 2)
telluric_oxygen = np.array([759.5, 686.7])
telluric_oxygen_exp = unp.uarray([762, 688], 2)
hydrogen = np.array([656.3, 486.1])
hydrogen_exp = unp.uarray([657, 488], 2)
sodium = np.array([589.6, 589])
sodium_exp = unp.uarray([590, 589], 2)
iron_and_calcium = np.array([527, 430.8])
iron_and_calcium_exp = unp.uarray([528, 428], 2)
magnesium = np.array([518.4])
magnesium_exp = unp.uarray([518.27], 2)
calcium = np.array([396.8, 393.4])
calcium_exp = unp.uarray([395, 395], 2)

sigma_balmer = np.abs(unp.nominal_values(balmer_series) - unp.nominal_values(balmer_series_exp)) / np.sqrt(
    unp.std_devs(balmer_series_exp) ** 2)
sigma_helium = np.abs(unp.nominal_values(helium_yellow) - unp.nominal_values(helium_yellow_exp)) / np.sqrt(
    unp.std_devs(helium_yellow_exp) ** 2)
sigma_telluric = np.abs(unp.nominal_values(telluric_oxygen) - unp.nominal_values(telluric_oxygen_exp)) / np.sqrt(
    unp.std_devs(telluric_oxygen_exp) ** 2)
sigma_hydrgen = np.abs(unp.nominal_values(hydrogen) - unp.nominal_values(hydrogen_exp)) / np.sqrt(
    unp.std_devs(hydrogen_exp) ** 2)
sigma_sodium = np.abs(unp.nominal_values(sodium) - unp.nominal_values(sodium_exp)) / np.sqrt(
    unp.std_devs(sodium_exp) ** 2)
sigma_FeCa = np.abs(unp.nominal_values(iron_and_calcium) - unp.nominal_values(iron_and_calcium_exp)) / np.sqrt(
    unp.std_devs(iron_and_calcium_exp) ** 2)
sigma_magnesium = np.abs(unp.nominal_values(magnesium) - unp.nominal_values(magnesium_exp)) / np.sqrt(
    unp.std_devs(magnesium_exp) ** 2)
sigma_calcium = np.abs(unp.nominal_values(calcium) - unp.nominal_values(calcium_exp)) / np.sqrt(
    unp.std_devs(calcium_exp) ** 2)

print('\nDer Fehler zu den Balmerserien beträgt\n ' + str(sigma_balmer))
print('\nDer Fehler zu der Heliumlinie beträgt\n ' + str(sigma_helium))
print('\nDer Fehler zu den Sauerstofflinien beträgt\n ' + str(sigma_telluric))
print('\nDer Fehler zu den Wasserstofflinien beträgt\n ' + str(sigma_hydrgen))
print('\nDer Fehler zu den Sodiumlinien beträgt\n ' + str(sigma_sodium))
print('\nDer Fehler zu den Eisen- und Calciumlinien beträgt\n ' + str(sigma_FeCa))
print('\nDer Fehler zu den Magnesiumlinien beträgt\n ' + str(sigma_magnesium))
print('\nDer Fehler zu den Calciumlinien beträgt\n ' + str(sigma_calcium))

# 2.Natriumspektrum
print('\n 2. Natriumspektrum')
## Ausmessung anhand der Messwerte
### [300 - 540]nm
lambda_left1 = unp.uarray(
    [331, 396, 405, 417, 421, 427.4, 428, 421.2, 434.6, 450.9, 456, 467.5, 476, 494.4, 499.4, 516], 2)
### [560 - 620]nm
lambda_left2 = unp.uarray([569, 590, 616], 2)
### [600 - 850]nm
lambda_left3 = unp.uarray([698, 707.5, 715, 728.6, 738, 752, 765, 773.3, 795.5, 802, 812.7, 820.5, 826.8, 843.9], 2)

# 3. Zuordnung der gefundenen Linien zu Serien
print('\n 3. Zuordnung')
## Erste Nebenserie mp 3p
E_ryd = -13.605  # eV
hc = 1.2398e3  # nm*eV
E3p = E_ryd / 9 - hc / 820
E3p_err = hc * 2.8 / 820 ** 2

liste1 = []
for m in range(3, 13):
    l = hc / (E_ryd / m ** 2 - E3p)
    l_err = hc * E3p_err / (E_ryd / m ** 2 - E3p) ** 2
    liste1.append(l)
    print('m={m:2d},  lambda={l:6.2f},  error={l_err:6.2f}'.format(m=m, l=l, l_err=l_err))

neben1_theory = np.array(liste1)

## Zweite Nebenserie ms 3p
D_line = 590.0  # nm
E3s = E3p - hc / D_line
E3s_err = E3p_err  # alle anderen Größen fehlerfrei

delta_s = 3 - np.sqrt(E_ryd / E3s)
delta_s_err = np.sqrt(E_ryd / E3s) / (2 * np.abs(E3s)) * E3s_err

liste2 = []
for n in range(4, 10):
    l2 = hc / (E_ryd / (n - delta_s) ** 2 - E3p)
    l2_err = np.sqrt(((hc * E3p_err) / (E_ryd / (n - delta_s) ** 2 - E3p) ** 2) ** 2 + (
            (2 * hc * (n - delta_s) * delta_s_err) / (E_ryd - E3p * (n - delta_s) ** 2) ** 2) ** 2)
    liste2.append(l2)
    print('n={n:2d},  lambda={l2:6.2f},  error={l2_err:6.2f}'.format(n=n, l2=l2, l2_err=l2_err))

neben2_theory = np.array(liste2)

## Hauptserie mp 3s
delta_p = 3 - np.sqrt(E_ryd / E3p)
delta_p_err = np.sqrt(E_ryd / E3p) / (2 * np.abs(E3p)) * E3p_err

liste3 = []
for i in range(4, 6):
    l3 = hc / (E_ryd / (i - delta_p) ** 2 - E3s)
    l3_err = np.sqrt(((hc * E3s_err) / (E_ryd / (i - delta_p) ** 2 - E3s) ** 2) ** 2 + (
            (2 * hc * (i - delta_p) * delta_p_err) / (E_ryd - E3s * (i - delta_p) ** 2) ** 2) ** 2)
    liste3.append(l3)
    print('i={i:2d},  lambda={l3:6.2f},  error={l3_err:6.2f}'.format(i=i, l3=l3, l3_err=l3_err))

main_theory = np.array(liste3)

# 4. Bestimmung der Serienenergie und der l-abhängigen Korrekturfaktoren
print('\n 4. Bestimmung der Serienenergie')
wellenl = unp.uarray([819, 569, 498.2, 467, 451, 434.6], 2)
quantenzahlen = np.array([3, 4, 5, 6, 7, 9])
para1 = [-13.6, -3, -0.2]
popt, pcov = curve_fit(fit_func, quantenzahlen, unp.nominal_values(wellenl), sigma=unp.std_devs(wellenl), p0=para1)

print('Die Fitparameter wurden wie folgt bestimmt:')
print()
print("E_ryd1=", popt[0], " +/- ", np.sqrt(pcov[0, 0]))
print("E3p1=", popt[1], " +/- ", np.sqrt(pcov[1, 1]))
print("delta_d=", popt[2], " +/- ", np.sqrt(pcov[2, 2]))

##Bestimmung der chi^2 Summe
chi2_1 = np.sum((fit_func(quantenzahlen, *popt) - unp.nominal_values(wellenl)) ** 2 / unp.std_devs(wellenl) ** 2)
dof1 = len(quantenzahlen) - 3  # Freiheitsgrade
chi2_red_1 = chi2_1 / dof1
print("chi2_1=", chi2_1)
print("chi2_red_1", chi2_red_1)

prob1 = round(1 - chi2.cdf(chi2_1, dof1), 2) * 100
print("Wahrscheinlichkeit:", prob1, "%")

## Analog 2te Nebenserie
wellenl = unp.uarray([516, 456], 2)
quantenzahlen = np.array([6, 8])
para1 = [-13.6, -3, 1]
popt2, pcov2 = curve_fit(fit_func2, np.arange(4, 10), neben2_theory, sigma=1.7 * np.ones(6), p0=para1)

print('Die Fitparameter wurden wie folgt bestimmt:')
print()
print("E_ryd2=", popt2[0], " +/- ", np.sqrt(pcov2[0, 0]))
print("E3p2=", popt2[1], " +/- ", np.sqrt(pcov2[1, 1]))
print("delta_s=", popt2[2], " +/- ", np.sqrt(pcov2[2, 2]))

# Bestimmung der chi^2 Summe
chi2_2 = np.sum((fit_func2(np.arange(4, 10), *popt2) - neben2_theory) ** 2 / (1.7 * np.ones(6)) ** 2)
dof2 = len(np.arange(4, 10)) - 3  # Freiheitsgrade
chi2_red_2 = chi2_2 / dof2
print("chi2_2=", chi2_2)
print("chi2_red_2", chi2_red_2)

# Fitwahrscheinlichkeit
prob2 = round(1 - chi2.cdf(chi2_2, dof2), 2) * 100
print("Wahrscheinlichkeit:", prob2, "%")
plot_data(num)