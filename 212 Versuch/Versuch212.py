import numpy as np
import uncertainties as unc
import uncertainties.unumpy as unp
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy.optimize import curve_fit
from scipy import odr

plt.style.use('classic')
plt.rcParams['errorbar.capsize'] = 2
plt.rcParams['font.family'] = 'serif'


def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2)


def linear_fit(x, a, b):
    return a * x + b

def log_fit(x,a,b):
    return a*np.log(x)+b


def plot_data(arg):
    plt.clf()
    plt.grid('dotted')
    if arg == 1:
        plt.clf()
        plt.errorbar(unp.nominal_values(r ** 2), unp.nominal_values(v_mean / (rho_k - rho_f)), xerr=unp.std_devs(r ** 2), yerr=unp.std_devs(v_mean / (rho_k - rho_f)), marker='.', fmt='none', color='darkred', label='Messwerte')
        plt.errorbar(unp.nominal_values(r ** 2), unp.nominal_values(v_mean_lam / (rho_k - rho_f)), xerr=unp.std_devs(r ** 2), yerr=unp.std_devs(v_mean_lam / (rho_k - rho_f)), marker='.', fmt='none', color='darkblue', label='korr. Messwerte')
        x = np.linspace(unp.nominal_values(r[7] ** 2), unp.nominal_values(r[0] ** 2), 2)
        plt.plot(x, linear_fit(x, *popt), color='green', marker=None, ls='-', label='Ausgleichsgerade')
        plt.plot(x, linear_fit(x, *popt2), color='magenta', marker=None, ls='-', label='korr. Ausgleichsgerade')
        plt.title('Stokes: Fallgeschwindigkeiten in Abhängigkeit des Radius r')
        plt.xlabel('$r^2$ [$m^2$]', fontsize=20)
        plt.ylabel(r'$\frac{v}{\rho_k - \rho_f}$ [$\frac{m^4}{gs}$]', fontsize=20)
        plt.tight_layout()
        plt.legend(frameon=True, borderpad=0, borderaxespad=0, loc=2)
        plt.savefig('results/V212Diagramm1.pdf', format='PDF')
        plt.show()
    if arg ==2:
        plt.errorbar(unp.nominal_values(Re), unp.nominal_values(v_mean/v_lam), xerr=unp.std_devs(Re), yerr=unp.std_devs(v_mean/v_lam), fmt='none', marker='.', color='darkred', label='Messwerte')
        plt.errorbar(unp.nominal_values(Re_korr), unp.nominal_values(v_mean/v_lam_korr), xerr=unp.std_devs(Re_korr), yerr=unp.std_devs(v_mean/v_lam_korr), fmt='none', marker='.', color='darkblue', label='korr. Messwerte')
        x = np.linspace(1e-2, 1, 2)
        plt.plot(x, log_fit(x, *popt))
        plt.title(r'Abhängigkeit von $\frac{v}{v_{lam}}$ zu log(Re)', fontsize=22)
        plt.xlabel(r'log(Re)', fontsize=20)
        plt.ylabel(r'$\frac{v}{v_lam}$', fontsize=20)
        plt.xscale('log')
        plt.tight_layout()
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=2)
        plt.show()
    if arg == 3:
        plt.errorbar(unp.nominal_values(r_square), unp.nominal_values(y2), xerr=unp.std_devs(r_square), yerr=unp.std_devs(y2), marker='.', fmt='none', color='darkred', label='Messwerte')
        plt.errorbar(unp.nominal_values(r_square), unp.nominal_values(y1), xerr=unp.std_devs(r_square), yerr=unp.std_devs(y1), marker='.', fmt='none', color='darkblue', label='korr. Messwerte')
        x = np.linspace(unp.nominal_values(r_square[7]), unp.nominal_values(r_square), 2)
        plt.plot(x, linear_fit(x, *popt), color='green', marker=None, ls='-', label='Ausgleichsgerade')
        plt.plot(x, linear_fit(x, *popt2), color='magenta', marker=None, ls='-', label='korr. Ausgleichsgerade')
        plt.title('Stokes: Fallgeschwindigkeiten in Abhängigkeit des Radius r')
        plt.xlabel('$r^2$ [$m^2$]', fontsize=20)
        plt.ylabel(r'$\frac{v}{\rho_k - \rho_f}$ [$\frac{m^4}{gs}$]', fontsize=20)
        plt.tight_layout()
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=2)
        plt.show()

    return


# 1 Fallviskosimeter
## Kugelreihenfolge : 9, 8, 7.144, 6, 5, 4, 3, 2 mm
d = unp.uarray([9, 8, 7.144, 6, 5, 4, 3, 2], np.array([9, 8, 7.144, 6, 5, 4, 3, 2]) * 0.01) * const.milli
t = unp.uarray([6.86, 6.89, 6.64, 6.89, 6.95,
                8.68, 8.43, 8.72, 8.64, 8.31,
                9.62, 9.68, 9.53, 9.54, 9.45,
                12.78, 12.73, 12.92, 12.96, 12.96,
                17.90, 17.59, 17.40, 17.43, 17.64,
                13.34, 13.48, 13.39, 13.51, 13.61,
                21.37, 21.31, 21.54, 21.67, 21.59,
                45.92, 47.02, 45.58, 44.15, 44.50],
               0.2)
h = unp.uarray([200, 200, 200, 200, 200,
                200, 200, 200, 200, 200,
                200, 200, 200, 200, 200,
                200, 200, 200, 200, 200,
                200, 200, 200, 200, 200,
                100, 100, 100, 100, 100,
                100, 100, 100, 100, 100,
                100, 100, 100, 100, 100],
               2) * const.milli
rho_k = unp.uarray([1.3625,
                    1.3575,
                    1.3575,
                    1.3575,
                    1.3575,
                    1.3575,
                    1.3575,
                    1.3575],
                   0.0025) * const.gram / const.centi**3

rho_f = unc.ufloat(1.1487, 5e-4) * const.gram / const.centi**3 # Anhand Temperatur

v_mean = unp.uarray(np.zeros(8), 0)
for i in range(0, 7):
    v_mean[i] = sum(h[5*i:5*i+4]/t[5*i:5*i+4])/len(t[5*i:5*i+4])
D = 75 * const.milli
k = (1+2.1*(d/D))
v_lam = v_mean * k
r_square = (d/2)**2
y1 = v_lam/(rho_k-rho_f)
y2 = v_mean/(rho_k-rho_f)
popt, pcov = curve_fit(linear_fit, unp.nominal_values(r_square), unp.nominal_values(y1))
popt2, pcov2 = curve_fit(linear_fit, unp.nominal_values(r_square), unp.nominal_values(y2))
a = unc.ufloat(popt2[0], pcov2[0, 0])  # Steigung aus Messwerten ohne Korrektur
a2 = unc.ufloat(popt[0], pcov[0, 0])   # Steigung mit Korrektur
eta = 2 * const.g / (9 * a)
eta2 = 2 * const.g / (9 * a2)

print('\n Teil 1. Kugelfallviskosimter')
print('Anhand der Messwerte ohne Korrektur ergibt sich eine Viskosität von ' + str(eta) + ' Pa s')
print('Anhand der Messwerte mit Korrektur ergibt sich eine Viskosität von ' + str(eta2) + ' Pa s')

## Alternativer Weg
r = unp.uarray([9, 8, 7.144, 6, 5, 4, 3, 2], np.array([9, 8, 7.144, 6, 5, 4, 3, 2]) * 0.01) * const.milli / 2
rho_k = unp.uarray([1.3625, 1.3575, 1.3775, 1.3775, 1.3775, 1.3775, 1.3775, 1.3775],
                   0.0025) * const.gram / const.centi ** 3
rho_f = unc.ufloat(1.1482, 5e-4) * const.gram / const.centi ** 3  # Anhand Temperatur
h = unp.uarray([200, 200, 200, 200, 200, 100, 100, 100], 2) * const.milli
R = 75 / 2 * const.milli
t_9 = unp.uarray([6.86, 6.89, 6.64, 6.89, 6.95], 0.2)
t_8 = unp.uarray([8.68, 8.43, 8.72, 8.64, 8.31], 0.2)
t_7 = unp.uarray([9.62, 9.68, 9.53, 9.54, 9.45], 0.2)
t_6 = unp.uarray([12.78, 12.73, 12.92, 12.96, 12.96], 0.2)
t_5 = unp.uarray([17.90, 17.59, 17.40, 17.43, 17.64], 0.2)
t_4 = unp.uarray([13.34, 13.48, 13.39, 13.51, 13.61], 0.2)
t_3 = unp.uarray([21.37, 21.31, 21.54, 21.67, 21.59], 0.2)
t_2 = unp.uarray([45.92, 47.02, 45.58, 44.15, 44.50], 0.2)
## Fallgeschwindigkeiten
v_9 = h[0] / t_9
v_8 = h[1] / t_8
v_7 = h[2] / t_7
v_6 = h[3] / t_6
v_5 = h[4] / t_5
v_4 = h[5] / t_4
v_3 = h[6] / t_3
v_2 = h[7] / t_2
## Durchschnittsgeschwindigkeit
v_mean = unp.uarray(np.zeros(8), 0)
v_mean[0] = sum(v_9) / len(v_9)
v_mean[1] = sum(v_8) / len(v_8)
v_mean[2] = sum(v_7) / len(v_7)
v_mean[3] = sum(v_6) / len(v_6)
v_mean[4] = sum(v_5) / len(v_5)
v_mean[5] = sum(v_4) / len(v_4)
v_mean[6] = sum(v_3) / len(v_3)
v_mean[7] = sum(v_2) / len(v_2)

popt, pcov = curve_fit(linear_fit, unp.nominal_values(r ** 2), unp.nominal_values(v_mean / (rho_k - rho_f)), sigma=unp.std_devs(v_mean / (rho_k - rho_f)))
a = unc.ufloat(popt[0], pcov[0][0])
eta = 2 * const.g / (9 * a)

lam = (1 + 2.1 * r / R)
v_mean_lam = v_mean * lam
popt2, pcov2 = curve_fit(linear_fit, unp.nominal_values(r ** 2), unp.nominal_values(v_mean_lam / (rho_k - rho_f)), sigma=unp.std_devs(v_mean_lam / (rho_k - rho_f)))
a = unc.ufloat(popt2[0], pcov[0, 0])
eta2 = 2*const.g / (9*a)

# v_lam = 2/9 * const.g * (rho_k-rho_f) / eta * r**2
# v_lam_korr = 2/9 * const.g * (rho_k-rho_f) / eta2 * r**2

v_theo = linear_fit(unp.nominal_values(r**2), *popt) * (rho_k - rho_f)
v_theo_korr = linear_fit(unp.nominal_values(r**2), *popt2) * (rho_k - rho_f)
plot_data(1)
Re = rho_f * v_mean * 2 * r / eta
Re_korr = rho_f * v_mean * 2 * r / eta2

popt, pcov = curve_fit(linear_fit, unp.nominal_values(Re[0:3]), unp.nominal_values(v_mean[0:3]/v_lam[0:3]), sigma=unp.std_devs(v_mean[0:3]/v_lam[0:3]))

## Feedback
print('\n Teil 1. Kugelfallviskosimter')
print('Genauer: Anhand der Messwerte ohne Korrektur ergibt sich eine Viskosität von ' + str(eta) + ' Pa s')
print('Genauer: Anhand der Messwerte mit Korrektur ergibt sich eine Viskosität von ' + str(eta2) + ' Pa s')
print('v_mean beträgt\n ' +str(v_mean))
print('v_mean mit Korrektur beträgt\n ' +str(v_mean_lam))
print('Mit der Geradensteigung ist v_mean ohne Korrektur als\n ' +str(v_theo) + ' zu erwarten')
print('Mit der Geradensteigung ist v_mean mit Korrektur als\n ' +str(v_theo_korr) + ' zu erwarten')
print('Die Abweichung ohne Korrektur ergibt '+str(calc_sigma(v_theo, v_mean))+' Sigma')
print('Die Abweichung mit Korrektur ergibt '+str(calc_sigma(v_theo_korr, v_mean_lam))+' Sigma')
print('Re beträgt ohne Korrektur '+str(Re))
print('Re beträgt mit Korrektur '+str(Re_korr))

Re = unp.nominal_values(Re)
Fehler_Re = unp.std_devs(Re)
breakpoint = 5
x_fit1 = np.linspace(Re[0]*1.1, Re[breakpoint+1], 1000)
x_fit2 = np.linspace(Re[breakpoint-3], Re[Re.size-1]/1.1, 1000)

k2 = unp.nominal_values(1/k)
Fehler_k2 = unp.std_devs(1/k)
line1 = k2[0]+np.log(x_fit1/Re[0])*(k2[breakpoint-1]-k2[0])/np.log(Re[breakpoint-1]/Re[0])
line2 = k2[breakpoint]+np.log(x_fit2/Re[breakpoint])*(k2[k2.size-1]-k2[breakpoint])/np.log(Re[Re.size-1]/Re[breakpoint])

fig, ax = plt.subplots(1, figsize=[6.4 *2, 4.8 *2])
plt.ticklabel_format(axis='both', style='sci', scilimits=(0,3), useMathText=True)
plt.errorbar(Re, k2, yerr=Fehler_k2, xerr=Fehler_Re, lw=1, ecolor='k', fmt='none', capsize=2, label='Messdaten')
plt.plot(x_fit1, line1, 'r', lw=1, label='1. Gerade, turbulente Strömung')
plt.plot(x_fit2, line2, 'g', lw=1, label='2. Gerade, laminare Strömung')
plt.title('Bestimmung der kritischen Reynoldszahl ${Re}_{kr}$')
plt.xscale('log')
plt.grid(which='major')
plt.grid(which='minor', linestyle=':')
plt.xlabel('Re')
plt.ylabel('$v/v_{lam}$')
plt.legend(loc='best')
plt.savefig('results/V212Diagramm2.pdf', format='PDF')
plt.show()

# Teil 2. Kapillarviskosimeter
V = unp.uarray([5, 10, 15, 20, 25], 0.5) * 1e-6
t2 = unp.uarray([2.27, 4.55, 7.29, 9.56, 12.3], 0.003) * 60
R = unc.ufloat(1.5, 0.01) * const.milli / 2
L = unc.ufloat(100, 0.5) * const.milli
h_a = unc.ufloat(530, 1) * const.milli
h_e = unc.ufloat(526, 1) * const.milli
q = unp.uarray([0.2, 0.4, 0.6, 0.8, 1], 0)
h_mean = h_e + (h_a - h_e)*q
rho_f = unc.ufloat(1.1482, 0.0002) * const.gram / const.centi**3

eta3 = const.pi * (rho_f*h_mean*const.g)*R**4*t2/(8*L*V)
eta3_mean = sum(eta3) / len(eta3)

Re2 = 2*rho_f*V / (t2 * const.pi * R * eta3)
Re2_mean = sum(Re2)/(len(Re2))

print('Hier beträgt die Viskosität '+str(eta3_mean)+' Pa s')
print('Re beträgt '+str(Re2_mean))