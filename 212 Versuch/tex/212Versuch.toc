\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {subsection}{\numberline {1.1}Aufbau}{1}{}%
\contentsline {section}{\numberline {2}Theorie}{2}{}%
\contentsline {subsection}{\numberline {2.1}Bestimmung der Viskosität nach Stokes}{2}{}%
\contentsline {subsection}{\numberline {2.2}Bestimmung der Viskosität nach Hagen-Poiseuille}{2}{}%
\contentsline {subsection}{\numberline {2.3}Strömungsarten}{3}{}%
\contentsline {section}{\numberline {3}Durchführung}{4}{}%
\contentsline {subsection}{\numberline {3.1}Kugelfallviskosimeter}{4}{}%
\contentsline {subsection}{\numberline {3.2}Kapillarviskosimeter}{4}{}%
\contentsline {section}{\numberline {4}Auswertung}{4}{}%
\contentsline {subsection}{\numberline {4.1}Kugelfallviskosimeter}{4}{}%
\contentsline {subsection}{\numberline {4.2}Kapillarviskosimeter}{6}{}%
\contentsline {section}{\numberline {5}Diskussion}{7}{}%
\contentsline {section}{\numberline {6}Anhang}{8}{}%
\contentsline {subsection}{\numberline {6.1}Messprotokoll}{8}{}%
\contentsline {subsection}{\numberline {6.2}Plots}{13}{}%
\contentsline {subsection}{\numberline {6.3}Source Code}{13}{}%
