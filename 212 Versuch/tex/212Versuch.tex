\documentclass{article}

\usepackage[ngerman]{babel}
\usepackage{amsmath, amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{wrapfig}
\usepackage{pdfpages}
\usepackage{xparse}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{siunitx}

\pagestyle{fancy}
\fancyhf{}
\rhead{mm268}
\lhead{November 2021}
\cfoot{\thepage}

\begin{document}
\begin{titlepage}
    \title{Versuch 212 \\ Zähigkeit von Flüssigkeiten}
    \author{Jan Bartels}
    \date{November 2021}
    \maketitle
    \tableofcontents
\end{titlepage}
\section{Einleitung}
In diesem Versuch wird die Viskosität von Polyethylenglykol einmal mithilfe eines Kugelfallviskosimeter und einmal mit einem Kapillarviskosimeter bestimmt. 
Zusätzlich wird der Übergang von laminarer zu turbulenter Umströmung einer Kugel ermittelt.\\
Am Ende sollen die beiden Viskositäten miteinander verglichen werden. 
\subsection{Aufbau}
Deshalb wurde folgendes aufgetragen
\begin{enumerate}
    \item Bestimmung der Viskosität von Polyethylenglykol nach Stokes
    \item Überprüfung der Gültigkeitsgrenze des Stokes'schen Gesetzes
    \item Bestimmung der Zähigkeit von Polyethylenglykol nach Hagen-Poiseuille
\end{enumerate}
Es wurde weiterhin gestellt
\begin{itemize}
    \item Messzylinder aus Hartglas mit Messskaler,Polyethylenglykol
    \item Kugeln aus Hostarform C mit unterschiedlichen Durchmessern
    \item Thermometer, Pinzette, Bechergläser, Maßstab, Stoppuhren
\end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{../data/aufbau.png}
    \caption{Kugelfallviskosimeter und Kapillarviskosimeter}
\end{figure}
Diese Grafik wurde aus dem von Dr. J. Wagner erstellten Skript zum physikalischen Anfängerpraktikum entnommen.
\section{Theorie}
In diesem Versuch wird die Viskosität der Probe nach Stokes und Hagen-Poiseuille berechnet.
\subsection{Bestimmung der Viskosität nach Stokes}
Der Ansatz hier ist, dass auf einen in einem Medium fallender Körper mit konstanter Geschwindigkeit eine Reibungskraft wirkt. Nimmt man den Körper als kugelförmig an, so berechnet sich die Reibungskraft $F_r$ anhand der Geschwindigkeit $v$, dem Kugelradius $r$ und der Viskosität $\eta$
\begin{align}
    F_r &= 6\pi \eta r v_S
\end{align} 
Wird eine Kugel mit Masse $m$ in einem Kugelfallviskosimeter fallen gelassen, so wirken Kräfte auf diese. Jene sind die Gravitationskraft $F_g$, die Auftriebskraft $F_a$ und die Reibungskraft $F_r$
\begin{align}
    V_k &= \frac{4}{3}\pi r^3\\
    F_a &= \rho_f V_k g\\
    F_g &= mg = \rho_k V_k g = \frac{4}{3}\pi\rho_k r^3 g
\end{align}
$\rho_f$ beschreibt die Dichte der Flüssigkeit und alle Werte mit Indizes $k$ beschreiben Eigenschaften der Kugel.
Fällt die Kugel gleichförmig, ist die Summe aller Kräfte 0 und umgeformt nach der Viskosität $\eta$ ergibt sich
\begin{align}\label{eq:1}
    \eta &= \frac{2}{9}g\frac{\rho_k - \rho_f}{v_s}r^2
\end{align}
$g$ beschreibt in allen Formeln die Gravitationskonstante. Misst man daher die Fallgeschwindigkeit $v_s$, so kann man aus dieser die Viskosität bestimmen.\\
Weil hier der Durchmesser des Fallrohres als unendlich angenommen wurde, dem aber in der Realität nicht so ist, so muss man die Ladenburg'sche Korrektur verwenden. Diese beträgt
\begin{align}\label{eq:2}
    \lambda &= 1+2.1\frac{r}{R}
\end{align}
Wobei $R$ den Rohrradius bezeichnet. Um diesen Korrekturterm zu benutzen muss man alle Geschwindigkeiten $v_s$ damit multiplizieren.
\subsection{Bestimmung der Viskosität nach Hagen-Poiseuille}
Hier wird die Fließgeschwindigkeit des Medium in den Vordergrund geschoben. Betrachtet man ein Rohr in welchem das Medium, von nun an eine Flüssigkeit, durch eine externe Kraft gedrückt wird, so berechnet sich die resultierende Kraft aus der Druckdifferenz. Wenn das Rohr eine Länge $L$ und einen Radius $R$ besitzt und die Flüssigkeit mit einem Druck $p_1$ in das Rohr getrieben wird und außerhalb einer von $p_2$ herrscht, so gilt
\begin{align}
    F_p &= \pi r^2 (p_1-p_2)
\end{align}
Nimmt man zusätzlich an, dass die Flüssigkeit in der Mitte des Rohres am schnellsten fließt und am Rand am langsamsten, was aufgrund der Reibung eintritt, so gilt für die Reibungskraft $F_r$
\begin{align}
    F_r &= -2\pi r L \eta \frac{dv}{dr}
\end{align}
Gleichsetzen und Umformen ergibt
\begin{align}
    \frac{dv}{dr} &= \frac{p_1-p_2}{2\eta L}r\\
    \text{Integration ergibt}\\
    v(r) &= \frac{p_1-p_2}{4\eta L}(R^2-r^2)
\end{align}
Über den gesamten Querschnitt des Rohres integriert folgt
\begin{align}\label{eq:3}
    \frac{dV}{dt} &= \frac{\pi (p_1-p_2)R^4}{8\eta L}
\end{align}
Wird also die Volumenänderung gemessen, so kann man daraus $\eta$ bestimmen.
\subsection{Strömungsarten}
Auch werden verschiedene Strömungsarten betrachtet. Hier differenziert man zwischen laminarer und turbulenter Strömung. 
Am Beispiel von Wasser lässt sich der Unterschied anschaulich erklären. Würde man eine Kugel in das Wasser hineinpacken und diese Kugel auf eine konstante Geschwindigkeit bringen, so würden auch die umliegenden Wasserteilchen bewegt. Würde man also Farbmittel hineingeben, so könnte man die Bewegung sehen. Laminare Strömung tritt genau dann ein, wenn sich die Kugel langsam genug fortbewegt, sodass die gefärbten Teilchen nur gerade Spuren ergeben. 
Turbulente Strömung tritt genau dann ein, wenn sich die Kugel zu schnell fortbewegt. Man würde auch Strudel erkennen können. Zusammenfassend kann man sagen, dass bei laminaren Strömungen eine Wechselwirkung zwischen den unterschiedlichen Wasserschichten nicht stattfindet, währenddessen sich turbulende Strömungen auf die Wechselwirkungen bezieht.\\
Eine Abschätzung dafür bietet die Reynoldszahl, die aus dem Quotienten der doppelten kinetischen Energie $E_{kin}$ der Flüssigkeit und der Reibungsenergie $W_{r}$ berechnet wird. 
\begin{align}
    Re &= \frac{2E_{kin}}{W_r}
\end{align}
Für die Messungen mit einem Kugelfallviskosimeter kann man den Wert auch wie folgt berechnen:
\begin{align}
    Re &= \frac{\rho_f v L}{\eta}
\end{align}
wobei L den Rohrdurchmesser beschreibt.

\section{Durchführung}
\subsection{Kugelfallviskosimeter}
Die Kugeln unterschiedlicher Radien wurden in das Kugelfallviskosimeter fallen gelassen, nachdem diese in einem Becherglas sorgfälltig in der Probe lagen, sodass sich beim Herausholen eine dünne Schicht Polyethylenglykol bereits auf der Kugeloberfläche befand.
Nachdem die Kugeln fallen gelassen wurden, wurde gewartet bis diese eine bestimme Distanz zurückgelegt haben. Sodann wurde deren Zeit gemessen, welche sie benötigten um eine vorgegebene Strecke zu hinterlegen. Die genauen Distanzen und Zeiten sind im Protokoll hinterlegt.
\subsection{Kapillarviskosimeter}
Der Hahn wurde geöffnet und darauf gewartet, dass die Tropfenfolge konstant wurde. Dann wurde das sich darunter befindende Becherglas mit einem Messzylinder getauscht. Für alle 5 $cm^3$ wurde die Zeit gemessen. Bei Anfangs- und Endwert wurde zudem noch die Höhe des Flüssigkeitsspiegels gemessen.
\section{Auswertung}
\subsection{Kugelfallviskosimeter}
Alle Messwerte wurden übergeben. Zusätzlich musste noch die Dichte der Probe $\rho_f$ bestimmt werden. Dafür wurde die Zimmertemperatur aufgenommen und anhand des Anhangs die Dichte abgelesen. Hierfür wurde benutzt
\begin{align*}
    T &= \SI{21\pm 0.5}{\celsius} &   \rho_f = 1.1482\pm 0.0005 [\frac{g}{cm^3}]
\end{align*}
Anhand der Messwerte wurde ein Diagramm geplottet, welches $\frac{v_{lam}}{\rho_k - \rho_f}$ in Abhängigkeit vom Radius $r^2$ der jeweiligen Kugeln widergibt.
Das Ergebnis ist auch in Abbildung \ref{fig:1} erkennbar.
Betrachtet man nun Formel \ref{eq:1}, so ergibt sich:
\begin{align*}
    d &= [9, 8, 7.144, 6, 5, 4, 3, 2]mm   &   \Delta d &= 1\%\\
    \rho_f &= \text{Siehe Tabelle im Protokoll} &   \Delta\rho_f &= 0.0025\frac{g}{cm^3}\\
    v_{lam} \propto r^2\\
    v_{lam} &= \frac{h}{t}   &   \Delta v_{lam} &= \sqrt{(\frac{\Delta h}{t})^2+(h \frac{\Delta t}{t^2})^2}\\
    r &= d/2    &   \Delta r &= \frac{\Delta d}{2}\\
    D &= 75mm   &   \Delta D &= 1mm\\
    R &= D/2    &   \Delta R &= \frac{\Delta D}{2}\\
    \lambda &= 1+2.1\frac{r}{R} &   \Delta \lambda &= \sqrt{(2.1\frac{\Delta r}{R})^2+(2.1r\frac{\Delta R}{R})^2}\\
    \frac{v_{lam}}{\rho_k - \rho_f} &= \frac{2g}{9\eta}r^2\\
    m &= \frac{2g}{9\eta}\\
    eta &= \frac{2g}{9a}    &   \Delta \eta &= \sqrt{(\frac{2\Delta g}{9a})^2+(\frac{2g\Delta a}{9a^2})^2}
\end{align*}
$a$ bezeichnet die Ausgleichsgeradensteigung, $h$ die im Protokoll festgelegte Falldistanz für welche die Zeit $t$ gemessen wurde. Die Werte für $h$ sind im Protokoll hinterlegt samt der Messzeit $t$. Für den Fehler wurde jeweil ein Skalenteil benutzt und die menschliche Reaktionszeit, welche zu einem Zeitfehler von $\Delta t = 0.2s$ führt. Auch wurde der Ladenburg'sche Korrekturterm berücksichtigt. Dafür wurde nochmal $v_{lam}$ gemäß \ref{eq:2} mit $\lambda$ multipliziert. 
Die neuen Werte wurden in dasselbe Diagramm eingetragen. Anhand Abbildung \ref{fig:1} kann man gut erkennen, dass gerade die Korrekturterme für größere Radien berücksichtigt werden müssen.
Anhand der berechneten $\eta$, bzw einmal mit Korrekturterm und einmal ohne, wurden die theoretisch zu erwartende Geschwindigkeiten berechnet, sowie die Abweichungen zu den Messwerten
\begin{table}[ht]
    \centering
    \label{tab:1}
    \begin{tabular}{|l|l|l|l|}
        \hline
        Ohne Korrektur\\
        \hline
        $d [mm]$    &   $v_{exp}[\frac{cm}{s}]$   &   $v_{theo}[\frac{cm}{s}]$  &   $\sigma$\\
        \hline
        9   &   $2.92\pm0.05$   &   $3.12\pm 0.04$   &     4\\
        8   &   $2.34\pm 0.04$  &   $2.42\pm 0.03$  &   1.9\\
        7.144   &   $2.09\pm 0.3$   &   $2.129\pm 0.024$  &   1.1\\
        6   &   $1.554\pm 0.019$    &   $1.523\pm 0.017$    &   1.3\\
        5   &   $1.137\pm 0.013$    &   $1.079\pm 0.012$    &   4\\
        4   &   $0.743\pm 0.016$    &   $0.716\pm 0.008$    &   1.6\\
        3   &   $0.465\pm 0.001$    &   $0.434\pm 0.005$  &   3\\
        2   &   $0.2202\pm 0.0005$  &   $0.232\pm 0.003$  &   2.4\\
        \hline 
        Mit Korrektur\\
        \hline
        9       &   $3.66\pm 0.07$      &   $3.75\pm 0.05$  &   1.2\\
        8       &   $2.86\pm 0.05$      &   $2.90\pm 0.04$  &   0.7\\
        7.144   &   $2.51\pm 0.04$      &   $2.542\pm 0.029$  &   0.8\\
        6       &   $1.815\pm 0.023$    &   $1.805\pm 0.021$  &   0.4\\
        5       &   $1.296\pm 0.015$    &   $1.267\pm 0.015$  &   1.5\\
        4       &   $0.826\pm 0.018$    &   $0.83\pm 0.01$  &   0.19\\
        3       &   $0.504\pm 0.011$    &   $0.483\pm 0.006$  &   1.9\\
        2       &   $0.233\pm 0.005$    &   $0.238\pm 0.003$    &   1.1\\
        \hline
    \end{tabular}
    \caption{Zusammenfassung der Ergebnisse aus 1.}
\end{table}
\\Es wurden folgende Viskositäten berechnet
\begin{align*}
    \eta &= 0.3097 \pm 0.0014 [Pa*s]\\
    \eta_{korr} &= 0.2551 \pm 0.0011 [Pa*s]
\end{align*}
Betrachtet man die Abweichungen aus der Tabelle \ref{tab:1} so wird einem klar, dass die Korrekturterme präziser die Realität beschreiben. Denn die geringeren Abweichungen implizieren, dass die theoretischen Werte genauer an den tatsächlichen Messwerten liegen.
Mit der Formel
\begin{align*}
    Re &= \frac{\rho_f v d}{\eta}
\end{align*}
wurde die Reynoldszahl für jede Kugel berechnet. Dann wurde ein Diagramm erstellt und aus den 'linearen' Anteilen eine Ausgleichsgerade erstellt. 
Diese ist in Abbildung \ref{fig:2} erkennbar.
Mit der Cursorfunktion kann man den Schnittpunkt sehr genau bestimmen:
\begin{align*}
    Re &= 0.231 \pm 0.001
\end{align*}
\subsection{Kapillarviskosimeter}
Formel \ref{eq:3} wurde nach $\eta$ umgestellt.
\begin{align*}
    \eta &= \pi \frac{\rho_f g h R^4 t}{8LV}\\
    \Delta \eta &= \eta \sqrt{(\frac{\Delta h}{h})^2+(\frac{\Delta g}{g})^2+(2\frac{\Delta R}{R})^2+(\frac{\Delta \rho_f}{\rho_f})^2+(\frac{\Delta t}{t})^2+(\frac{\Delta L}{L})^2+(\frac{\Delta \Delta V}{V})^2}
\end{align*}
Hierfür wurde das Pascal'sche Gesetz für die Druchänderung benutzt. Der Druck ist abhängig von der Wassersäule, oder der Höhe $h$. Er berechnet sich aus dieser und der Gravitationskonstante $g$
\begin{align*}
    p(h) &= \rho_f g h
\end{align*}
$t$ und $V$ sind die Messwerte. Weil sich während der Messung die Höhe der Flüssigkeit verändert hat, wurde angenommen das diese Änderung linear verlief, sodass folgendes geschlossen werden kann:
\begin{enumerate}
    \item Zum Zeitpunkt t=0 muss die Höhe $530\pm 1$ mm betragen, was der Anfangshöhe aus dem Protokoll entspricht
    \item Zum Zeitpunkt t=12.3 min muss die Höhe $526\pm 1$mm betragen (Endhöhe).
    \item Weil 5 Messwerte genommen wurde muss also die Höhe in 0.2 Schritten abnehmen, sodass die ersten beiden Bedingungen erfüllt werden
\end{enumerate}
Daher ist es sehr nacheliegend, dass die Höhe zu jedem Zeitpunkt der Messung aus der Anfangshöhe $h_a$ und der Endhöhe $h_e$ berechnet werden kann:
\begin{align*}
    h &= h_e + (h_a - h_e)q\\
    q &= [0.2, 0.4, 0.6, 0.8, 1]
\end{align*}
Das wurde für $h$ aus der erstegenannten Formel eingesetzt. Aus den insgesammt 5 Messwerten wurde der Mittelwert und dessen Fehler berechnet. Das ergab
\begin{align*}
    \eta &= 0.210 \pm 0.009 [Pa*s]
\end{align*}
Dies wurde mmit den Messwerten aus 1 verglichen. Das ergab Abweichungen von
\begin{align*}
    \sigma &= 9\\
    \sigma_{korr} &= 4
\end{align*}
Auch wurde die Reynoldszahl berechneten
\begin{align}
    Re &= \frac{\rho_f v 2 R}{\pi R^2 \eta} = \frac{2 \rho_f \Delta V}{\pi R \eta t}\\
    \Delta Re &= Re \sqrt{(\frac{\Delta \Delta V}{\Delta V})^2+(\frac{\Delta R}{R})^2+(\frac{\Delta\eta}{\eta})^2+(\frac{\Delta t}{t})^2}
\end{align}
Hierfür kann angenommen werden, dass der Fehler der Dichte vernachlässigbar ist. Weil sich die Zimmertemperatur während des Versuches nicht geändert hat, wurde dieselbe Dichte $\rho_f$ aus 1 verwendet.
Das ergab
\begin{align*}
    Re &= 0.164 \pm 0.013
\end{align*}

\section{Diskussion}
Aus den beiden Methoden ergaben sich Viskositäten von
\begin{align*}
    \eta_1 &= 0.2429\pm 0.0028 [Pa*s]\\
    \eta_2 &= 0.210\pm 0.009 [Pa*s]\\
    \sigma &= 4
\end{align*}
D.h obwohl schon der Korrekturterm $\eta_1$ verwendet wurde fällt die Abweichung signifikant aus. Weil sich die Temperatur innerhalb des Raumes nicht verändert hat, müssen Fehler bei der Zeitmessung stattgefunden haben.
Weil aber erst ab einer Distanz gemessen wurde, bei welcher man mit Sicherheit davon ausgehen konnte, dass die Kugeln gleichförmig fallen, muss es schlussendlich an unserer Betätigung der Stoppuhr gelegen haben. Hier wurde aber die Reaktionszeit innerhalb der Rechnungen berücksichtigt und eine schiefe Sicht auf die Messmarken wurde durch Beugen verhindert. Das lässt nur noch 2 Fehlerquellen zu.
Das Kugeln vermischt wurden wird hier ausgeschlossen, da darauf geachtet wurde und dies dann bedeuten würde, dass die Kugeln innerhalb der Box schon vermischt hätten sein müssen.\\
Das bedeutet, dass die systematische Fehlerquelle nur noch Luftbläschen unterhalb der Kugeln sein muss. Obwohl dies schon mit dem Mischen innerhalb des Becherglases vermieden sein sollte, könnte das dennoch passiert sein. Zuletzt kann es noch daran liegen, dass die Kugeln zu nah am Fallrohrrand fallen gelassen wurden. 
Das bot zwar die Möglichkeit genauer Messen zu können, aber bringt auch den Fehler mit sich, dass auch in Korrekturterm davon ausgegangen wird, dass die Kugel zentral im Fallrohr fällt, da aufgrund weitere Reibung am Glasrand die Geschwindigkeit zusätzlich verfälscht wird.\\
Folglich kann man den Versuch genauer gestalten, indem man mehr Messungen unternimmt und vermehrt darauf achtet, dass die Kugel im Fallrohrzentrum fallen gelassen wird.\\
Dann wurden die Reynoldszahlen bestimmt:
\begin{align*}
    Re_1 &= 0.231 \pm 0.001\\
    Re_2 &= 0.164 \pm 0.013
\end{align*}
Die erste Zahl ist weit unterhab der zu Erwartenden. Das liegt daran, dass dieser Wert mit den Geschwindigkeiten berechnet wurden und diese wie oben erwähnt verfälscht sind.
Der zweite Wert untermauert hingegen, dass es sich um eine laminare Strömung während des Versuches handelte.
Das liegt daran, dass $Re_2$ aus der Messung nach Hagen-Poiseuille deutlich unterhalb der zuvor bestimmten kritischen Reynoldszahl $Re_1$ liegt (Abweichung: $5\sigma$). Man kann also hier mit großer Sicherheit von einer laminaren Strömung ausgehen.
\section{Anhang}
\subsection{Messprotokoll}
\includepdf[pages=-]{../data/V212.pdf}
\subsection{Plots}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V212Diagramm1.pdf}
    \caption{Kugelfallviskosimeter: Messwerte}
    \label{fig:1}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V212Diagramm2.pdf}
    \caption{Kugelfallviskosimeter: Reynoldszahl}
    \label{fig:2}
\end{figure}
\subsection{Source Code}
\begin{verbatim}
    import numpy as np
    import uncertainties as unc
    import uncertainties.unumpy as unp
    import matplotlib.pyplot as plt
    import scipy.constants as const
    from scipy.optimize import curve_fit
    from scipy import odr   

    plt.style.use('classic')
    plt.rcParams['errorbar.capsize'] = 2
    plt.rcParams['font.family'] = 'serif'


def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / 
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2)


def linear_fit(x, a, b):
    return a * x + b

def log_fit(x,a,b):
    return a*np.log(x)+b


def plot_data(arg):
    plt.clf()
    plt.grid('dotted')
    if arg == 1:
        plt.errorbar(unp.nominal_values(r ** 2), 
            unp.nominal_values(v_mean / (rho_k - rho_f)), 
            xerr=unp.std_devs(r ** 2), 
            yerr=unp.std_devs(v_mean / (rho_k - rho_f)), 
            marker='.', fmt='none', color='darkred', label='Messwerte')
        
        plt.errorbar(unp.nominal_values(r_square), 
            unp.nominal_values(y2), 
            xerr=unp.std_devs(r_square), 
            yerr=unp.std_devs(y2), 
            marker='.', fmt='none', color='darkred', label='Messwerte')
        
        plt.errorbar(unp.nominal_values(r ** 2), 
            unp.nominal_values(v_mean_lam / (rho_k - rho_f)), 
            xerr=unp.std_devs(r ** 2), 
            yerr=unp.std_devs(v_mean_lam / (rho_k - rho_f)), 
            marker='.', fmt='none', color='darkblue', label='korr. Messwerte')
        x = np.linspace(unp.nominal_values(r[7] ** 2), unp.nominal_values(r[0] ** 2), 2)
        
        plt.plot(x, linear_fit(x, *popt), 
            color='green', marker=None, ls='-', label='Ausgleichsgerade')
        
        plt.plot(x, linear_fit(x, *popt2), 
            color='magenta', marker=None, ls='-', label='korr. Ausgleichsgerade')
        plt.title('Stokes: Fallgeschwindigkeiten in Abhängigkeit des Radius r')
        plt.xlabel('$r^2$ [$m^2$]', fontsize=20)
        plt.ylabel(r'$\frac{v}{\rho_k - \rho_f}$ [$\frac{m^4}{gs}$]', fontsize=20)
        plt.tight_layout()
        plt.legend(frameon=True, borderpad=0, borderaxespad=0, loc=2)
        plt.savefig('results/V212Diagramm1.pdf', format='PDF')
        plt.show()
    if arg ==2:
        plt.errorbar(unp.nominal_values(Re), 
            unp.nominal_values(v_mean/v_lam), 
            xerr=unp.std_devs(Re), 
            yerr=unp.std_devs(v_mean/v_lam), 
            fmt='none', marker='.', color='darkred', label='Messwerte')
        
        plt.errorbar(unp.nominal_values(Re_korr), 
            unp.nominal_values(v_mean/v_lam_korr), 
            xerr=unp.std_devs(Re_korr), 
            yerr=unp.std_devs(v_mean/v_lam_korr), 
            fmt='none', marker='.', color='darkblue', label='korr. Messwerte')
        x = np.linspace(1e-2, 1, 2)
        plt.plot(x, log_fit(x, *popt))
        plt.title(r'Abhängigkeit von $\frac{v}{v_{lam}}$ zu log(Re)', fontsize=22)
        plt.xlabel(r'log(Re)', fontsize=20)
        plt.ylabel(r'$\frac{v}{v_lam}$', fontsize=20)
        plt.xscale('log')
        plt.tight_layout()
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=2)
        plt.show()
    if arg == 3:
        plt.errorbar(unp.nominal_values(r_square), 
            unp.nominal_values(y2), 
            xerr=unp.std_devs(r_square), 
            yerr=unp.std_devs(y2), 
            marker='.', fmt='none', color='darkred', label='Messwerte')
        
        plt.errorbar(unp.nominal_values(r_square), 
            unp.nominal_values(y1), 
            xerr=unp.std_devs(r_square), 
            yerr=unp.std_devs(y1), 
            marker='.', fmt='none', color='darkblue', label='korr. Messwerte')
        x = np.linspace(unp.nominal_values(r_square[7]), unp.nominal_values(r_square), 2)
        
        plt.plot(x, linear_fit(x, *popt), 
            color='green', marker=None, ls='-', label='Ausgleichsgerade')
        plt.plot(x, linear_fit(x, *popt2), 
        color='magenta', marker=None, ls='-', label='korr. Ausgleichsgerade')
        plt.title('Stokes: Fallgeschwindigkeiten in Abhängigkeit des Radius r')
        plt.xlabel('$r^2$ [$m^2$]', fontsize=20)
        plt.ylabel(r'$\frac{v}{\rho_k - \rho_f}$ [$\frac{m^4}{gs}$]', fontsize=20)
        plt.tight_layout()
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=2)
        plt.show()

    return


    # 1 Fallviskosimeter
    ## Kugelreihenfolge : 9, 8, 7.144, 6, 5, 4, 3, 2 mm
    d = unp.uarray([9, 8, 7.144, 6, 5, 4, 3, 2], np.array([9, 8, 7.144, 6, 5, 4, 3, 2]) * 0.01) * const.milli
    t = unp.uarray([6.86, 6.89, 6.64, 6.89, 6.95,
                    8.68, 8.43, 8.72, 8.64, 8.31,
                    9.62, 9.68, 9.53, 9.54, 9.45,
                    12.78, 12.73, 12.92, 12.96, 12.96,
                    17.90, 17.59, 17.40, 17.43, 17.64,
                    13.34, 13.48, 13.39, 13.51, 13.61,
                    21.37, 21.31, 21.54, 21.67, 21.59,
                    45.92, 47.02, 45.58, 44.15, 44.50],
                   0.2)
    h = unp.uarray([200, 200, 200, 200, 200,
                    200, 200, 200, 200, 200,
                    200, 200, 200, 200, 200,
                    200, 200, 200, 200, 200,
                    200, 200, 200, 200, 200,
                    100, 100, 100, 100, 100,
                    100, 100, 100, 100, 100,
                    100, 100, 100, 100, 100],
                   2) * const.milli
    rho_k = unp.uarray([1.3625,
                        1.3575,
                        1.3575,
                        1.3575,
                        1.3575,
                        1.3575,
                        1.3575,
                        1.3575],
                       0.0025) * const.gram / const.centi**3
    
    rho_f = unc.ufloat(1.1487, 5e-4) * const.gram / const.centi**3 # Anhand Temperatur
    
    v_mean = unp.uarray(np.zeros(8), 0)
    for i in range(0, 7):
        v_mean[i] = sum(h[5*i:5*i+4]/t[5*i:5*i+4])/len(t[5*i:5*i+4])
    D = 75 * const.milli
    k = (1+2.1*(d/D))
    v_lam = v_mean * k
    r_square = (d/2)**2
    y1 = v_lam/(rho_k-rho_f)
    y2 = v_mean/(rho_k-rho_f)
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(r_square), 
        unp.nominal_values(y1))
    popt2, pcov2 = curve_fit(linear_fit, unp.nominal_values(r_square), 
        unp.nominal_values(y2))
    a = unc.ufloat(popt2[0], pcov2[0, 0])  # Steigung aus Messwerten ohne Korrektur
    a2 = unc.ufloat(popt[0], pcov[0, 0])   # Steigung mit Korrektur
    eta = 2 * const.g / (9 * a)
    eta2 = 2 * const.g / (9 * a2)
    
    print('\n Teil 1. Kugelfallviskosimter')
    print('Anhand der Messwerte ohne Korrektur ergibt sich eine Viskosität von ' + str(eta) + ' Pa s')
    print('Anhand der Messwerte mit Korrektur ergibt sich eine Viskosität von ' + str(eta2) + ' Pa s')
    
    ## Alternativer Weg
    r = unp.uarray([9, 8, 7.144, 6, 5, 4, 3, 2], 
        np.array([9, 8, 7.144, 6, 5, 4, 3, 2]) * 0.01) * const.milli / 2
    rho_k = unp.uarray([1.3625, 1.3575, 1.3775, 1.3775, 1.3775, 1.3775, 1.3775, 1.3775],
                       0.0025) * const.gram / const.centi ** 3
    rho_f = unc.ufloat(1.1482, 5e-4) * const.gram / const.centi ** 3  # Anhand Temperatur
    h = unp.uarray([200, 200, 200, 200, 200, 100, 100, 100], 2) * const.milli
    R = 75 / 2 * const.milli
    t_9 = unp.uarray([6.86, 6.89, 6.64, 6.89, 6.95], 0.2)
    t_8 = unp.uarray([8.68, 8.43, 8.72, 8.64, 8.31], 0.2)
    t_7 = unp.uarray([9.62, 9.68, 9.53, 9.54, 9.45], 0.2)
    t_6 = unp.uarray([12.78, 12.73, 12.92, 12.96, 12.96], 0.2)
    t_5 = unp.uarray([17.90, 17.59, 17.40, 17.43, 17.64], 0.2)
    t_4 = unp.uarray([13.34, 13.48, 13.39, 13.51, 13.61], 0.2)
    t_3 = unp.uarray([21.37, 21.31, 21.54, 21.67, 21.59], 0.2)
    t_2 = unp.uarray([45.92, 47.02, 45.58, 44.15, 44.50], 0.2)
    ## Fallgeschwindigkeiten
    v_9 = h[0] / t_9
    v_8 = h[1] / t_8
    v_7 = h[2] / t_7
    v_6 = h[3] / t_6
    v_5 = h[4] / t_5
    v_4 = h[5] / t_4
    v_3 = h[6] / t_3
    v_2 = h[7] / t_2
    ## Durchschnittsgeschwindigkeit
    v_mean = unp.uarray(np.zeros(8), 0)
    v_mean[0] = sum(v_9) / len(v_9)
    v_mean[1] = sum(v_8) / len(v_8)
    v_mean[2] = sum(v_7) / len(v_7)
    v_mean[3] = sum(v_6) / len(v_6)
    v_mean[4] = sum(v_5) / len(v_5)
    v_mean[5] = sum(v_4) / len(v_4)
    v_mean[6] = sum(v_3) / len(v_3)
    v_mean[7] = sum(v_2) / len(v_2)
    
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(r ** 2), 
        unp.nominal_values(v_mean / (rho_k - rho_f)), 
        sigma=unp.std_devs(v_mean / (rho_k - rho_f)))
    a = unc.ufloat(popt[0], pcov[0][0])
    eta = 2 * const.g / (9 * a)
    
    lam = (1 + 2.1 * r / R)
    v_mean_lam = v_mean * lam
    popt2, pcov2 = curve_fit(linear_fit, unp.nominal_values(r ** 2), 
        unp.nominal_values(v_mean_lam / (rho_k - rho_f)), 
        sigma=unp.std_devs(v_mean_lam / (rho_k - rho_f)))
    a = unc.ufloat(popt2[0], pcov[0, 0])
    eta2 = 2*const.g / (9*a)
    
    # v_lam = 2/9 * const.g * (rho_k-rho_f) / eta * r**2
    # v_lam_korr = 2/9 * const.g * (rho_k-rho_f) / eta2 * r**2
    
    v_theo = linear_fit(unp.nominal_values(r**2), *popt) * (rho_k - rho_f)
    v_theo_korr = linear_fit(unp.nominal_values(r**2), *popt2) * (rho_k - rho_f)
    plot_data(1)
    Re = rho_f * v_mean * 2 * r / eta
    Re_korr = rho_f * v_mean * 2 * r / eta2
    
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(Re[0:3]), 
        unp.nominal_values(v_mean[0:3]/v_lam[0:3]), 
        sigma=unp.std_devs(v_mean[0:3]/v_lam[0:3]))
    
    ## Feedback
    print('\n Teil 1. Kugelfallviskosimter')
    print('Genauer: Anhand der Messwerte ohne Korrektur ergibt sich eine 
        Viskosität von ' + str(eta) + ' Pa s')
    print('Genauer: Anhand der Messwerte mit Korrektur ergibt sich eine 
        Viskosität von ' + str(eta2) + ' Pa s')
    print('v_mean beträgt\n ' +str(v_mean))
    print('v_mean mit Korrektur beträgt\n ' +str(v_mean_lam))
    print('Mit der Geradensteigung ist v_mean ohne 
        Korrektur als\n ' +str(v_theo) + ' zu erwarten')
    print('Mit der Geradensteigung ist v_mean mit 
        Korrektur als\n ' +str(v_theo_korr) + ' zu erwarten')
    print('Die Abweichung ohne Korrektur 
        ergibt '+str(calc_sigma(v_theo, v_mean))+' Sigma')
    print('Die Abweichung mit Korrektur 
        ergibt '+str(calc_sigma(v_theo_korr, v_mean_lam))+' Sigma')
    print('Re beträgt ohne Korrektur '+str(Re))
    print('Re beträgt mit Korrektur '+str(Re_korr))
    
    Re = unp.nominal_values(Re)
    Fehler_Re = unp.std_devs(Re)
    breakpoint = 5
    x_fit1 = np.linspace(Re[0]*1.1, Re[breakpoint+1], 1000)
    x_fit2 = np.linspace(Re[breakpoint-3], Re[Re.size-1]/1.1, 1000)
    
    k2 = unp.nominal_values(1/k)
    Fehler_k2 = unp.std_devs(1/k)
    line1 = k2[0]+np.log(x_fit1/Re[0])*(k2[breakpoint-1]-k2[0])
        /np.log(Re[breakpoint-1]/Re[0])
    line2 = k2[breakpoint]+np.log(x_fit2/Re[breakpoint])*(k2[k2.size-1]-k2[breakpoint])
        /np.log(Re[Re.size-1]/Re[breakpoint])
    
    fig, ax = plt.subplots(1, figsize=[6.4 *2, 4.8 *2])
    plt.ticklabel_format(axis='both', style='sci', scilimits=(0,3), useMathText=True)
    plt.errorbar(Re, k2, yerr=Fehler_k2, xerr=Fehler_Re, lw=1, 
        ecolor='k', fmt='none', capsize=2, label='Messdaten')
    plt.plot(x_fit1, line1, 'r', lw=1, label='1. Gerade, turbulente Strömung')
    plt.plot(x_fit2, line2, 'g', lw=1, label='2. Gerade, laminare Strömung')
    plt.title('Bestimmung der kritischen Reynoldszahl ${Re}_{kr}$')
    plt.xscale('log')
    plt.grid(which='major')
    plt.grid(which='minor', linestyle=':')
    plt.xlabel('Re')
    plt.ylabel('$v/v_{lam}$')
    plt.legend(loc='best')
    plt.savefig('results/V212Diagramm2.pdf', format='PDF')
    plt.show()
    
    # Teil 2. Kapillarviskosimeter
    V = unp.uarray([5, 10, 15, 20, 25], 0.5) * 1e-6
    t2 = unp.uarray([2.27, 4.55, 7.29, 9.56, 12.3], 0.003) * 60
    R = unc.ufloat(1.5, 0.01) * const.milli / 2
    L = unc.ufloat(100, 0.5) * const.milli
    h_a = unc.ufloat(530, 1) * const.milli
    h_e = unc.ufloat(526, 1) * const.milli
    q = unp.uarray([0.2, 0.4, 0.6, 0.8, 1], 0)
    h_mean = h_e + (h_a - h_e)*q
    rho_f = unc.ufloat(1.1482, 0.0002) * const.gram / const.centi**3
    
    eta3 = const.pi * (rho_f*h_mean*const.g)*R**4*t2/(8*L*V)
    eta3_mean = sum(eta3) / len(eta3)
    
    Re2 = 2*rho_f*V / (t2 * const.pi * R * eta3)
    Re2_mean = sum(Re2)/(len(Re2))
    
    print('Hier beträgt die Viskosität '+str(eta3_mean)+' Pa s')
    print('Re beträgt '+str(Re2_mean))
\end{verbatim}
\end{document}