import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import chi2
import uncertainties as unc
import uncertainties.unumpy as unp

#
plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2


#

def fit_func(x, A1, l1, A2, l2):
    return A1 * np.exp(-x * l1) + A2 * np.exp(-x * l2) + y0

def exp_fit(x, A, l3):
    return A * np.exp(-l3 * x) + y0

def calc_prob(func, popt, t, N , v):
    chi2_ = np.sum((func(t, *popt) - unp.nominal_values(N))**2 / unp.std_devs(N)**2)
    dof = len(unp.std_devs(N)) - v
    chi2_red = chi2_ / dof
    prob = round(1-chi2.cdf(chi2_, dof), 2) * 100

    print('chi2 = ' + str(chi2_))
    print('chi2_red = ' + str(chi2_red))
    print('Fitwahrscheinlichkeit = ' + str(prob) +r'%')

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def plot_fit(func, t, N, popt, title, index):
    plt.clf()
    plt.grid(ls='dotted')

    plt.errorbar(t, unp.nominal_values(N), yerr=unp.std_devs(N), fmt='none', marker='.', color='darkred', label='Messwerte')
    plt.plot(t, func(t, *popt), marker=None, ls='--', color='darkblue', label='Fit-Kurve')
    plt.xlabel(r'Zeit $t$ [s]', size=17)
    plt.ylabel(r'Zerfälle', size=17)
    plt.title(title, size=20)
    plt.yscale('log')

    plt.tight_layout()
    plt.legend(frameon=True, loc='best')
    plt.savefig('results/V252Diagramm{0}.pdf'.format(index), format='PDF')
    plt.show()

def calc_result(l, l_p, l_m):
    diff_l_p = np.abs(unp.nominal_values(l) - unp.nominal_values(l_p))
    diff_l_m = np.abs(unp.nominal_values(l) - unp.nominal_values(l_m))
    diff_err = np.mean([diff_l_m, diff_l_p])
    err = np.sqrt(diff_err**2+unp.std_devs(l)**2)
    return unc.ufloat(unp.nominal_values(l), err)

def calc_silver(plotdata = False):
    global y0

    unterg = np.loadtxt('data/untergrund.txt', usecols=[1])
    mittel_unterg = unc.ufloat(np.mean(unterg * 4), np.std(unterg * 4) / np.sqrt(len(unterg)))
    N = np.zeros(40)
    for i in range(1, 5):
        N += np.loadtxt('data/Ag{0}.txt'.format(i), usecols=[1])
    N = unp.uarray(N, np.sqrt(N))
    t = np.arange(5, 405, 10)
    p0 = [50, 0.02, 50, 0.01]

    y0 = unp.nominal_values(mittel_unterg)
    popt, pcov = curve_fit(fit_func, t, unp.nominal_values(N), sigma=unp.std_devs(N), p0=p0, maxfev=5000)
    if plotdata == True : plot_fit(fit_func, t, N, popt, 'Zerfall von Silber mit Untergrund', 1)
    calc_prob(fit_func, popt, t, N, 4)

    y0 = unp.nominal_values(mittel_unterg) + unp.std_devs(mittel_unterg)
    popt_plus, pcov_plus = curve_fit(fit_func, t, unp.nominal_values(N), sigma=unp.std_devs(N), p0=p0, maxfev=5000)
    if plotdata == True : plot_fit(fit_func, t, N, popt_plus, r'Zerfall von Silber mit Untergrund ($+1\sigma$)', 2)
    calc_prob(fit_func, popt_plus, t, N, 4)

    y0 = unp.nominal_values(mittel_unterg) - unp.std_devs(mittel_unterg)
    popt_minus, pcov_minus = curve_fit(fit_func, t, unp.nominal_values(N), sigma=unp.std_devs(N), p0=p0, maxfev=5000)
    if plotdata == True: plot_fit(fit_func, t, N, popt_minus, r'Zerfall von Silber mit Untergrund ($-1\sigma$)', 3)
    calc_prob(fit_func, popt_minus, t, N, 4)

    ## Values
    A1 = unc.ufloat(popt[0], np.sqrt(pcov[0, 0]))
    l1 = unc.ufloat(popt[1], np.sqrt(pcov[1, 1]))
    A2 = unc.ufloat(popt[2], np.sqrt(pcov[2, 2]))
    l2 = unc.ufloat(popt[3], np.sqrt(pcov[3, 3]))

    A1_plus = unc.ufloat(popt_plus[0], np.sqrt(pcov_plus[0, 0]))
    l1_plus = unc.ufloat(popt_plus[1], np.sqrt(pcov_plus[1, 1]))
    A2_plus = unc.ufloat(popt_plus[2], np.sqrt(pcov_plus[2, 2]))
    l2_plus = unc.ufloat(popt_plus[3], np.sqrt(pcov_plus[3, 3]))

    A1_minus = unc.ufloat(popt_minus[0], np.sqrt(pcov_minus[0, 0]))
    l1_minus = unc.ufloat(popt_minus[1], np.sqrt(pcov_minus[1, 1]))
    A2_minus = unc.ufloat(popt_minus[2], np.sqrt(pcov_minus[2, 2]))
    l2_minus = unc.ufloat(popt_minus[3], np.sqrt(pcov_minus[3, 3]))

    ##Calc
    l1_fin = calc_result(l1, l1_plus, l1_minus)
    l2_fin = calc_result(l2, l2_plus, l2_minus)

    T_110Ag = np.log(2) / l1_fin
    T_108Ag = np.log(2) / l2_fin

    T_110Ag_lit = 24.6
    T_108Ag_lit = 2.41 * 60

    ## Feedback
    print('\nA1 = ' + str(A1))
    print('l1 = ' + str(l1) +' 1/s')
    print('A2 = ' + str(A2))
    print('l2 = ' + str(l2) +' 1/s')

    print('\nA1_plus = ' + str(A1_plus))
    print('l1_plus = ' + str(l1_plus) +' 1/s')
    print('A2_plus = ' + str(A2_plus))
    print('l2_plus = ' + str(l2_plus) +' 1/s')

    print('\nA1_minus = ' + str(A1_minus))
    print('l1_minus = ' + str(l1_minus) +' 1/s')
    print('A2_minus = ' + str(A2_minus))
    print('l2_minus = ' + str(l2_minus) +' 1/s')

    print('\nl1 final ergibt '+str(l1_fin) + ' 1/s')
    print('l2 final ergibt '+str(l2_fin) + ' 1/s')

    print('\nDaraus ergibt sich eine Halbwertszeit von 110Ag = ' + str(T_110Ag))
    print('Daraus ergibt sich eine Halbwertszeit von 108Ag = ' + str(T_108Ag))

    print('\nDer erste Fehler beträgt '+str(calc_sigma(T_110Ag, T_110Ag_lit))+ ' sigma')
    print('\nDer zweite Fehler beträgt '+str(calc_sigma(T_108Ag, T_108Ag_lit)) + ' sigma')

def calc_indium(plotdata = False):
    global y0

    unterg = np.loadtxt('data/untergrund.txt', usecols=[1])
    mittel_unterg = 3 * unc.ufloat(np.mean(unterg * 4), np.std(unterg * 4) / np.sqrt(len(unterg))) # sqrt(12*3) = sqrt(3*3*4) = sqrt(4) * 3 = 3 * un(ag)

    N2 = np.loadtxt('data/in.txt', usecols=[1])
    N2 = unp.uarray(N2, np.sqrt(N2))
    N2 = N2[1:]
    t2 = np.arange(5, 50*60, 120)
    t2 = t2[1:]

    p0 = [500, 0.0004]

    y0 = unp.nominal_values(mittel_unterg)
    popt, pcov = curve_fit(exp_fit, t2, unp.nominal_values(N2), sigma=unp.std_devs(N2), p0=p0, maxfev=5000)
    if plotdata == True : plot_fit(exp_fit, t2, N2, popt, 'Zerfall von Indium mit Untergrund', 4)
    calc_prob(exp_fit, popt, t2, N2, 2)

    y0 = unp.nominal_values(mittel_unterg) + unp.std_devs(mittel_unterg)
    popt_plus, pcov_plus = curve_fit(exp_fit, t2, unp.nominal_values(N2), sigma=unp.std_devs(N2), p0=p0, maxfev=5000)
    if plotdata == True : plot_fit(exp_fit, t2, N2, popt_plus, r'Zerfall von Indium mit Untergrund ($+1\sigma$)', 5)
    calc_prob(exp_fit, popt_plus, t2, N2, 2)

    y0 = unp.nominal_values(mittel_unterg) - unp.std_devs(mittel_unterg)
    popt_minus, pcov_minus = curve_fit(exp_fit, t2, unp.nominal_values(N2), sigma=unp.std_devs(N2), p0=p0, maxfev=5000)
    if plotdata == True: plot_fit(exp_fit, t2, N2, popt_minus, r'Zerfall von Indium mit Untergrund ($-1\sigma$)', 6)
    calc_prob(exp_fit, popt_minus, t2, N2, 2)

    ##Values
    N0 = unc.ufloat(popt[0], np.sqrt(pcov[0, 0]))
    l3 = unc.ufloat(popt[1], np.sqrt(pcov[1, 1]))

    N0_plus = unc.ufloat(popt_plus[0], np.sqrt(pcov_plus[0, 0]))
    l3_plus = unc.ufloat(popt_plus[1], np.sqrt(pcov_plus[1, 1]))

    N0_minus = unc.ufloat(popt_minus[0], np.sqrt(pcov_minus[0, 0]))
    l3_minus = unc.ufloat(popt_minus[1], np.sqrt(pcov_minus[1, 1]))

    ##Calc
    l3_fin = calc_result(l3, l3_plus, l3_minus)

    T_116ln = np.log(2)/l3_fin
    T_116ln_lit = 54 * 60


    ## Feedback
    print('\nN0 = ' + str(N0))
    print('l3 = ' + str(l3) +' 1/s')
    print('\nN0_plus = ' + str(N0_plus))
    print('l3_plus = ' + str(l3_plus) +' 1/s')
    print('\nN0_minus = ' + str(N0_minus))
    print('l3_minus = ' + str(l3_minus) +' 1/s')

    print('\nl3 final ergibt '+str(l3_fin) + ' 1/s')
    print('Daraus folgt eine Halbwertszeit von '+str(T_116ln) + ' 1/s')
    print('D.h eine Abweichung von '+str(calc_sigma(T_116ln, T_116ln_lit)) + ' sigma')

# 1. Halbwertszeit von Silber
print('\n1. Halbwertszeit von Silber')
calc_silver(True)
# 2. Halbwertszeit von Indium
print('\n2. Halbwertszeit von Indium')
calc_indium(True)