\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {subsection}{\numberline {1.1}Aufgaben und Aufbau}{1}{}%
\contentsline {subsection}{\numberline {1.2}Theorie}{1}{}%
\contentsline {subsection}{\numberline {1.3}Durchführung}{2}{}%
\contentsline {subsection}{\numberline {1.4}Halbwertszeit von Silber}{2}{}%
\contentsline {subsection}{\numberline {1.5}Halbwertszeit von Indium}{2}{}%
\contentsline {subsection}{\numberline {1.6}Auswertung}{3}{}%
\contentsline {subsection}{\numberline {1.7}Halbwertszeit von Silber}{3}{}%
\contentsline {subsection}{\numberline {1.8}Halbwertszeit von Indium}{4}{}%
\contentsline {subsection}{\numberline {1.9}Diskussion}{4}{}%
\contentsline {subsection}{\numberline {1.10}Anhang}{5}{}%
\contentsline {subsection}{\numberline {1.11}Source Code}{5}{}%
\contentsline {subsection}{\numberline {1.12}Plots}{10}{}%
