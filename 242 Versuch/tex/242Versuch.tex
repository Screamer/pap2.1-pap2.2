\documentclass{article}

\usepackage[ngerman]{babel}
\usepackage{amsmath, amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{wrapfig}
\usepackage{pdfpages}
\usepackage{xparse}
\usepackage[utf8]{inputenc}
\usepackage{listings}

\pagestyle{fancy}
\fancyhf{}
\rhead{mm268}
\lhead{November 2021}
\cfoot{\thepage}

\begin{document}
\begin{titlepage}
    \title{Versuch 242 \\ Spannungsverstärkung}
    \author{Jan Bartels}
    \date{November 2021}
    \maketitle
    \tableofcontents
\end{titlepage}
\section{Einleitung}
In diesem Versuch wird ein Operationsverstärker auf seine charakteristischen Eigenschaften untersucht. Die Ausgangsspannung wird unter der Variation der Eingangsspannung und des 
Widerstandes aufgenommen.
\subsection{Aufgaben und Aufbau}
Infolgedessen wurde folgendes aufgetragen:
\begin{enumerate}
    \item Messen der Ausgangsspannung in Abhängigkeit der Eingangsspannung bei Gleich- und Wechselspannung
    \item Messen des Frequenzganges für verschiedene Gegenkopplungen in einem vorgegebenen Intervall
    \item Beschreiben des Einflusses des Frequenzganges auf eine Rechteckfunktion
\end{enumerate}
Für den Messaufbau bracht man lediglich
\begin{itemize}
    \item 1 Schaltungskästchen mit Operationsverstärker
    \item 1 Sinus-Rechteck-Generator
    \item 1 Zweikanaloszillograph
\end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{../data/aufbau.png}
    \caption{Aufbau und Schaltkreis des Versuchs}
    \label{fig:1}
\end{figure}
Dieses Bild wurde aus dem von Dr. J. Wagner erstellen Skript 'Physikalisches Anfängerpraktikum' entnommen. Der dargestellte Schaltkreis entspricht dem eines Spannungsverstärkers.

\section{Theorie}
Ein Operationsverstärker besitzt zwei Eingänge und einen Ausgang für die Spannung. Bei den Eingängen unterscheidet man den Invertierenden und den Nicht-Invertierenden. Der erste ist
in Abbildung \ref{fig:1} mit einem Minus gekennzeichnet, der zweite mit einem Plus. Ein Spannungsverstärker verstärkt, wie der Name schon impliziert, die Eingangsspannung um einen Faktor $v_0$.
 Wichtig ist hier, dass die Ausgangsspannung $U_A$ also das Produkt aus $v_0$ und der Differenz der jeweiligen Eingangsspannungen $U^+, U^-$ ist:
 \begin{align}
     U_A &= v_0(U^+-U^-)
 \end{align}
In diesem Aufbau wird weiterhin ein Gegenkopplungswiderstand $R_G$ verwendet. Dieser führt dazu, dass der Operationsverstärker sich so einregeln wird, sodass die Differenzspannung verschwindet. 
Hierbei handelt es sich um einen invertierenden Versärker. In dem Versuch wird die Spannung $U^+$ auf Masse gelegt, sodass man die Schaltung umformen kann:
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../data/schaltung.png}
    \label{fig:2}
\end{figure}
Mithilfe der Kirchhoffschen Regeln lässt sich die Ausgangsspannung berechnen
\begin{align}\label{eq:1}
    I_+ &= \frac{U_E}{R_E}\\
    I_A &= -\frac{U_A}{R_A}\\
    I_A &= I_+\\
    U_A &= -\frac{R_G}{R_E}U_E\\
    v_0 &= \frac{R_G}{R_E} = \frac{U_A}{U_E}
\end{align}
Das die resultierende Spannung negativ ist, begründet den Namen 'invertierender Verstärker'.
Bei Wechselspannung ist die Verstärkung jedoch abhängig von der Frequenz und schlussendlich werden größere Frequenzen schwächer verstärkt.
Man kann diesen Effekt mit einem parallel zum Widerstand $R_G$ geschalteten Kondensator verstärken.

\section{Durchführung und Messprotokoll}
Alle Messwerte sind innerhalb des Messprotokolls im Anhang zu finden.
\subsection{Gleichspannungsverstärker}
Der Operationsverstärker wurde mit dem Oszillographen verbunden. Für verschiedene Gegenkopplungswiderstand wurde die Ausgangsspannung unter Variation der Eingangsspannung gemessen.
\subsection{Verstärker für Wechselspannung}
Der Frequenzgenerator wurde an den Ausgang $U_G$ aus Abbildung \ref{fig:1} angeschlossen. Dann wurden die Kanäle des Oszillographen entsprechend verbunden, sodass die Eingangsspannung und Ausgangsspannung sichtbar war.
Für zwei verschiedene Gegenkoppelwiderstände wurde unter Variation der Eingansspannung bei konstanter Frequenz (1kHz) die Ausgangsspannung gemessen.
\subsection{Frequenzgang des Verstärkungsfaktors}
Diesmal wurde die Eingansspannung konstant gehalten und die Frequenz am Frequenzgenerator verändert. Nach jeder Messung wurde der Gegenkopplungswiderstand wiederum variiert.
Es wurde auch die Parallelschaltung aus Widerstand und Kondensator untersucht. Zuletzt wurde noch der Schalter S1 aus Abbildung \ref{fig:1} auf Stellung 3 gebracht. Hier wurde auch analog gemessen. Der Kopplungswiderstand wurde auf $48.7k\Omega$ zurückgestellt.
\subsection{Untersuchung eines Rechtecksignals}
Ein Rechtecksignal wird an den Verstärker angeschlossen und die Impulsform unter verschiedenen Widerständen betrachtet.

\section{Auswertung}
\subsection{Gleichspannungsverstärker}
Für die beiden Messungen wurden Diagramme erstellt. 
Betrachte hierfür Abbildung \ref{fig:3} und \ref{fig:4}.
Anhand der Messwerte wurde eine Ausgleichsgerade gezeichnet und die Steigung berechnet. Die theoretische Verstärkung wurde wie folgt berechnet:
\begin{align}
    v^{theo} &= \frac{R_G}{R_E}\\
    \frac{\Delta v}{v^{theo}} &= \sqrt{(\frac{\Delta R_G}{R_G})^2+(\frac{\Delta R_E}{R_E})^2}
\end{align}
Der Fehler der Widerstände wurde als 5\% angenommen. Das ergibt die theoretischen Widerstände 
\begin{align*}
    R &= 48.7k\Omega    &   v^{theo} &= 16.2\pm 1.1\\
    R &= 274k\Omega &   v^{theo} &= 91\pm 6 
\end{align*}
Aus den Ausgleichsgeraden wurde die Steigung als Verstärkung genommen. Das ergibt sich aus Gleichung \ref{eq:1}.
\begin{align*}
    R &= 48.7k\Omega    &   v^{exp} &= 16.298\pm 0.008\\
    R &= 274k\Omega     &   v^{exp} &= 90.6\pm 0.6
\end{align*}
Das ergibt eine Abweichung von
\begin{align*}
    \sigma(R=48.7k\Omega) &= 0.06\\
    \sigma(R=274k\Omega) &= 0.12
\end{align*}
Diese außerordentlich geringen Abweichungen weisen auf eine sehr genaue Messung hin und auf geringe unbeachtete Fehlerquellen.

\subsection{Verstärker für Wechselspannung}
Hier wurde dasselbe gemacht, nur das einmal mit einem Widerstand von $680k\Omega$ gemessen wurde. Die theoretischen Verstärkungen betragen
\begin{align*}
    v^{theo}(R=274k\Omega) &= 91\pm 6\\
    v^{theo}(R=680k\Omega) &= 227\pm 16
\end{align*}
Wobei der erste Wert nicht überraschend ist, da derselbe Widerstand aus 1 verwendet wurde.
Auch hier wurden die Messwerte extrapoliert. Die dazugehörigen Diagramme sind Abbildung \ref{fig:5} und \ref{fig:6}.
Aus den Ausgleichsgeraden ergaben sich Verstärkungen und Abweichungen von
\begin{align*}
    v^{exp}(R=274k\Omega)   &= 69.36\pm 0.07    &   \sigma &= 3.5\\
    v^{exp}(R=680k\Omega)   &= 172.2\pm 1.5     &   \sigma &= 3.4
\end{align*}
Hier fallen die Abweichungen signifikant aus. D.h. es wurde entweder falsch gerechnet oder gemessen. Um mit Sicherheit die Fehlerquelle ausfindig machen zu können wurden die 
Messwerte aus dem von Dr. J. Wagner hochgeladenen Video übernommen und getestet. Da diese hier nicht aufgenommen wurden wird nur erwähnt, dass sich aus denen Verstärkungen berechnen lassen, welche deutlich präziser sind und näher an den Messwerten liegen.
Das bedeutet für diesen Versuch, dass ein Messfehler auftrat.

\subsection{Frequenzgang des Verstärkungsfaktors}
Die Messwerte wurden übernommen und alle in einem Diagramm (Abbildung \ref{fig:7}) zusammengefasst. Betrachtet man dieses, fällt zuerst auf, dass sich die Messungen nicht mit den Theoretisch vorhergesagten gleichen.
Betrachte daher alle einzelnt.\\
Anhand der Kurve zur Messung von 2c) (C=47nF) lässt sich der Hochpassfilter erkennen. Denn die Spannung nimmt für höhere Frequenzen zu. Dies wurde auch so erwartet.\\
In der Messung zu 2b), also mit Parallelschaltung aus Widerstand und Kondensator, ist deutlich die Ebene erkennbar und der darauffolgende lineare Abstieg. Vergleicht man dies mit den Messwerten ohne Kondensator aber mit demselben Widerstand, so erkennt man, dass die Steigung größer ist.
Das liegt daran, dass der Gesamtwiderstand der Schaltung unterschiedlich ist.\\
Dann sind noch die Kurven zu den Messungen aus 2a) zu betrachten. Hier kann man nicht erkennen, dass alle negativen Steigungen für hohe Frequenzen dieselben werden. Zu erwarten wäre, dass die Geraden für hohe Frequenzen zusammen konvergieren. Die schwarze Kennlinie sollte sich nur um eine konstante Verschiebung unterscheiden.\\
Der grobe Verlauf ist erkennbar, jedoch kann man auch anhand der Unstetigkeit rückschließen, dass ein Messfehler aufgetreten sein muss.  
\subsection{Untersuchung eines Rechtecksignals}
Die Beobachtungen wurden in das Messprotokoll geschrieben. Es lässt sich abschließend behaupten, dass die empirischen Ergebnisse sich mit den Theoretischen gleichen.
Denn ein Rechtecksignal ist durch die Fouriertransformation als Summe aus periodischen Funktionen darstellbar. Die Funktionen niedriger Frequenzen sind für die grobe Gestalt und Amplitude zuständig, während die höherer Frequenzen für die Kantenschärfe des Rechtecksignals verantwortlich sind.
Mit einem Tiefpassfilter, also einer Reihenschaltung aus Widerstand und Kondensator, werden höhere Frequenzen gefiltert und das bedeutet, dass die Kanten unschärfer werden. Dies wurde auch so beobachtet. Für einen Hochpassfilter gilt dasselbe nur für niedrige Frequenzen, weshalb die grobe Form verloren geht.
\section{Diskussion}
Zuerst wurde der Gleichspannungsverstärker untersucht. Zusammenfassend ergab sich 
\begin{table}[ht]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        Widerstand [k$\Omega$]  &   theo. Verstärkung   &   exp. Verstärkung    &   Abweichung\\
        \hline
        48.7    &   $16.2\pm 1.1$ &   $16.298\pm 0.008$ &   0.06\\
        274 &   $91\pm 6$ &   $90.6\pm 6$   &   0.12\\
        \hline 
    \end{tabular}
    \caption{Zusammenfassung aus 1a}
\end{table}
\\Hier wurde aus den geringen Abweichungen auf eine geringe unbeachtete Fehlerquelle geschlossen.
Dann wurde Welchselspannung angelegt und auch hier die Verstärkung gemessen. Das ergab
\begin{table}[ht]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        Widerstand [k$\Omega$]  &   theo. Verstärkung   &   exp. Verstärkung    &   Abweichung\\
        \hline
        274 &   $91\pm 6$ &   $69.36\pm 0.07$   &   3.5\\
        680 &   $277\pm 17$ &   $172.2\pm 1.5$  &   3.4\\
        \hline 
    \end{tabular}
    \caption{Zusammenfassung aus 1b}
\end{table}
Hier muss ein systematischer Fehler aufgetreten sein, da die Unsicherheiten signifikant sind. Weil eine Steigung berechnet wurde, kann ein konstantes Offset nicht für den Fehler verantwortlich sein. Die Widerstände der Kabel darf man vernachlässigen, was aus den Messungen aus 1a) resultiert.
Weil beide Unsicherheiten ähnlich sind, kann man weiterhin ausschließen, dass die falsche Schaltereinstellung gewählt wurde. Folglich muss der Fehler beim Ablesen der Daten am Oszillographen passiert sein.\\
Zuletzt wurde die Frequenzabhängigkeit untersucht. Im Diagramm ist deutlich sichtbar wie für höhere Frequenzen die Verstärkung abnimmt. Trotzdem ist nicht erkennbar, dass die Messwerte für hohe Messwerte zueinander konvergieren müssen, da die Abnahme gleich sein sollte.\\
Um diesen Versuch genauer zu gestalten bietet es sich nicht an die Anzahl an Messwerten zu erhöhen um den systematischen Fehler zu verringern. Das kann man daraus schließen, dass die Messungen zu 1b) auf eine Gerade hindeuten mit sehr geringer Unsicherheit. Weil die Messungen jedoch falsch sein mussten, liegt das nicht an Schwankungen.\\
Daher muss eine Verringerung der statistischen Fehler erfolgen. Hier würde der Hinweis reichen, dass man die Spannungswerte des Oszillographen und Frequenzgenerator vergleichen soll bevor man misst. Denn würden sich die Werte eindeutig voneinander unterscheiden, so wüsste man, dass ein Fehler vorliegt.
\section{Anhang}
\subsection{Messprotokoll}
\includepdf{../data/protokoll.pdf}
\subsection{Source Code}
Hier wird der Code hinterlegt. Um ihn genauer zu betrachten und ausführen zu können, wird auch die Datei übergeben.
\begin{verbatim}[b]
    import numpy as np
    import uncertainties as unc
    import uncertainties.unumpy as unp
    import scipy.constants as const
    import matplotlib.pyplot as plt
    from scipy.optimize import curve_fit
    
    plt.style.use('classic')
    plt.rcParams['errorbar.capsize'] = 2
    plt.rcParams['font.family'] = 'serif'
    pic = 1
    
    def linear_fit(x, a, b):
        return a * x + b
    
    
    def calc_sigma(x, y):
        return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) 
            / np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2)

    R_E = unc.ufloat(3, 3 * 0.05) * const.kilo
    # 1 Gleichspannungsverstärker
    R_G = unc.ufloat(48.7, 48.7 * 0.05) * const.kilo
    R_theo_1 = R_G / R_E
    U_E = np.linspace(-0.25, 0.25, 11)
    U_E = unp.uarray(U_E, 0.01)
    U_A = unp.uarray([4.2, 3.39, 2.59, 1.72, 0.87, 0.013, -0.69, -1.48, -2.29, -3.12, -4], 
        0.1)
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E), 
        unp.nominal_values(U_A), sigma=unp.std_devs(U_A))
    if pic == 1: plot_data(1)
    R_exp_1 = -1 * unc.ufloat(popt[0], pcov[0][0])
    R_G = unc.ufloat(274, 274 * 0.05) * const.kilo
    R_theo_2 = R_G / R_E
    U_A = unp.uarray([14.4, 14.4, 13.8, 9.35, 4.61, 0.0195, -4.49, -8.68, 
        -12.9, -13.7, -13.7], 0.1)
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E[2:8]), 
        unp.nominal_values(U_A[2:8]), sigma=unp.std_devs(U_A[2:8]))
    R_exp_2 = -1 * unc.ufloat(popt[0], pcov[0, 0])
    if pic == 1: plot_data(2)

    ## Feedback
    print('\n Widerstand 48.7kOhm')
    print('Die theoretische Verstärkung beträgt ' + str(R_theo_1))
    print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_1))
    print('Der Fehler beträgt ' + str(calc_sigma(R_exp_1, R_theo_1)) + ' Sigma')

    print('\n Widerstand 274kOhm')
    print('Die theoretische Verstärkung beträgt ' + str(R_theo_2))
    print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_2))
    print('Der Fehler beträgt ' + str(calc_sigma(R_exp_2, R_theo_2)) + ' Sigma')

    # 2 Wechselspannung
    R_G = unc.ufloat(274, 274 * 0.05) * const.kilo
    U_E = unp.uarray([0.01, 0.02, 0.04, 0.06, 0.08, 0.1], 0.001)
    U_A = unp.uarray([0.76, 1.48, 2.84, 4.20, 5.64, 7], 
        [0.05, 0.1, 0.2, 0.2, 0.2, 0.2])
    #U_A = unp.uarray([0.88, 1.7, 3.5, 5.2, 7.0, 8.8], 
        [0.05, 0.1, 0.2, 0.2, 0.2, 0.2])
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E), 
        unp.nominal_values(U_A), sigma=unp.std_devs(U_A))
    if pic == 1: plot_data(3)
    R_exp_3 = unc.ufloat(popt[0], pcov[0, 0])
    R_G = unc.ufloat(680, 680 * 0.05) * const.kilo
    R_theo_3 = R_G / R_E
    U_A = unp.uarray([1.8, 3.48, 6.88, 10.2, 13.8, 17.4],
    [0.1, 0.2, 0.2, 0.2, 0.2, 0.2])

    popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E), 
        unp.nominal_values(U_A), sigma=unp.std_devs(U_A))
    R_exp_4 = unc.ufloat(popt[0], pcov[0, 0])
    if pic == 1: plot_data(4)
    ## Feedback
    print('\n Wechselspannung Widerstand 274kOhm')
    print('Die theoretische Verstärkung beträgt ' + str(R_theo_2))
    print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_3))
    print('Der Fehler beträgt ' + str(calc_sigma(R_exp_3, R_theo_2)) + ' Sigma')
    
    print('\n Wechselspannung Widerstand 680kOhm')
    print('Die theoretische Verstärkung beträgt ' + str(R_theo_3))
    print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_4))
    print('Der Fehler beträgt ' + str(calc_sigma(R_exp_4, R_theo_3)) + ' Sigma')
    
    # 3 Frequenzgang
    f = unp.uarray([0.1, 0.3, 0.6, 1, 3, 6, 10, 30, 60, 100, 300],
        np.array([0.1, 0.3, 0.6, 1, 3, 6, 10, 30, 60, 100, 300]) * 0.01) 
            * const.kilo
    ## Messwerte zu R = 688kOhm
    R_G = unp.uarray(680, 680 * 0.05) * const.kilo
    U_G = 0.3
    U_A_1 = unp.uarray([5.28, 5.36, 5.36, 5.2, 4.4, 2.96, 2.08, 
        0.8, 0.6, 0.035, 0.031], 0.1) / U_G
    ## Messwerte für R = 274kOhm
    R_G_2 = unc.ufloat(274, 274 * 0.05) * const.kilo
    U_G_2 = 0.3
    U_A_2 = unp.uarray([2.06, 2.12, 2.12, 2.14, 2.04, 1.82, 1.5, 1.2, 1, 0.8, 0.6],
                       np.array([0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1, 0.1, 0.1])) / U_G_2
    ## Messwerte für R = 48.7kOhm
    R_G_3 = unc.ufloat(48.7, 48.7 * 0.05) * const.kilo
    U_G_3 = 1
    U_A_3 = unp.uarray([1.24, 1.24, 1.24, 1.24, 1.25, 1.25, 
        1.22, 1.09, 0.84, 0.62, 0.24], 0.1) / U_G_3
    ## Messwerte für R = 48.7kOhm und C=560pF
    R_G_4 = R_G_3
    U_G_4 = U_G_3
    U_A_4 = unp.uarray([1.24, 1.24, 1.23, 1.23, 1.15, 0.94, 
        0.7, 0.28, 0.15, 0.094, 0.032], 0.1) / U_G_4
    ## Messwerte für R = 48.7kOhm und C=47nF
    R_G_5 = R_G_3
    U_G_5 = U_G_3
    f_2 = unp.uarray([0.3, 0.6, 1, 3, 6, 10, 20], 
        np.array([0.3, 0.6, 1, 3, 6, 10, 20]) * 0.01) * const.kilo
    U_A_5 = unp.uarray([0.41, 0.68, 0.92, 1.21, 1.25, 1.23, 1.19], 0.05) / U_G_5
    plot_data(5)
\end{verbatim}
\subsection{Plots}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V242Diagramm1.pdf}
    \caption{Gleichspannungsverstärker R = $48.7k\Omega$}
    \label{fig:3}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V242Diagramm2.pdf}
    \caption{Gleichspannungsverstärker R = $274k\Omega$}
    \label{fig:4}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V242Diagramm3.pdf}
    \caption{Wechselspannungsverstärker R = $274k\Omega$}
    \label{fig:5}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V242Diagramm4.pdf}
    \caption{Wechselspannungsverstärker R = $680k\Omega$}
    \label{fig:6}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V242Diagramm5.pdf}
    \caption{Wechselstromverstärker in Abh. der Frequenz}
    \label{fig:7}
\end{figure}
\end{document}