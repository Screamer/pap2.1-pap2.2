\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {subsection}{\numberline {1.1}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {2}Theorie}{2}{}%
\contentsline {section}{\numberline {3}Durchführung und Messprotokoll}{3}{}%
\contentsline {subsection}{\numberline {3.1}Gleichspannungsverstärker}{3}{}%
\contentsline {subsection}{\numberline {3.2}Verstärker für Wechselspannung}{3}{}%
\contentsline {subsection}{\numberline {3.3}Frequenzgang des Verstärkungsfaktors}{3}{}%
\contentsline {subsection}{\numberline {3.4}Untersuchung eines Rechtecksignals}{3}{}%
\contentsline {section}{\numberline {4}Auswertung}{3}{}%
\contentsline {subsection}{\numberline {4.1}Gleichspannungsverstärker}{3}{}%
\contentsline {subsection}{\numberline {4.2}Verstärker für Wechselspannung}{4}{}%
\contentsline {subsection}{\numberline {4.3}Frequenzgang des Verstärkungsfaktors}{5}{}%
\contentsline {subsection}{\numberline {4.4}Untersuchung eines Rechtecksignals}{5}{}%
\contentsline {section}{\numberline {5}Diskussion}{5}{}%
\contentsline {section}{\numberline {6}Anhang}{6}{}%
\contentsline {subsection}{\numberline {6.1}Messprotokoll}{6}{}%
\contentsline {subsection}{\numberline {6.2}Source Code}{8}{}%
\contentsline {subsection}{\numberline {6.3}Plots}{10}{}%
