import numpy as np
import uncertainties as unc
import uncertainties.unumpy as unp
import scipy.constants as const
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

plt.style.use('classic')
plt.rcParams['errorbar.capsize'] = 2
plt.rcParams['font.family'] = 'serif'
pic = 0 #Setze 1 wenn Diagramme gespeichert werden sollen

def linear_fit(x, a, b):
    return a * x + b


def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2)


def plot_data(arg):
    if arg == 1:
        plt.clf()
        plt.errorbar(unp.nominal_values(U_E), unp.nominal_values(U_A), xerr=unp.std_devs(U_E), yerr=unp.std_devs(U_A),
                     fmt='none', marker='.', color='darkred', label='Messwerte für R=48.7k$\Omega$')
        plt.plot(np.linspace(-0.25, 0.25, 2), linear_fit(np.linspace(-0.25, 0.25, 2), *popt), ls='-', color='darkblue',
                 marker=None, label='Ausgleichsgerade')
        plt.title('Ergebnisse aus Messung 1 mit Widerstand 48.7k$\Omega$')
        plt.ylabel('Ausgansspannung [V]')
        plt.xlabel('Eingansspannung [V]')
        plt.ylim(-5, 5)
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=1)
        plt.savefig('results/V242Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.clf()
        plt.errorbar(unp.nominal_values(U_E), unp.nominal_values(U_A), xerr=unp.std_devs(U_E), yerr=unp.std_devs(U_A),
                     fmt='none', marker='.', color='darkred', label='Messwerte für R=274k$\Omega$')
        x = np.linspace(-0.15, 0.15, 2)
        plt.plot(x, linear_fit(x, *popt), ls='-', color='darkblue', marker=None, label='Ausgleichsgerade')
        plt.title('Ergebnisse aus Messung 1 mit Widerstand 274k$\Omega$')
        plt.ylabel('Ausgansspannung [V]')
        plt.xlabel('Eingansspannung [V]')
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=1)
        plt.savefig('results/V242Diagramm2.pdf', format='PDF')
    if arg == 3:
        plt.clf()
        plt.errorbar(unp.nominal_values(U_E), unp.nominal_values(U_A), xerr=unp.std_devs(U_E), yerr=unp.std_devs(U_A),
                     fmt='none', marker='.', color='darkred', label='Messwerte für R=274k$\Omega$')
        x = np.linspace(0, 0.12, 2)
        plt.plot(x, linear_fit(x, *popt), ls='-', color='darkblue', marker=None, label='Ausgleichsgerade')
        plt.title('Wechselspannung R=274k$\Omega$, f=1Hz')
        plt.ylabel('Ausgansspannung [V]')
        plt.xlabel('Eingansspannung [V]')
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=4)
        plt.savefig('results/V242Diagramm3.pdf', format='PDF')
    if arg == 4:
        plt.clf()
        plt.errorbar(unp.nominal_values(U_E), unp.nominal_values(U_A), xerr=unp.std_devs(U_E), yerr=unp.std_devs(U_A),
                     fmt='none', marker='.', color='darkred', label='Messwerte für R=680k$\Omega$')
        x = np.linspace(0, 0.12, 2)
        plt.plot(x, linear_fit(x, *popt), ls='-', color='darkblue', marker=None, label='Ausgleichsgerade')
        plt.title('Wechselspannung R=680k$\Omega$, f=1Hz')
        plt.ylabel('Ausgansspannung [V]')
        plt.xlabel('Eingansspannung [V]')
        plt.legend(frameon=True, borderpad=1, borderaxespad=1, loc=4)
        plt.savefig('results/V242Diagramm4.pdf', format='PDF')
    if arg == 5:
        plt.clf()
        plt.errorbar(unp.nominal_values(f), unp.nominal_values(U_A_1), xerr=unp.std_devs(f), yerr=unp.std_devs(U_A_1), ecolor='r', marker='.', fmt='--r', label='2a) 680k$\Omega$')
        plt.errorbar(unp.nominal_values(f), unp.nominal_values(U_A_2), xerr=unp.std_devs(f), yerr=unp.std_devs(U_A_2), ecolor='g', marker='.', fmt='--g', label='2a) 274k$\Omega$')
        plt.errorbar(unp.nominal_values(f), unp.nominal_values(U_A_3), xerr=unp.std_devs(f), yerr=unp.std_devs(U_A_3), ecolor='b', marker='.', fmt='--b', label='2a) 48.7k$\Omega$')
        plt.errorbar(unp.nominal_values(f), unp.nominal_values(U_A_4), xerr=unp.std_devs(f), yerr=unp.std_devs(U_A_4), ecolor='k', marker='.', fmt='--k', label='2b) 48.7k$\Omega$, C=560pF')
        plt.errorbar(unp.nominal_values(f_2), unp.nominal_values(U_A_5), xerr=unp.std_devs(f_2), yerr=unp.std_devs(U_A_5), ecolor='m', fmt='--m', label='2c) 48.7k$\Omega$, C=47nF')
        plt.title('Messung unter Variation der Frequenz')
        plt.ylabel('U_A/U_E')
        plt.xlabel('Frequenz [Hz]')
        plt.yscale('log')
        plt.xscale('log')
        plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=3)
        plt.savefig('results/V242Diagramm5.pdf', format='PDF')
    plt.tight_layout()
    plt.grid('dotted')

    plt.show()
    return


R_E = unc.ufloat(3, 3 * 0.05) * const.kilo
# 1 Gleichspannungsverstärker
R_G = unc.ufloat(48.7, 48.7 * 0.05) * const.kilo
R_theo_1 = R_G / R_E
U_E = np.linspace(-0.25, 0.25, 11)
U_E = unp.uarray(U_E, 0.01)
U_A = unp.uarray([4.2, 3.39, 2.59, 1.72, 0.87, 0.013, -0.69, -1.48, -2.29, -3.12, -4], 0.1)
popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E), unp.nominal_values(U_A), sigma=unp.std_devs(U_A))
if pic == 1: plot_data(1)
R_exp_1 = -1 * unc.ufloat(popt[0], pcov[0][0])
R_G = unc.ufloat(274, 274 * 0.05) * const.kilo
R_theo_2 = R_G / R_E
U_A = unp.uarray([14.4, 14.4, 13.8, 9.35, 4.61, 0.0195, -4.49, -8.68, -12.9, -13.7, -13.7], 0.1)
popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E[2:8]), unp.nominal_values(U_A[2:8]),
                       sigma=unp.std_devs(U_A[2:8]))
R_exp_2 = -1 * unc.ufloat(popt[0], pcov[0, 0])
if pic == 1: plot_data(2)

## Feedback
print('\n Widerstand 48.7kOhm')
print('Die theoretische Verstärkung beträgt ' + str(R_theo_1))
print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_1))
print('Der Fehler beträgt ' + str(calc_sigma(R_exp_1, R_theo_1)) + ' Sigma')

print('\n Widerstand 274kOhm')
print('Die theoretische Verstärkung beträgt ' + str(R_theo_2))
print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_2))
print('Der Fehler beträgt ' + str(calc_sigma(R_exp_2, R_theo_2)) + ' Sigma')

# 2 Wechselspannung
R_G = unc.ufloat(274, 274 * 0.05) * const.kilo
U_E = unp.uarray([0.01, 0.02, 0.04, 0.06, 0.08, 0.1], 0.001)
U_A = unp.uarray([0.76, 1.48, 2.84, 4.20, 5.64, 7], [0.05, 0.1, 0.2, 0.2, 0.2, 0.2])
#U_A = unp.uarray([0.88, 1.7, 3.5, 5.2, 7.0, 8.8], [0.05, 0.1, 0.2, 0.2, 0.2, 0.2])
popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E), unp.nominal_values(U_A), sigma=unp.std_devs(U_A))
if pic == 1: plot_data(3)
R_exp_3 = unc.ufloat(popt[0], pcov[0, 0])
R_G = unc.ufloat(680, 680 * 0.05) * const.kilo
R_theo_3 = R_G / R_E
U_A = unp.uarray([1.8, 3.48, 6.88, 10.2, 13.8, 17.4], [0.1, 0.2, 0.2, 0.2, 0.2, 0.2])

popt, pcov = curve_fit(linear_fit, unp.nominal_values(U_E), unp.nominal_values(U_A), sigma=unp.std_devs(U_A))
R_exp_4 = unc.ufloat(popt[0], pcov[0, 0])
if pic == 1: plot_data(4)
## Feedback
print('\n Wechselspannung Widerstand 274kOhm')
print('Die theoretische Verstärkung beträgt ' + str(R_theo_2))
print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_3))
print('Der Fehler beträgt ' + str(calc_sigma(R_exp_3, R_theo_2)) + ' Sigma')

print('\n Wechselspannung Widerstand 680kOhm')
print('Die theoretische Verstärkung beträgt ' + str(R_theo_3))
print('Anhand der Messwerte ergibt sich eine Verstärkung von ' + str(R_exp_4))
print('Der Fehler beträgt ' + str(calc_sigma(R_exp_4, R_theo_3)) + ' Sigma')

# 3 Frequenzgang
f = unp.uarray([0.1, 0.3, 0.6, 1, 3, 6, 10, 30, 60, 100, 300],
               np.array([0.1, 0.3, 0.6, 1, 3, 6, 10, 30, 60, 100, 300]) * 0.01) * const.kilo
## Messwerte zu R = 688kOhm
R_G = unp.uarray(680, 680 * 0.05) * const.kilo
U_G = 0.3
U_A_1 = unp.uarray([5.28, 5.36, 5.36, 5.2, 4.4, 2.96, 2.08, 0.8, 0.6, 0.035, 0.031], 0.1) / U_G
## Messwerte für R = 274kOhm
R_G_2 = unc.ufloat(274, 274 * 0.05) * const.kilo
U_G_2 = 0.3
U_A_2 = unp.uarray([2.06, 2.12, 2.12, 2.14, 2.04, 1.82, 1.5, 1.2, 1, 0.8, 0.6],
                   np.array([0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1, 0.1, 0.1])) / U_G_2
## Messwerte für R = 48.7kOhm
R_G_3 = unc.ufloat(48.7, 48.7 * 0.05) * const.kilo
U_G_3 = 1
U_A_3 = unp.uarray([1.24, 1.24, 1.24, 1.24, 1.25, 1.25, 1.22, 1.09, 0.84, 0.62, 0.24], 0.1) / U_G_3
## Messwerte für R = 48.7kOhm und C=560pF
R_G_4 = R_G_3
U_G_4 = U_G_3
U_A_4 = unp.uarray([1.24, 1.24, 1.23, 1.23, 1.15, 0.94, 0.7, 0.28, 0.15, 0.094, 0.032], 0.1) / U_G_4
## Messwerte für R = 48.7kOhm und C=47nF
R_G_5 = R_G_3
U_G_5 = U_G_3
f_2 = unp.uarray([0.3, 0.6, 1, 3, 6, 10, 20], np.array([0.3, 0.6, 1, 3, 6, 10, 20]) * 0.01) * const.kilo
U_A_5 = unp.uarray([0.41, 0.68, 0.92, 1.21, 1.25, 1.23, 1.19], 0.05) / U_G_5
plot_data(5)