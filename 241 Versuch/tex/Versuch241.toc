\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{2}{}%
\contentsline {section}{\numberline {4}Auswertung}{4}{}%
\contentsline {subsection}{\numberline {4.1}Bestimmung der Zeitkonstante $\tau $ eines RC-Gliedes}{4}{}%
\contentsline {subsection}{\numberline {4.2}RC-Glied als Integrator und Differentiator}{5}{}%
\contentsline {subsection}{\numberline {4.3}Frequenz- und Phasengang eines RC-Gliedes}{5}{}%
\contentsline {subsection}{\numberline {4.4}Frequenzgang eines Serienschwingkreises}{6}{}%
\contentsline {subsection}{\numberline {4.5}Bestimmung der Dämpfungskonstante}{7}{}%
\contentsline {subsection}{\numberline {4.6}Resonanzüberhöhung}{8}{}%
\contentsline {subsection}{\numberline {4.7}Parallelschwingkreis- Bandsperre}{9}{}%
\contentsline {subsection}{\numberline {4.8}Signalformung}{9}{}%
\contentsline {subsection}{\numberline {4.9}Einfacher AM-Empfänger}{10}{}%
\contentsline {section}{\numberline {5}Diskussion}{10}{}%
\contentsline {section}{\numberline {6}Anhang}{12}{}%
\contentsline {subsection}{\numberline {6.1}Messprotokoll}{12}{}%
\contentsline {subsection}{\numberline {6.2}Plots}{23}{}%
