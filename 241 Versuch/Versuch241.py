import pandas as pd
import numpy as np
import uncertainties as unc
from uncertainties import unumpy as unp
from uncertainties.umath import *
import matplotlib.pyplot as plt
import scipy as sp
from scipy import signal
from scipy.optimize import curve_fit
from scipy.constants import constants as const
from scipy.signal import argrelextrema
import scipy.interpolate as ip

# Einstellungen
plt.rcParams['font.family'] = 'serif'
plt.rcParams['errorbar.capsize'] = 2
plt.style.use('classic')


# Generelle Funktionen
def exponential_fit(x, a, b):
    return a * np.power(x, b)


def linear_fit(x, a, b):
    return a * x + b


def find_nearest(uarray, ufloat, index):
    array1, array2 = uarray[0:index], uarray[index:uarray.size]
    array1 = np.asarray(array1)
    array2 = np.asarray(array2)
    indx1 = (np.abs(array1 - ufloat)).argmin()
    indx2 = index + (np.abs(array2 - ufloat)).argmin()
    return indx1, indx2


def phasengang_fit(frequ, phi):
    return np.arctan(phi / frequ) * 360 / (2 * const.pi)


def calc_sigma(mess1, mess2):
    return np.abs(unp.nominal_values(mess1) - unp.nominal_values(mess2)) / np.sqrt(
        unp.std_devs(mess1) ** 2 + unp.std_devs(mess2) ** 2)


def plot_data(arg):
    plt.clf()
    plt.grid(ls='dotted')
    if arg == 1:
        plt.plot(rc_data['N'], rc_data['CH1'], marker='.', ls='--', color='darkred', linewidth=0.8,
                 label='Eingangsspannung ')
        plt.plot(rc_data['N'], rc_data['CH2'], marker='.', ls='--', color='darkblue', linewidth=0.8,
                 label='Ausgansspannung ')
        plt.title('Spannung eines Kondensators im RC-Glied')
        plt.xlabel('t [s]')
        plt.ylabel('Spannung [V]')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', loc=2)
        plt.savefig('results/V241Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.plot(tiefpassfilter_data['f'], tiefpassfilter_data['V'], marker=None, ls='--', linewidth=0.8,
                 label='Messwerte')
        plt.yscale('log')
        plt.xscale('log')
        x = np.logspace(2, 5, base=10)
        plt.plot(x, exponential_fit(x, *popt), marker=None, ls='--', linewidth=0.8, label='linearer Fit')
        x = np.logspace(3, 5, base=10)
        plt.plot(x, exponential_fit(x, *popt2), marker=None, ls='--', linewidth=0.8, label='linearer Fit')
        plt.plot(unp.nominal_values(intersection2), exponential_fit(unp.nominal_values(intersection2), *popt),
                 marker='*', markersize=10, ls=None, label='Schnittpunkt')
        plt.title('Messwerte am Tiefpassfilter' + r' (C=47nF, R=1k$\Omega$)')
        plt.xlabel('Frequenz [Hz]')
        plt.ylabel('Spannung [Vrms]')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', loc=2)
        plt.savefig('results/V241Diagramm3.pdf', format='PDF')
    if arg == 3:
        plt.plot(hochpassfilter_data['f'], hochpassfilter_data['V'], marker=None, ls='--', linewidth=0.8,
                 color='darkred', label='Messwerte')
        x = np.logspace(2, 4, base=10)
        plt.plot(x, exponential_fit(x, *popt), marker=None, ls='--', linewidth=0.8, label='linearer Fit')
        x = np.logspace(3, 5, base=10)
        plt.plot(x, exponential_fit(x, *popt2), marker=None, ls='solid', linewidth=0.8, label='linearer Fit')
        plt.plot(unp.nominal_values(intersection), exponential_fit(unp.nominal_values(intersection), *popt), marker='*',
                 markersize=10, ls=None,
                 label='Schnittpunkt')
        plt.title('Messwerte am Hochpassfilter' + r' (C=47nF, R=1k$\Omega$)')
        plt.xlabel('Frequenz [Hz]')
        plt.ylabel('Spannung [Vrms]')
        plt.tight_layout()
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim(10 ** 2, 10 ** 5)
        plt.legend(frameon=True, fontsize='small', loc=2)
        plt.savefig('results/V241Diagramm2.pdf', format='PDF')
    if arg == 4:
        plt.plot(resonanz_data_1['f'], resonanz_data_1['V'], marker=None, ls='--', linewidth=0.8, color='darkred',
                 label='Messwerte $U_c$')
        plt.plot(resonanz_data_2['f'], resonanz_data_2['V'], marker=None, ls='--', linewidth=0.8, color='darkblue',
                 label='Messwerte $U_l$')
        plt.plot(resonanz_data_3['f'], resonanz_data_3['V'], marker=None, ls='--', linewidth=0.8, color='black',
                 label='Messwerte $U_r$')
        plt.plot(np.array(resonanz_data_1['f'])[max_resonanz_1], np.array(resonanz_data_1['V'])[max_resonanz_1],
                 marker='*', ls=None, linewidth=1, color='red', label='Maximum der Messwerte $U_c$')
        plt.plot(np.array(resonanz_data_2['f'])[max_resonanz_2], np.array(resonanz_data_2['V'])[max_resonanz_2],
                 marker='*', ls=None, linewidth=1, color='red', label='Maximum der Messwerte $U_l$')
        plt.plot(np.array(resonanz_data_3['f'])[max_resonanz_3], np.array(resonanz_data_3['V'])[max_resonanz_3],
                 marker='*', ls=None, linewidth=1, color='red', label='Maximum der Messwerte $U_d$')

        plt.title('Resonanzkurve eines Serienschwingkreises')
        plt.xlabel('Frequenz [Hz]')
        plt.ylabel('Spannung [Vrms]')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', loc=1)
        plt.savefig('results/V241Diagramm7.pdf', format='PDF')
    if arg == 5:
        plt.plot(daempfung_data['N'], daempfung_data['CH1'], marker=None, ls='--', linewidth=0.8, color='darkred',
                 label='Eingangsspannung')
        plt.plot(daempfung_data['N'], daempfung_data['CH2'], marker=None, ls='--', linewidth=1, color='darkblue',
                 label='Ausgangsspannung')
        plt.title('Messwerte am gedämpften Schwingkreis' + r' (C=47nF, R=47$\Omega$)')
        plt.xlabel('t [us]')
        plt.ylabel('Spannung [Vrms]')
        plt.xlim(1500, 3000)
        plt.tight_layout()
        plt.legend(frameon=True, loc=1)
        plt.savefig('results/V241Diagramm6.pdf', format='PDF')
    if arg == 6:
        plt.plot(rlc_data1['f'], rlc_data1['V'], marker=None, ls='--', linewidth=0.8, color='darkred',
                 label='Messwerte mit R=1k$\Omega$')
        plt.plot(rlc_data2['f'], rlc_data2['V'], marker=None, ls='--', linewidth=0.8, color='darkblue',
                 label='Messwerte mit R=220$\Omega$')
        plt.plot(rlc_data3['f'], rlc_data3['V'], marker=None, ls='--', linewidth=0.8, color='black',
                 label='Messwerte mit R=47$\Omega$')
        plt.plot(rlc_data1['f'][max_resonanz_1], rlc_data1['V'][max_resonanz_1], color='darkblue', marker='*', ls='--',
                 linewidth=1, label='U_max1')
        plt.plot(rlc_data2['f'][max_resonanz_2], rlc_data2['V'][max_resonanz_2], color='darkblue', marker='*', ls='--',
                 linewidth=1, label='U_max2')
        plt.plot(rlc_data3['f'][max_resonanz_3], rlc_data3['V'][max_resonanz_3], color='darkblue', marker='*', ls='--',
                 linewidth=1, label='U_max3')
        plt.vlines(rlc_data1['f'][ind11], 0, rlc_data1['V'][ind11], color='orange', ls='solid', linewidth=1,
                   label='U_eff')
        plt.vlines(rlc_data1['f'][ind12], 0, rlc_data1['V'][ind12], color='orange', ls='solid', linewidth=1,
                   label='U_eff')
        plt.title('Frequenzgang eines Serieschwingkreises')
        plt.xlabel('Frequenz [Hz]')
        plt.ylabel('Spannung [Vrms]')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize=10, loc=1)
        plt.savefig('results/V241Diagramm5.pdf', format='PDF')
    if arg == 7:
        plt.errorbar(unp.nominal_values(frequ), unp.nominal_values(phase), xerr=unp.std_devs(frequ),
                     yerr=unp.std_devs(phase), color='darkred', marker='.', ls='', linewidth=0.8,
                     label='Messwerte der Phase')
        plt.xscale('log')
        plt.plot(unp.nominal_values(frequ), phasengang_fit(unp.nominal_values(frequ), *popt1), color='darkblue',
                 linewidth=0.5, linestyle='--', label='Fit-Funktion für $\phi$')
        plt.xlim(0.5, 11)
        plt.ylim(0, 100)
        plt.title('Phasenverlauf eines Serieschwingkreises')
        plt.xlabel('Frequenz [kHz]')
        plt.ylabel('Phase [deg]')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize=15, loc=3)
        plt.savefig('results/V241Diagramm4.pdf', format='PDF')
    plt.show()


# Daten aus den Aufgaben
rc_data = pd.read_csv('data/RC-Messung1.txt', skiprows=10, delimiter='\s+')
tiefpassfilter_data = pd.read_csv('data/Tiefpassfilter.txt', skiprows=1, delimiter='\s+')
hochpassfilter_data = pd.read_csv('data/Hochpassfilter.txt', skiprows=1, delimiter='\s+')
resonanz_data_1 = pd.read_csv('data/Serienschwingkreis.txt', skiprows=1, delimiter='\s+', nrows=90)
resonanz_data_2 = pd.read_csv('data/Serienschwingkreis.txt', skiprows=93, delimiter='\s+', nrows=90)
resonanz_data_3 = pd.read_csv('data/Serienschwingkreis.txt', skiprows=185, delimiter='\s+', nrows=90)
rlc_data1 = pd.read_csv('data/RLC.txt', skiprows=1, delimiter='\s+', nrows=90)
rlc_data2 = pd.read_csv('data/RLC.txt', skiprows=93, delimiter='\s+', nrows=90)
rlc_data3 = pd.read_csv('data/RLC.txt', skiprows=185, delimiter='\s+', nrows=90)
daempfung_data = pd.read_csv('data/Dämpfung.txt', skiprows=10, delimiter='\s+')

# Sortieren
rc_data.columns = ['N', 'CH1', 'CH2']
tiefpassfilter_data.columns = ['f', 'V', 'dBV']
hochpassfilter_data.columns = ['f', 'V', 'dBV']
resonanz_data_1.columns = ['f', 'V', 'dBV']
resonanz_data_2.columns = ['f', 'V', 'dBV']
resonanz_data_3.columns = ['f', 'V', 'dBV']
rlc_data1.columns = ['f', 'V', 'dBV']
rlc_data2.columns = ['f', 'V', 'dBV']
rlc_data3.columns = ['f', 'V', 'dBV']
daempfung_data.columns = ['N', 'CH1', 'CH2']

# Max Values
max_resonanz_1 = resonanz_data_1['V'].idxmax()
max_resonanz_2 = resonanz_data_2['V'].idxmax()
max_resonanz_3 = resonanz_data_3['V'].idxmax()

# 1
## Messwerte
C_1 = unp.uarray([470, 4.7, 47], np.array([470, 4.7, 47]) * 0.1) * const.nano
R_1 = unp.uarray([1, 10, 1], np.array([1, 10, 1]) * 0.05) * const.kilo
T_1 = unp.uarray([300, 32.7, 32.6], [10, 0.4, 0.3]) * const.micro

## Rechnung
### Experiment
tau_exp = T_1 / np.log(2)
tau_theo = C_1 * R_1
sigma = np.abs(unp.nominal_values(tau_exp) - unp.nominal_values(tau_theo)) / np.sqrt(
    unp.std_devs(tau_exp) ** 2 + unp.std_devs(tau_theo) ** 2)
## Feedback
print('\n 1. Bestimmung der Zeitkonstante eines RC-Gliedes')
print('Die Zeitkonstante beträgt den Messungen zufolge: ' + str(tau_exp))
print('Die Zeitkonstante beträgt der Theorie zufolge: ' + str(tau_theo))
print('Die Abweichung beträgt ' + str(sigma) + ' sigma')
# 2
## Keine Rechnung notwendig
# 3
print('\n 3. Frequenz und Phasengang eines RC-Gliedes')

C_2 = unc.ufloat(47, 47 * 0.1) * const.nano
R_2 = unc.ufloat(1, 0.05) * const.kilo
## Aufgenommen
frequ_tiefpass = unc.ufloat(3.03, 0.05) * const.kilo
frequ_hochpass = unc.ufloat(3.3, 0.04) * const.kilo
##Curve_fit
## Hochpassfilter
popt, pcov = curve_fit(exponential_fit, hochpassfilter_data['f'][:10:], hochpassfilter_data['V'][:10:])
popt2, pcov2 = curve_fit(exponential_fit, hochpassfilter_data['f'][130::], hochpassfilter_data['V'][130::])
intersection = pow(unc.ufloat(popt[0], pcov[0][0]) / unc.ufloat(popt2[0], pcov2[0][0]),
                   1 / (unc.ufloat(popt2[1], pcov2[1][1]) - unc.ufloat(popt[1], pcov[1][1])))

## Tiefpassfilter
popt, pcov = curve_fit(exponential_fit, tiefpassfilter_data['f'][0:20:], tiefpassfilter_data['V'][0:20:])
popt2, pcov2 = curve_fit(exponential_fit, tiefpassfilter_data['f'][420::], tiefpassfilter_data['V'][420::])
intersection2 = pow(popt[0] / popt2[0], 1 / (popt2[1] - popt[1]))
# Fehlerrechnunng funktioniert hier nicht
# d = popt2[1]-popt[1]
# d2 = 1/d-1
# delta = np.sqrt((np.power(pcov[0, 0]/(popt2[0]*d), d2))**2+(np.power(pcov2[0, 0]*popt[0]/(d*popt2[0]), d2))**2+(pcov[1, 1]*np.log(popt[0]/popt2[0])/(d**2)*np.power(popt[0]/popt2[0], d2+1))**2+(pcov2[1][1]*np.log(popt[0]/popt2[0])/(d**2)*np.power(popt[0]/popt2[0], d2+1))**2)


## Messwerte der Phase
frequ = unp.uarray([1, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10], 0)
phase = unp.uarray([79.2, 70.2, 56.88, 48.6, 41.76, 32.4, 28.512, 26.208, 22.464, 22.032, 20.16],
                   [0.4, 0.5, 1.44, 2.16, 2.88, 1.8, 1.7, 1.5, 1.4, 1.6, 1.8])
popt1, pcov1 = curve_fit(phasengang_fit, unp.nominal_values(frequ[2::]), unp.nominal_values(phase[2::]),
                         sigma=unp.std_devs(phase[2::]))
grenz_frequ = unc.ufloat(popt1[0], pcov1[0, 0] ** 0.5) * const.kilo
## Theoretischer Wert
frequ_theo = 1 / (2 * const.pi * R_2 * C_2)
# freq_theo_err=unp.nominal_values(frequ_theo)*((unp.std_devs(R_2)/unp.nominal_values(R_2))**2+(unp.std_devs(C_2)/unp.nominal_values(C_2))**2)**0.5 # Beweis, dass die Fehlerrechnung funktioniert

## Fehler
sigma1 = calc_sigma(grenz_frequ, frequ_theo)
sigma2 = calc_sigma(frequ_hochpass, frequ_theo)
sigma3 = calc_sigma(intersection, frequ_theo)
sigma4 = calc_sigma(frequ_tiefpass, frequ_theo)
sigma5 = calc_sigma(intersection2, frequ_theo)
## Feedback
print('\n Hochpassfilter:')
print('Die theoretische Grenzfrequenz des Hochpassfilters beträgt ' + str(frequ_theo) + ' Hz')
print('Die Ausgleichsfunktion der Phasenverschiebung liefert eine Grenzfrequenz von ' + str(grenz_frequ) + ' Hz')
print('Der Fehler dieser zum theoretischen Wert beträgt ' + str(sigma1) + ' Sigma')
print('Der gemessene Wert beträgt ' + str(frequ_hochpass) + ' Hz')
print('Der Fehler beträgt zum theoretischen Wert ist ' + str(sigma2) + ' Sigma')
print('Aus der Ausgleichsgerade ergibt sich eine Grenzfrequenz von ' + str(intersection) + ' Hz')
print('Dies entspricht einem Fehler von ' + str(sigma3) + ' Sigma zum theoretischen Wert')

print('\n Tiefpassfilter')
print('Die theoretische Grenzfrequenz des Tiefpassfilters beträgt ' + str(frequ_theo) + ' Hz')
print('Der gemessene Wert beträgt ' + str(frequ_tiefpass) + ' Hz')
print('Dies entspricht einem Fehler von ' + str(sigma4) + ' Sigma')
print('Der Wert anhand der Ausgleichsgerade beträgt ' + str(intersection2) + ' Hz')
print('Der Fehler beträgt ' + str(sigma5) + ' Sigma')

print('\nTiefpassfilter')

# 4
print('\n4. Frequenzgang eines Serienschwingkreises')
C = unc.ufloat(47, 47 * 0.1) * const.nano
R = unp.uarray([1000, 220, 47], np.array([1000, 220, 47]) * 0.05)
f_r = 2 * const.pi * unp.uarray([3.94, 3.72, 3.72], [0.03, 0.02, 0.03]) * const.kilo
df_r = 2 * const.pi * unp.uarray([5.37, 1.36, 0.63], [0.03, 0.03, 0.03]) * const.kilo
U_max4 = unp.uarray([0.658, 0.516, 0.233], [0.007, 0.014, 0.007])

max_resonanz_1 = rlc_data1['V'].idxmax()
max_resonanz_2 = rlc_data2['V'].idxmax()
max_resonanz_3 = rlc_data3['V'].idxmax()
U_max = unc.ufloat(rlc_data1['V'][max_resonanz_1], rlc_data1['V'][max_resonanz_1] * 0.015)
U_max2 = unc.ufloat(rlc_data2['V'][max_resonanz_2], rlc_data2['V'][max_resonanz_2] * 0.015)
U_max3 = unc.ufloat(rlc_data3['V'][max_resonanz_3], rlc_data3['V'][max_resonanz_3] * 0.015)
U_eff1 = U_max / np.sqrt(2)
U_eff2 = U_max2 / np.sqrt(2)
U_eff3 = U_max3 / np.sqrt(2)
ind11, ind12 = find_nearest(rlc_data1['V'], U_eff1, max_resonanz_1)
ind21, ind22 = find_nearest(rlc_data2['V'], U_eff2, max_resonanz_2)
ind31, ind32 = find_nearest(rlc_data3['V'], U_eff3, max_resonanz_3)
f_r2 = 2 * const.pi * unp.uarray(
    [rlc_data1['f'][max_resonanz_1], rlc_data2['f'][max_resonanz_2], rlc_data3['f'][max_resonanz_3]],
    np.array([rlc_data1['f'][max_resonanz_1], rlc_data2['f'][max_resonanz_2], rlc_data3['f'][max_resonanz_3]]) * 0.015)
delta_1 = np.abs(rlc_data1['f'][ind11] - rlc_data1['f'][ind12])
delta_2 = np.abs(rlc_data2['f'][ind21] - rlc_data2['f'][ind22])
delta_3 = np.abs(rlc_data3['f'][ind31] - rlc_data3['f'][ind32])
df_r2 = 2 * const.pi * unp.uarray([delta_1, delta_2, delta_3], np.array([delta_1, delta_2, delta_3]) * 0.015)
f_mean1 = sum(f_r) / len(f_r)
f_mean2 = sum(f_r2) / len(f_r2)

L1 = 1 / (f_mean1 ** 2 * C)
L2 = 1 / (f_mean2 ** 2 * C)

R_v = df_r * L1 - R
R_v2 = df_r * L2 - R
R_ges = R + R_v
R_ges2 = R + R_v2

U_E = unp.uarray(np.ones(3), 0.1 * np.ones(3))
U_A = U_max4
U_A2 = unp.uarray([unp.nominal_values(U_max), unp.nominal_values(U_max2), unp.nominal_values(U_max3)],
                  [unp.std_devs(U_max), unp.std_devs(U_max2), unp.std_devs(U_max3)]) / np.sqrt(2)
R_v3 = R * (U_E / U_A - 1)
R_v4 = R * (U_E / U_A2 - 1)

## Feedback
print('Die gemessenen Grenzfrequenzen betragen\n ' + str(f_r * const.kilo) + ' 1/s')
print('Die berechneten Grenzfrequenzen betragen\n ' + str(f_r2) + ' 1/s')
print('Das ergibt einen gemessenen Mittelwert von ' + str(f_mean1) + ' 1/s')
print('Das ergibt einen berechneten Mittelwert von ' + str(f_mean2) + ' 1/s')
print('Die Induktivität beträgt gemessen ' + str(L1) + ' H')
print('Die Induktivität beträgt berechnet ' + str(L2) + ' H')
print('Die gemessene Breite beträgt\n' + str(df_r) + ' 1/s')
print('Die berechnete Breite beträgt\n ' + str(df_r2) + ' 1/s')
print('\nDer gemessene Verlustwiderstand beträgt\n ' + str(R_v) + ' Ohm')
print('\nDer berechnete Verlustwiderstand beträgt\n ' + str(R_v2) + ' Ohm')
print('\nDas bedeutet ein gemessener Gesamtwiderstand von\n ' + str(R_ges) + ' Ohm')
print('\nDas bedeutet ein berechneter Gesamtwiderstand von\n ' + str(R_ges2) + ' Ohm')
print('Aus der Spannungsmessung folgt ein Verlustwiderstand von\n ' + str(R + R_v3) + ' Ohm')
print('Aus der Spannungsrechnung folgt ein Verlustwiderstand von\n ' + str(R + R_v4) + ' Ohm')

# 5
print('\n5. Dämpfungskonstante')
C = unc.ufloat(47, 47 * 0.1) * const.nano
R = unc.ufloat(47, 47 * 0.05)
L1_2 = 1 / (f_r[2] ** 2 * C)
L1_3 = 1 / (f_r2[2] ** 2 * C)

A = unp.uarray([0.92, 0.57, 0.36, 0.22, 0.14], 0.01)
T = unp.uarray([0.26, 0.25, 0.26, 0.25, 0.25], 0.01) * const.milli

Lambda = []
Lambda_err = []
for i in range(4):
    Lambda.append(np.log(unp.nominal_values(A[i]) / unp.nominal_values(A[i + 1])))
    Lambda_err.append(np.sqrt((unp.std_devs(A[0]) / unp.nominal_values(A[i])) ** 2 + (
            unp.std_devs(A[0]) / unp.nominal_values(A[i + 1])) ** 2))
Lambda_err = np.array(Lambda_err)
Lambda_mean = np.mean(Lambda)
Lambda_mean_err = 1 / 3 * np.sum(Lambda_err ** 2) ** 0.5
Lambda_mean_std = np.std(Lambda) / np.sqrt(len(Lambda))
Lambda = unc.ufloat(Lambda_mean, Lambda_mean_std + Lambda_mean_std)

T = sum(T) / len(T)
R_ges3 = 2 * L1_2 * Lambda / T
diff1 = calc_sigma(R_ges3, R_ges[2])
diff2 = calc_sigma(R_ges3, R + R_v3[2])
## Feedback
print('Die gemessene Induktivität der Spule beträgt ' + str(L1_2) + ' H')
print('Das logarithmische Dekrement beträgt ' + str(Lambda))
print('Der Gesamtwiderstand beträgt ' + str(R_ges3) + ' Ohm')
print('Die Abweichung zu A4 ist ' + str(diff1) + ' Sigma')
print('Die Abweichung zu A4 ist ' + str(diff2) + ' Sigma')

# 6
print('\n 6. Resonanzüberhöhung')
## Max Values
max_resonanz_1 = resonanz_data_1['V'].idxmax()
max_resonanz_2 = resonanz_data_2['V'].idxmax()
max_resonanz_3 = resonanz_data_3['V'].idxmax()
## Spannung
U_max_1 = unc.ufloat(resonanz_data_1['V'][max_resonanz_1], resonanz_data_1['V'][max_resonanz_1] * 0.015)
U_max_2 = unc.ufloat(resonanz_data_2['V'][max_resonanz_2], resonanz_data_2['V'][max_resonanz_2] * 0.015)
U_max_3 = unc.ufloat(resonanz_data_3['V'][max_resonanz_3], resonanz_data_3['V'][max_resonanz_3] * 0.015)
## Frequenz
omega_c = 2 * const.pi * unc.ufloat(resonanz_data_1['f'][max_resonanz_1], resonanz_data_1['f'][max_resonanz_1] * 0.015)
omega_l = 2 * const.pi * unc.ufloat(resonanz_data_2['f'][max_resonanz_2], resonanz_data_2['f'][max_resonanz_2] * 0.015)
omega_r = 2 * const.pi * unc.ufloat(resonanz_data_3['f'][max_resonanz_3], resonanz_data_3['f'][max_resonanz_3] * 0.015)

## Gemessen
omega_c2 = 2 * const.pi * unc.ufloat(3.75, 0.02) * const.kilo
omega_l2 = 2 * const.pi * unc.ufloat(4, 0.04) * const.kilo
omega_r2 = 2 * const.pi * unc.ufloat(3.87, 0.05) * const.kilo

## Theorie
C = unc.ufloat(47, 47 * 0.1) * const.nano
R = unc.ufloat(220, 220 * 0.05)
L = L1_3
omega_r_theo = 1 / sqrt(L * C)
omega_c_theo = sqrt(omega_r_theo ** 2 - (R / L) ** 2)
omega_l_theo = sqrt(omega_r_theo ** 2 + (R / L) ** 2)
## Fehler
sigma_r = calc_sigma(omega_r_theo, omega_r2)
sigma_c = calc_sigma(omega_c_theo, omega_c2)
sigma_l = calc_sigma(omega_l_theo, omega_l2)
## Feedback
print('\n 6. Resonanzüberhöhung')
print('Die berechnete Resonanzfrequenz für den Condensator beträgt: ' + str(omega_c) + ' [Hz]')
print('Die berechnete Resonanzfrequenz für die Spule beträgt: ' + str(omega_l) + ' [Hz]')
print('Die berechnete Resonanzfrequenz für den Widerstand beträgt: ' + str(omega_r) + ' [Hz]')
print('Die gemessene Resonanzfrequenz für den Condensator beträgt: ' + str(omega_c2) + ' [Hz]')
print('Die gemessene Resonanzfrequenz für die Spule beträgt: ' + str(omega_l2) + ' [Hz]')
print('Die gemessene Resonanzfrequenz für den Widerstand beträgt: ' + str(omega_r2) + ' [Hz]')
print('Die theoretische Resonanzfrequenz für den Condensator beträgt: ' + str(omega_c_theo) + ' [Hz]')
print('Die theoretische Resonanzfrequenz für die Spule beträgt: ' + str(omega_l_theo) + ' [Hz]')
print('Die theoretische Resonanzfrequenz für den Widerstand beträgt: ' + str(omega_r_theo) + ' [Hz]')
print('\n Der Fehler zum Messwert des Condensators beträgt: ' + str(sigma_c) + ' sigma')
print('Der Fehler zum Messwert des Spule beträgt: ' + str(sigma_l) + ' sigma')
print('Der Fehler zum Messwert des Widerstands beträgt: ' + str(sigma_r) + ' sigma')

# 7
print('\n 7. Bandsperre')
f_r = 2 * const.pi * unc.ufloat(3.68, 0.02) * const.kilo
f_theo = f_mean2
print('Die theoretische Resonanzfrequenz beträgt: ' + str(f_theo) + ' Hz')
print('Die gemessene Resonanzfrequenz beträgt: ' + str(f_r) + ' Hz')
print('Die Abweichung beträgt: ' + str(calc_sigma(f_theo, f_r)) + ' Sigma')

# 8
print('\n 8.Signalformung')
signal_rein = 10 ** (-1 / 20 * np.array([8.38, 7.75, 2.44]))
signal_hochpass = 10 ** (-1 / 20 * np.array([9, 10.56, 31.19]))
signal_tiefpass = 10 ** (-1 / 20 * np.array([16.81, 11.81, 2.44]))
signal_LC = 10 ** (-1 / 20 * np.array([25.25, 6, 8.69]))
signal_bandpassfilter_1 = 10 ** (-1 / 20 * np.array([20.88, 9.63, 2.13]))
signal_bandpassfilter_47 = 10 ** (-1 / 20 * np.array([21.99, 6, 3.06]))

v1 = signal_hochpass / signal_rein
v2 = signal_tiefpass / signal_rein
v3 = signal_LC / signal_rein
v4 = signal_bandpassfilter_1 / signal_rein
v5 = signal_bandpassfilter_47 / signal_rein

print('Die Verhältnisse betragen:')
print(v1)
print(v2)
print(v3)
print(v4)
print(v5)

v1 = v1 / v1[1]
v2 = v2 / v2[1]
v3 = v3 / v3[1]
v4 = v4 / v4[1]
v5 = v5 / v5[1]

print('Die Verhältnisse genormt betragen:')
print(v1)
print(v2)
print(v3)
print(v4)
print(v5)

print('Vergleich zu A3')
v = 10 ** (-1 / 20 * unc.ufloat(31.19, 0.2)) / signal_rein[2]
a_3 = 10 ** (-1 / 20 * unc.ufloat(33.19, 0.2)) / hochpassfilter_data['V'][hochpassfilter_data['V'].size-1]
print('Die Dämpfung beträgt ' + str(100*v) + ' %')
print('Aus 3 ergibt sich eine Dämpfung von ' + str(100 * a_3) + ' %')
print('Das enspricht einer Abweichung von: '+str(calc_sigma(a_3, v))+' Sigma')
