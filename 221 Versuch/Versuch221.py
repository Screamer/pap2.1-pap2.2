import uncertainties as unc
import uncertainties.unumpy as unp
import numpy as np
import scipy.constants as const


def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))


def calc_kappa(m, V, r, T, p0):
    T = T / 50
    p = p0 + m * const.g/ const.pi / r ** 2
    return 4 * m * V / (r ** 4 * T ** 2 * p)


kappa_02_lit = unc.ufloat(1.398, 0.001)
kappa_Ar_lit = unc.ufloat(1.648, 0.001)

# 1. Clemement und Desormes
print('\n 1. Clement und Desormes')
h1 = unp.uarray([21.5, 18.4, 13.6, 14.4, 13.8], 0.2)
h3 = unp.uarray([5.9, 4.8, 2.8, 3.8, 3.6], 0.2)
kappa = h1 / (h1 - h3)
kappa_mean = sum(kappa) / len(kappa)

print('Der Adiabatenkoeffizient von Sauerstoff beträgt nach Clement ' + str(kappa_mean))
print('Der Fehler zum Literaturwert beträgt ' + str(calc_sigma(kappa_mean, kappa_02_lit)))
print('Nach den Einzelmessungen zufolge beträgt der Koeffizient '+str(kappa))
print('Nach den Einzelmessungen zufolge beträgt die Abweichung '+str(calc_sigma(kappa, kappa_02_lit)))

# 2. Rüchhardt
print('\n 2. Rüchardt')
## Sauerstoff
print('Sauerstoff:')
kappa_02 = calc_kappa(m = unc.ufloat(26.116, 0.002) * const.gram, V = unc.ufloat(5370, 5) * const.centi**3, r = unc.ufloat(15.95, 0.02) / 2 * const.milli, T = unc.ufloat(50.95, 0.2), p0 = unc.ufloat(987.9, 0.1) * const.milli * 10**5)
print('Argon:')
kappa_Ar = calc_kappa(unc.ufloat(26.006, 0.002) * const.gram, unc.ufloat(5460, 5) * const.centi**3, unc.ufloat(15.97, 0.05) / 2 * const.milli, unc.ufloat(46.93, 0.2), unc.ufloat(987.9, 0.1) * const.milli * 10**5)

## Feedback
print('Der Adiabatenkoeffizient von O2 beträgt ' + str(kappa_02))
print('Der Adiabatenkoeffizient von Ar beträgt ' + str(kappa_Ar))
print('Die Abweichung O2 zum Literaturwert beträgt '+str(calc_sigma(kappa_02, kappa_02_lit)))
print('Die Abweichung Ar zum Literaturwert beträgt '+str(calc_sigma(kappa_Ar, kappa_Ar_lit)))
