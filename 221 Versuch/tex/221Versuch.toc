\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{1}{}%
\contentsline {subsection}{\numberline {3.1}Clèment und Desormes}{1}{}%
\contentsline {subsection}{\numberline {3.2}Rüchardt}{2}{}%
\contentsline {section}{\numberline {4}Durchführung}{3}{}%
\contentsline {subsection}{\numberline {4.1}Methode nach Clèment und Desormes}{3}{}%
\contentsline {subsection}{\numberline {4.2}Methode nach Rüchardt}{3}{}%
\contentsline {section}{\numberline {5}Protokoll}{3}{}%
\contentsline {section}{\numberline {6}Auswertung}{6}{}%
\contentsline {subsection}{\numberline {6.1}Methode nach Clèment und Desormes}{6}{}%
\contentsline {subsection}{\numberline {6.2}Methode nach Rüchardt}{6}{}%
\contentsline {section}{\numberline {7}Diskussion}{7}{}%
\contentsline {section}{\numberline {8}Anhang}{8}{}%
\contentsline {subsection}{\numberline {8.1}Source Code}{8}{}%
