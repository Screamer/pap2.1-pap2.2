import numpy as np
import uncertainties as unc
from uncertainties import unumpy as unp
from scipy.constants import constants as const


def calc_sigma(v1, v2):
    return np.abs(unp.nominal_values(v1) - unp.nominal_values(v2)) / np.sqrt(
        unp.std_devs(v1) ** 2 + unp.std_devs(v2) ** 2)


# Berechnung der Kreisfrequenz
t1 = unp.uarray([2.11, 3.83], 0.1)
t2 = unp.uarray([52.76, 38.07], 0.1)
n = unp.uarray([31, 21], 0)
T = (t2 - t1) / n
omega = 2 * const.pi * 1 / T
omega_mean = sum(omega) / len(omega)
print(T)
print('Die Winkelgeschwindigkeit des ersten Pendels beträgt ' + str(omega[0]) + ' Hz')
print('Die Winkelgeschwindigkeit des zweiten Pendels beträgt ' + str(omega[1]) + ' Hz')
print('Das ergibt einen Mittelwert von ' + str(omega_mean) + ' Hz')

# symmetrische und antisymmetrische Eigenschwingung und Schwebung
l1 = unc.ufloat(15.5, 0.1) * const.centi
l2 = unc.ufloat(25.5, 0.1) * const.centi
l3 = unc.ufloat(40.5, 0.1) * const.centi

## gemessene Eigenschwingungen symmterisch der Reihenfolge l1, l2, l3
omega_symm = 2 * const.pi * unp.uarray([0.615, 0.613, 0.612], [0.012, 0.007, 0.012])
omega_sym_mean = sum(omega_symm) / len(omega_symm)
sigma_sym = calc_sigma(omega_sym_mean, omega_mean)
print('\nDie symmetrischen Eigenschwingungen betragen:\n ' + str(omega_symm) + ' Hz')
print('Das ergibt einen Mittelwert von ' + str(omega_sym_mean) + ' Hz')
print('Die Abweichung zum theretischen Wert beträgt ' + str(sigma_sym) + ' Sigma')

## gemesseene Eigenschwingungen antisymmetrisch l1, l2, l3
omega_anti = 2 * const.pi * unp.uarray([0.629, 0.66, 0.735], [0.008, 0.008, 0.012])
omega_anti_mean = sum(omega_anti) / len(omega_anti)
print('\nDie antisymmetrischen Eigenschwingungen betragen:\n ' + str(omega_anti) + ' Hz')
print('Das ergibt einen Mittelwert von ' + str(omega_anti_mean) + ' Hz')

## Schwebung
omega_schw_1 = 2 * const.pi * unp.uarray([0.613, 0.614, 0.612], [0.003, 0.002, 0.012])
omega_schw_2 = 2 * const.pi * unp.uarray([0.631, 0.659, 0.736], [0.007, 0.002, 0.016])
omega_1 = 1 / 2 * (omega_schw_1 + omega_schw_2)
omega_2 = 1 / 2 * (omega_schw_2 - omega_schw_1)
omega_schw_1_mean = sum(omega_schw_1) / len(omega_schw_1)
omega_schw_2_mean = sum(omega_schw_2) / len(omega_schw_2)
omega_1_theo = 1 / 2 * (omega_anti + omega_symm)
omega_2_theo = 1 / 2 * (omega_anti - omega_symm)

print('Omega 1 ist:\n' + str(omega_1))
print('Omega 2 ist:\n' + str(omega_2))
print('Omega theo 1 ist:\n' + str(omega_1_theo))
print('Omega theo 2 ist:\n' + str(omega_2_theo))
print('Die Abweichung 1 beträgt:\n' + str(calc_sigma(omega_1, omega_1_theo)))
print('Die Abweichung 2 beträgt:\n' + str(calc_sigma(omega_2, omega_2_theo)))

## Kopplungsgrade

v_l_12 = l2 ** 2 / l1 ** 2
v_l_23 = l3 ** 2 / l2 ** 2
v_l_13 = l3 ** 2 / l1 ** 2

print('\nDas Kopplungsverhältnis der ersten Position zur zweiten beträgt: ' + str(v_l_12))
print('Das Kopplungsverhältnis der zweiten Position zur dritten beträgt: ' + str(v_l_23))
print('Das Kopplungsverhältnis der ersten Position zur dritten beträgt: ' + str(v_l_13))

## Kopplung aus symmetrsisch und antisymmetrisch ohne Näherung
k_1 = (omega_anti ** 2 - omega_symm ** 2) / (omega_anti ** 2 + omega_symm ** 2)
print('\nAnhand der Messwerte des symmetrischen und antisymmetrischen Vorgangs berechnet sich k zu:\n ' + str(k_1))

v_12 = k_1[1] / k_1[0]
v_23 = k_1[2] / k_1[1]
v_13 = k_1[2] / k_1[0]

print('Das gemessene Kopplungsverhältnis der ersten Position zur zweiten beträgt: ' + str(v_12))
print('Das gemessene Kopplungsverhältnis der zweiten Position zur dritten beträgt: ' + str(v_23))
print('Das gemessene Kopplungsverhältnis der ersten Position zur dritten beträgt: ' + str(v_13))

print('Der Fehler der Kopplungsverhältnis der ersten Position zur zweiten beträgt: ' + str(calc_sigma(v_12, v_l_12)))
print('Der Fehler der Kopplungsverhältnis der zweiten Position zur dritten beträgt: ' + str(calc_sigma(v_23, v_l_23)))
print('Der Fehler der Kopplungsverhältnis der ersten Position zur dritten beträgt: ' + str(calc_sigma(v_13, v_l_13)))

## Kopplung aus symmetrsisch und antisymmetrisch mit Näherung
k_2 = (omega_anti ** 2 - omega_symm ** 2) / (2 * omega_symm ** 2)
dk_2 = np.sqrt(((2*unp.nominal_values(omega_anti)*unp.std_devs(omega_anti))/(2*unp.nominal_values(omega_symm)**2))**2+((4*unp.nominal_values(omega_symm**3)+(unp.nominal_values(omega_anti)**2-unp.nominal_values(omega_symm)**2)*4*unp.nominal_values(omega_symm))/(4*unp.nominal_values(omega_symm)**4)*unp.std_devs(omega_symm))**2)

print('Anhand der Messwerte des symmetrischen und antisymmetrischen Vorgangs berechnet sich k zu:\n ' + str(k_2))
print('\nMit alternativer Fehlerrechnung\n '+str(dk_2))

v2_12 = k_2[1] / k_2[0]
v2_23 = k_2[2] / k_2[1]
v2_13 = k_2[2] / k_2[0]

print('Das gemessene Kopplungsverhältnis mir Näherung der ersten Position zur zweiten beträgt: ' + str(v2_12))
print('Das gemessene Kopplungsverhältnis mir Näherung der zweiten Position zur dritten beträgt: ' + str(v2_23))
print('Das gemessene Kopplungsverhältnis mir Näherung der ersten Position zur dritten beträgt: ' + str(v2_13))

print('Der Fehler der Kopplungsverhältnis mir Näherung der ersten Position zur zweiten beträgt: ' + str(
    calc_sigma(v2_12, v_l_12)))
print('Der Fehler der Kopplungsverhältnis mir Näherung der zweiten Position zur dritten beträgt: ' + str(
    calc_sigma(v2_23, v_l_23)))
print('Der Fehler der Kopplungsverhältnis mir Näherung der ersten Position zur dritten beträgt: ' + str(
    calc_sigma(v2_13, v_l_13)))

## Kopplung aus Schwebung
k_3 = (omega_schw_2 ** 2 - omega_schw_1 ** 2) / (omega_schw_2 ** 2 + omega_schw_1 ** 2)

print('\nAnhand der Messwerte der Schwebung berechnet sich k zu:\n ' + str(k_3))

v3_12 = k_3[1] / k_3[0]
v3_23 = k_3[2] / k_3[1]
v3_13 = k_3[2] / k_3[0]

print('Das gemessene Kopplungsverhältnis mir Näherung der ersten Position zur zweiten beträgt: ' + str(v3_12))
print('Das gemessene Kopplungsverhältnis mir Näherung der zweiten Position zur dritten beträgt: ' + str(v3_23))
print('Das gemessene Kopplungsverhältnis mir Näherung der ersten Position zur dritten beträgt: ' + str(v3_13))
print('Der Fehler beträgt:' + str(calc_sigma(v3_12, v_l_12)))
print('Der Fehler beträgt:' + str(calc_sigma(v3_23, v_l_23)))
print('Der Fehler beträgt:' + str(calc_sigma(v3_13, v_l_13)))

k_4 = (omega_schw_2 ** 2 - omega_schw_1 ** 2) / (2 * omega_schw_1 ** 2)

print('\nAnhand der Messwerte der Schwebung berechnet sich k zu:\n ' + str(k_4))

v4_12 = k_4[1] / k_4[0]
v4_23 = k_4[2] / k_4[1]
v4_13 = k_4[2] / k_4[0]

print('Das gemessene Kopplungsverhältnis mir Näherung der ersten Position zur zweiten beträgt: ' + str(v4_12))
print('Das gemessene Kopplungsverhältnis mir Näherung der zweiten Position zur dritten beträgt: ' + str(v4_23))
print('Das gemessene Kopplungsverhältnis mir Näherung der ersten Position zur dritten beträgt: ' + str(v4_13))
print('Der Fehler beträgt:' + str(calc_sigma(v4_12, v_l_12)))
print('Der Fehler beträgt:' + str(calc_sigma(v4_23, v_l_23)))
print('Der Fehler beträgt:' + str(calc_sigma(v4_13, v_l_13)))
