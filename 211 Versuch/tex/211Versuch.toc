\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {subsection}{\numberline {1.1}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {2}Theorie}{2}{}%
\contentsline {subsection}{\numberline {2.1}Symmetrische Schwingung}{2}{}%
\contentsline {subsection}{\numberline {2.2}Asymmetrische Schwingung}{3}{}%
\contentsline {subsection}{\numberline {2.3}Schwebungsschwingung}{3}{}%
\contentsline {subsection}{\numberline {2.4}Kopplungsgrad}{3}{}%
\contentsline {section}{\numberline {3}Durchführung}{3}{}%
\contentsline {section}{\numberline {4}Auswertung}{4}{}%
\contentsline {subsection}{\numberline {4.1}Ohne Feder}{4}{}%
\contentsline {subsubsection}{\numberline {4.1.1}Source Code}{4}{}%
\contentsline {subsection}{\numberline {4.2}Symmetrische Schwingung}{5}{}%
\contentsline {subsubsection}{\numberline {4.2.1}Source Code}{5}{}%
\contentsline {subsection}{\numberline {4.3}Antsymmetrische Schwingung}{5}{}%
\contentsline {subsubsection}{\numberline {4.3.1}Source Code}{6}{}%
\contentsline {subsection}{\numberline {4.4}Schwebung}{6}{}%
\contentsline {subsubsection}{\numberline {4.4.1}Source Code}{7}{}%
\contentsline {subsection}{\numberline {4.5}Kopplungsgrade}{8}{}%
\contentsline {subsubsection}{\numberline {4.5.1}Source Code}{9}{}%
\contentsline {section}{\numberline {5}Diskussion}{11}{}%
\contentsline {section}{\numberline {6}Anhang}{12}{}%
\contentsline {subsection}{\numberline {6.1}Messprotokoll}{12}{}%
\contentsline {subsection}{\numberline {6.2}Plots}{14}{}%
