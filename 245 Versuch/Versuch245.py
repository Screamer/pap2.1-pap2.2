import uncertainties as unc
import uncertainties.unumpy as unp
from uncertainties.umath import atan, asin, acos
import numpy as np
import scipy.constants as const
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2
plotdata = False


def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def linear_fit(x,a,b):
    return a*x+b

def cos_fit(x, a, b):
    return a*np.cos(x*2*const.pi / 360 )

def plot_data(arg):
    plt.clf()
    plt.grid(ls='dotted')
    if arg == 1:
        plt.errorbar(unp.nominal_values(omega), unp.nominal_values(U_ind), xerr=unp.std_devs(omega), yerr=unp.std_devs(U_ind), fmt='none', marker='.', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(omega), linear_fit(unp.nominal_values(omega), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title(r'$U_{ind}$ in Abhängigkeit von $\omega$', size=20)
        plt.xlabel(r'$\omega$ [Hz]', size=18)
        plt.ylabel('$U_{ind}$ [V]', size=18)
        plt.savefig('results/V245Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.errorbar(unp.nominal_values(I), unp.nominal_values(U_ind), xerr=unp.std_devs(I), yerr=unp.std_devs(U_ind), fmt='none', color='darkred', marker='.', label='Messwerte')
        plt.plot(unp.nominal_values(I), linear_fit(unp.nominal_values(I), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title(r'$U_{ind}$ in Abhängigkeit von $I$', size=20)
        plt.xlabel(r'$I$ [A]', size=18)
        plt.ylabel('$U_{ind}$ [V]', size=18)
        plt.savefig('results/V245Diagramm2.pdf', format='PDF')
    if arg == 3:
        plt.errorbar(unp.nominal_values(alpha), unp.nominal_values(U_ind), xerr=unp.std_devs(alpha), yerr=unp.std_devs(U_ind), fmt='none', marker='.', color='darkred', label='Messwerte')
        #plt.plot(unp.nominal_values(alpha), cos_fit(unp.nominal_values(alpha), *popt), marker=None, ls='--', color='darkred', label='Ausgleichsfunktion')
        plt.title(r'$U_{ind}$ in Abhängigeit des Winkels $\alpha$', size=20)
        plt.xlabel(r'$\alpha$ [°]', size=18)
        plt.ylabel(r'$U_{ind}$ [V]', size=18)
        plt.savefig('results/V245Diagramm3.pdf', format='PDF')
    if arg == 4:
        plt.errorbar(unp.nominal_values(omega), unp.nominal_values(verh), xerr=unp.std_devs(omega), yerr=unp.std_devs(verh), ls='--', marker='.', color='darkred', label='Messwerte')
        plt.title(r'$\frac{U_{ind}}{U_{ein}}$ als Funktion der Kreisfrequenz $\omega$', size=20)
        plt.xlabel(r'$\omega$ [Hz]', size=18)
        plt.ylabel(r'Verhältniss $\frac{U_{ind}}{U_{ein}}$', size=18)
        plt.savefig('results/V245Diagramm4.pdf', format='PDF')
    if arg == 5:
        plt.errorbar(unp.nominal_values(omega), unp.nominal_values(R_s), xerr=unp.std_devs(omega), yerr=unp.std_devs(R_s), fmt='none', marker='.', colot='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(omega), linear_fit(unp.nominal_values(omega), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title(r'Verhältnis $R_S = \frac{U_{ein}}{I_{ein}}$ als Funktion der Kreisfrequenz $\omega$', size=20)
        plt.xlabel(r'$\omega$ [Hz]', size=18)
        plt.ylabel(r'$R_S = \frac{U_{ein}}{I_{ein}}$ [$\frac{V}{A}$]', size=18)
        plt.savefig('results/V245Diagramm5.pdf', format='PDF')
    plt.tight_layout()
    plt.legend(frameon=True, loc='best')
    plt.show()
    return

# Konstanten
N_Helm = 124
N_Feld = 4000
r_helm = unc.ufloat(295, 1) * const.milli / 2
dist = unc.ufloat(147, 1) * const.milli / 2
area = unc.ufloat(41.7, 0.1) * const.centi ** 2

# 1. Induktionsgesetz
print('\n 1. Induktionsgesetz')
## U - f
I = unc.ufloat(4, 0.1)
frequ = unp.uarray([2.67, 5.9, 9, 12.31, 14.84], 0.015)
U_ind = unp.uarray([0.86, 2.05, 3.14, 4.28, 5.15], [0.05, 0.1, 0.08, 0.08, 0.06])
omega = 2 * const.pi * frequ
popt, pcov = curve_fit(linear_fit, unp.nominal_values(omega), unp.nominal_values(U_ind), sigma=unp.std_devs(U_ind))
if plotdata : plot_data(1)
m = unc.ufloat(popt[0], float(pcov[0, 0]**0.5))
B_exp = m / area / N_Feld
B_theo = const.mu_0 * 8 * N_Helm * I / np.sqrt(125) / r_helm
## U - I
I = unp.uarray([0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5], 0.1)
U_ind = unp.uarray([0.6, 1, 1.45, 1.9, 2.44, 2.82, 3.20, 3.75, 4.1, 4.7], [0.05, 0.05, 0.05, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1])
popt, pcov = curve_fit(linear_fit, unp.nominal_values(I), unp.nominal_values(U_ind), sigma=unp.std_devs(U_ind))
if plotdata : plot_data(2)

##Feedback
print('Die theoretische Magnetfeldstärke beträgt '+str(B_theo)+ ' T')
print('Die experimentelle Magnetfeldstärke beträgt '+str(B_exp)+ ' T')
print('Die Abweichung beträgt '+str(calc_sigma(B_theo, B_exp)))

# 2. Lufttransformator
print('\n 2. Lufttransformator')
## U - alpha
alpha = unp.uarray([0, 30, 60, 90, 120, 150, 180], 2)
U_ind = unp.uarray([0.94, 0.8, 0.46, 0.06, 0.54, 0.87, 0.98], 0.01)
frequ = unc.ufloat(118.2, 0.3)
#popt, pcov = curve_fit(cos_fit, unp.nominal_values(alpha), unp.nominal_values(U_ind), sigma=unp.std_devs(U_ind))
if plotdata : plot_data(3)
## U_ind / U_ein - omega
frequ = unp.uarray([20.5, 39.8, 59.7, 79.7, 104.7, 123.1, 143.4, 164.7, 178.8, 202.0, 393, 600, 803.8, 1002, 1175, 1370, 1590, 1812, 1996], 0.1)
U_ind = unp.uarray([0.72, 0.85, 0.9, 0.92, 0.94, 0.94, 0.94, 0.95, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.97, 0.98], 0.01)
I = unp.uarray([269, 175, 125, 96, 74, 63, 62, 47, 44, 38, 19, 12, 8, 7.62, 6.85, 5.82, 5.11, 4.38, 3.84], [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.2, 0.1, 0.1, 0.12, 0.02, 0.02, 0.02, 0.02, 0.02]) * const.milli
U_ein = unp.uarray([1.48, 1.52, 1.46, 1.46, 1.46, 1.46, 1.46, 1.46, 1.46, 1.46, 1.46, 1.44, 1.44, 1.42, 1.42, 1.4, 1.38, 1.36, 1.34], 0.05)
verh = U_ind / U_ein
omega = 2 * const.pi * frequ
R_s = U_ein / I
popt, pcov = curve_fit(linear_fit, unp.nominal_values(omega), unp.nominal_values(R_s), sigma=unp.std_devs(R_s))
L = unc.ufloat(popt[0], pcov[0,0]**0.5)
#Die Länge der Spule ist nicht bekannt
#L_theo = 2 * N_Helm**2 * r_helm * const.mu_0 * (const.pi * r_helm / (0.1 * 2 * r_helm / 2.2) - 4.941 / 4 / const.pi)
if plotdata : plot_data(4)
if plotdata : plot_data(5)

## Feedback
print('Die Induktivität der Spule beträgt '+str(L)+' H')

# 3. Magnetfeld der Erde
print('\n 3. Magnetfeld der Erde')
## Ohne Kompensation
omega = 2 * const.pi * unc.ufloat(15.15, 0.04)
U_ind = unc.ufloat(162, 5) * const.milli / 2
B = U_ind / area / N_Feld / omega
I = unc.ufloat(0.94, 0.01) * const.milli
U_ind_kom = unc.ufloat(60, 2) * const.milli / 2
B_vert = const.mu_0 * 8 * N_Helm * I / np.sqrt(125) / r_helm
B_hor = U_ind_kom / area / N_Feld / omega

alpha = atan(B_vert / B_hor) * 360 / (2 * const.pi)
alpha2 = 360/(2 * const.pi) * asin(B_vert / B)
alpha3 = 260/(2 * const.pi) * acos(B_hor / B)
##Feedback
print('Ohne Kompensation beträgt das Magnetfeld der Erde '+str(B)+ 'T')
print('Die vertikale Komponente des Erdmagnetfeldes beträgt '+str(B_vert)+' T')
print('Die horizontale Komponente des Erdmagnetfeldes beträgt '+str(B_hor)+' T')
print('Der Inklinationswinkel beträgt mit arctan '+str(alpha))
print('Der Inklinationswinkel beträgt mit arcsin '+str(alpha2))
print('Der Inklinationswinkel beträgt mit arccos '+str(alpha3))
print('Mittelwert '+str((alpha+alpha2+alpha3)/3))

## Werte von Dr. Wagner
omega = 2 * const.pi * unc.ufloat(18.39, 0.2)
U_ind = unc.ufloat(184, 2) * const.milli / 2
B = U_ind / area / N_Feld / omega
I = unc.ufloat(64.01, 0.1) * const.milli
U_ind_kom = unc.ufloat(20.6, 0.2) * const.milli / 2
omega = 2 * const.pi * unc.ufloat(18.85, 0.2)
B_vert = const.mu_0 * 8 * N_Helm * I / np.sqrt(125) / r_helm
B_hor = U_ind_kom / area / N_Feld / omega

alpha = atan(B_vert / B_hor) * 360 / (2 * const.pi)
#alpha2 = 360/(2 * const.pi) * asin(B_vert / B)
alpha3 = 260/(2 * const.pi) * acos(B_hor / B)

##Feedback
print('\n Ohne Kompensation beträgt das Magnetfeld der Erde '+str(B)+ 'T')
print('Die vertikale Komponente des Erdmagnetfeldes beträgt '+str(B_vert)+' T')
print('Die horizontale Komponente des Erdmagnetfeldes beträgt '+str(B_hor)+' T')
print('Der Inklinationswinkel beträgt mit arctan '+str(alpha))
#print('Der Inklinationswinkel beträgt mit arcsin '+str(alpha2))
print('Der Inklinationswinkel beträgt mit arccos '+str(alpha3))
print('Mittelwert '+str((alpha+alpha3)/2))