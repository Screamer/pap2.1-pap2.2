\documentclass{article}

\usepackage[ngerman]{babel}
\usepackage{amsmath, amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{wrapfig}
\usepackage{pdfpages}
\usepackage{xparse}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage[shortlabels]{enumitem}

\pagestyle{fancy}
\fancyhf{}
\rhead{mm268}
\lhead{Dezember 2021}
\cfoot{\thepage}

\begin{document}
\begin{titlepage}
    \title{Versuch 245 \\ Induktion}
    \author{Jan Bartels}
    \date{Dezember 2021}
    \maketitle
    \tableofcontents
\end{titlepage}
\section{Einleitung}
In diesem Versuch wird das Induktionsgesetz mithilfe einer Helmholtzspule und einer Induktionsspule überprüft. Weiterhin wird das Erdmagnetfeld gemessen und der Inklinationswinkel bestimmt.
\section{Aufgaben und Aufbau}
Unsere Aufgaben bestanden aus
\begin{enumerate}
    \item das Verhalten einer Spule mit einem Stabmagneten untersuchen
    \item das Induktionsgesetz zu validieren
    \item die Stärke des Erdmagnetfeldes sowie den Inklinationswinkel bestimmen
\end{enumerate}
Zur Verfügung wurde gestellt 
\begin{itemize}
    \item Oszilloskop
    \item Leistungsfunktionsgenerator
    \item Antriebsmotor mit Treibriemen
    \item Netzteile
    \item Multimeter
    \item Kompass
    \item RC-Filter
    \item Helmholtzspule und Induktionsspule
\end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{../data/aufbau.png}
    \caption{Aufbau des Versuches}
\end{figure}
Dieses Bild wurde aus dem von Dr. J. Wagner erstellten Skript zum physikalischen Anfängerpraktikum entnommen.
\subsection{Theorie}
Zwei räumlich voneinander getrennte Spulen gleicher Windungszahl $N$ beschreiben eine Helmholtzspule. Werden beide Spulen mit Radius $R$ und Abstand $R$ von einem Strom $I$ durchflossen, so lässt sich zeigen, dass für das Magnefeld $B$, welches als Superposition der einzelnen Spulen entsteht,
im Mittelpunkt der Anordnung homogen ist und der Betrag des Magnetfeldes 
\begin{align}\label{eq:1}
    B &= \mu_0 \frac{8NI}{\sqrt{125}R}
\end{align}
beträgt. Hierbei bezeichnet $\mu_0$ die magnetische Feldkonstante. Plaziert man eine Feldspule mit Windungszahl $N'$ und Spulenfläche $A$ in das Innere der Anordnung, so kann man davon ausgehen, dass das Magnetfeld konstant ist.
Nach dem Faradayschen Induktionsgesetz wird eine Spannung $U_{ind}$ in dieser induziert, wenn sich das der Spule umgebende Potential $\Phi$ zeitlich verändert
\begin{align}
    \Phi_m &= \int \vec{B}\vec{dA}\\
    U_{ind} &= -\frac{d}{dt}\Phi_m = -\frac{d}{dt}\int \vec{B}\vec{dA}
\end{align}
Wird die Induktionsspule mit einer zeitlich konstanten Drehfrequenz $f$ im Magnetfeld der Spule rotiert, so beträgt die die induzierte Spannung
\begin{align}
    U_{int} &= -\int\vec{B}\frac{d}{dt}\vec{dA} = -BAN\omega\sin{\omega t}\\
    \omega &= 2\pi f
\end{align}
Wird hingegen das Magnefeld zeitlich variiert und die Spule nicht rotiert, so folgt
\begin{align}\label{eq:2}
    U_{ind} &= -\int\frac{d}{dt}\vec{B}\vec{dA} = BAN\Omega\cos\alpha\sin{\Omega t}
\end{align}
$\Omega$ bschreibt die Kreisfrequenz mit der das Magnetfeld periodisch variiert wird.\\
Die Erde verfügt auch über ein Magnetfeld, welches in Deutschland unter einem Inklinationswinkel $\alpha$ von 66 Grad auf die Erdoberfläche trifft. Das bedeutet, dass wenn kein Strom durch die Helmholtzspule fließt dennoch eine Spannung in der sich rotierenden Flachspule induziert wird aufgrund der oben erwähnten Oberflächenänderung.
Das Magnetfeld $\vec{B_E}$ der Erde kann man in eine horizontale $\vec{B_H}$ und vertikale $\vec{B_V}$ Komponente zerlegen, sodass gilt
\begin{align}
    \vec{B_E} &= \vec{B_V} + \vec{B_H}\\
    B_H &= B_E\cos\alpha\\
    B_V &= B_E\sin\alpha
\end{align} 
Kompensiert man das vertiale Magnetfeld der Erde mit der Helmholtzspule, so kann man das Horizontale messen und anhand dessen den Inklinationswinkel bestimmen
\begin{align}
    \alpha &= \arccos{\frac{B_H}{B_E}}
\end{align}
\section{Durchführung}
\subsection{Vorversuch}
Eine Spule wurde mit dem Oszilloskop verbunden. Dann wurde ein Stabmagnet unter verschiedenen Geschwindigkeiten durch die Spule bewegt. Auch wurde der Stabmagnet fixiert und die Spule entlang dessen bewegt.
\subsection{Induktionsgesetz}
Die Einzelspulen wurden überkreutz miteinander verbunden und an ein Netzteil angeschlossen. Durch das Überkreuzen wurde gewährleistet, dass sich die beiden Magnetfelder nicht gegenseitig kompensieren, d.h. der Strom floss in dieselbe Richtung innerhalb beider Spulenpaare.
Das Oszilloskop wurde mit der Flachspule im Zentrum verbunden und der Treibriemen mit um den Drehramen gewunden. Dann wurde zuerst die induzierte Spannung in Abhängigkeit der Drehfrequenz gemessen und danach wurde sie unter Variation des Magnetfeldes bei konstanter Drehfrequenz gemessen.
\subsection{Lufttransformator}
Die Helmholtzspule wurde an den Leistungsfunktionsgenerator angeschlossen und eine Wechselspannung eingeschaltet. Die induzierte Spannung wurde einmal unter Veränderung des Drehwinkels gemessen, einmal als Funktion der Frequenz, wobei gleichzeitig auch der Strom aufgenommen wurde, und zuletzt wurde eine qualitative Beobachtung zur Schwebungsfrequenz unternommen.
\subsection{Bestimmung des Erdmagnetfeldes}
Der Aufbau wurde in Nord-Süd-Richtung ausgerichtet. Die Induktionsspannung der mit 15Hz rotierenden Flachspule wurde aufgenommen. Sodann wurde durch das Anlegen eines Stromes an die Helmholtzspule das vertiale Magnetfeld der Erde so kompensiert, sodass die induzierte Spannung minimal wurde.

\section{Protokoll}
\includepdf[pages=-]{../data/protokoll.pdf}

\section{Auswertung}
\subsection{Induktionsgesetz}
Zuerst wurden alle Konstanten definiert. Damit sind die charakteristischen Werte der Spulen gemeint.
\begin{align*}
    \text{Helmholtzspule}\\
    D &= (295\pm 1)mm \\
    Dist &= (147\pm 1)mm \\
    N &= 124 \\
    \text{Feldspule}\\
    N &= 4000\\
    A &= (41.7\pm 0.1)cm^2
\end{align*}
$Dist$ beschreibt den Abstand der beiden Spulen voneinander. Anhand der Messwerte wurde eine Ausgleichsgerade berechnet. Diese und die Messwerte wurden geplottet und sind in Diagramm \ref{fig:1} erkennbar. Aus der Geradensteigung ließ sich das Magnetfeld $B$ folgendermaßen bestimmen:
\begin{align}\label{eq:4}
    U_{ind} \propto \omega \\
    U_{ind} &= BAN\omega\sin{\omega t}\\
    \overline{U_{ind}} &= BAN\omega \\
    m &= \overline{U_{ind}} / \omega \\
    B &= \frac{m}{AN}\\
    \Delta B &= \sqrt{(\frac{\Delta m}{AN})^2+(\frac{m}{NA^2}\Delta A)^2}
\end{align}
Auch wurde der theoretisch zu erwartende Wert des Magnetfeldes berechnet. Hierfür wurde Formel \ref{eq:1} verwendet:
\begin{align}\label{eq:3}
    B &= \mu_0 \frac{8NI}{\sqrt{125}R}\\
    \Delta B &= B\sqrt{(\frac{\Delta I}{I})^2+(\frac{\Delta R}{R})^2}
\end{align}
Zusammenfassend ergab sich
\begin{align*}
    B_{theo} &= (3.02 \pm 0.08)*10^{-3} T\\
    B_{exp} &=  (3.3650 \pm 0.008)*10^{-3} T\\
    \sigma &= 4.5
\end{align*}
Es ist sofort erkennbar, dass die Werte signifikant voneinander abweichen. Außerdem fällt auch auf, dass die Abweichung des theoretischen Wertes um den Faktor 10 größer ist als die des experimentellen Wertes.
Das heißt, dass die Unsicherheiten in den Messwerten viel zu gering abgeschätz wurden. \\
Auch wurde die gemessene Induktionsspannung als Funktion des Spulenstroms geplottet und im Anhang als Abbildung \ref{fig:2} hinterlegt. Auch hier wurde eine Ausgleichsgerade eingezeichnet.
Auch hier ergibt sich wie zu erwarten war ein linearer Zusammenhang zwischen den beiden Messgrößen. Das lässt sich durch folgende Proportionalitäten leicht beweisen
\begin{align}
    \text{Formel \ref{eq:1}}\\
    B \propto I \\
    U_{ind} \propto B\\
    U_{ind} \propto I
\end{align}
Aus der Steigung der Ausgleichsgerade lässt sich lediglich die Kreisfrequenz $\omega$ bestimmen, welche hingegen völlig irrelevant ist. Daher wird es hierbei belassen.
\subsection{Lufttransformator}
Die gemessene Spannung wurde als Funktion des Winkels in Diagramm \ref{fig:3} dargestellt. Anhand der Gleichung \ref{eq:2} ist ein periodischer Zusammenhang zu erwarten. Diese Periodizität nach einer Rotation um 90 Grad (bis auf Vorzeichen) ist auch dadurch einsichtlich, wenn man sich Gedanken über die Symmetrie des Aufbaus macht.
Leider konnte keine Ausgleichsfunktion geplottet werden aufgrund der Steilheit der Messwerte um das Minimum herum. Hätte man Messwerte über eine vollständige Drehung um die eigene Achse genommen, so wäre dies wahrscheinlich möglich.\\
Weiterhin wurde das Verhältnis von induzierter und angelegter Spannung als Funktion der Frequenz graphisch dargestellt. Das Ergebnis ist in Abbildung \ref{fig:4} zu sehen. Hier ist eindeutig der anfangs stark lineare Teil erkennbar der dadurch resultiert, dass der ohmsche Widerstand der Primärspule die induzierte Spannung verkleinert und auch die Phase verschiebt. Für die Werte mit höheren Kreisfrequenzen ist die Steigung geringer, 
was auf eine Unabhängigkeit von der Sekundärspule zur Primärspule hindeutet.\\
Auch wurde der Widerstand als Funktion der Kreisfrequenz graphisch dargestellt. Hierfür wurde lediglich die Formel des Ohmschen Widerstandes benötigt
\begin{align}
    R &= \frac{U}{I}
\end{align} 
Der Plot ist als Abbildung \ref{fig:5} hinterlegt. Weiterhin gilt für eine Spule der Induktivität $L$
\begin{align}
    U_0 &= \omega L I_0\\
    m &= \frac{U_0}{I_0 \omega}\\
    m &= L\\
    \Delta m &= \Delta L
\end{align}
D.h aus der Steigung der Ausgleichsgeraden ließ sich sofort die Induktivität der Helmholtzspule berechnen.
\begin{align*}
    L &= (28.3\pm 0.6)*10^{-3} H
\end{align*}
Um diesen Wert mit einem Theoretischen vergleichen zu können benötigt man die Länge $l$ der Helmholtzspule. Da diese nicht gegeben war, wird es hierbei belassen.
Die Schwebung wurde auch betrachtet, wobei die Nullstellen dieser mit der der Drehfrequenz übereinstimmen. Damit wurde die Gleichheit dieser sofort einsehbar.
\subsection{Bestimmung des Erdmagnetfeldes}
Zuerst wurde ohne Kompensation die Spule mit einer Frequenz von $f=(15.15\pm 0.04)$ Hz zum rotieren gebracht und die durch das Erdmagnetfeld induzierte Spannung gemessen.
Benutzt man analog zu Formel \ref{eq:4} die Gleichung, so kann man das Erdmagnetfeld bestimmen.
\begin{align}
    B_E &= \frac{U_{ind}}{AN\omega}\\
    \Delta B_E &= B_E \sqrt{(\frac{\Delta U}{U})^2+(\frac{\Delta A}{A})^2+(\frac{\Delta \omega}{\omega})^2}
\end{align}
\begin{align*}
    B_E &= (51.0 \pm 1.6) * 10^{-6} T
\end{align*}
Danach wurden die Werte für die Kompensationsmessung übernommen und das Erdmagnetfeld in den Komponenten berechnet. Zuerst ist zu beachten, dass mit der Kompensationsmessung das vertikale Magnetfeld kompensiert wird. Das bedeutet, dass man aus der eingestellten Stromstärke die Magnetfeldstärke determinieren kann.
Formel \ref{eq:3} angewendet ergibt
\begin{align*}
    I = (0.94 \pm 0.01)*10^{-3} A\\
    B_V = (7.11 \pm 0.08)*10^{-7} T
\end{align*}
und für das horizontale Feld, welches mit der Spannung gemessen wurde ergibt sich
\begin{align*}
    B_H &= (1.89 \pm 0.06)*10^{-5} T
\end{align*} 
Das ergibt jetzt schon keinen Sinn mehr, da das vertikale Magnetfeld größer sein muss als das horizontale, welches hier allerdings nicht stimmt. Sodann wurden die Winkelformeln angewendet um 3 mal den Inklinationswinkel bestimmen zu können. Es folgt
\begin{align}
    \alpha &= \arctan\frac{B_V}{B_H}\\
    \alpha &= \arcsin\frac{B_V}{B_E}\\
    \alpha &= \arccos\frac{B_H}{B_E}
\end{align}
Weil die Fehlerrechnung recht monoton ist, sind hier nur die Ableitungen der Winkelfunktionen gegeben
\begin{align}
    \frac{d}{dx}\arccos x &= \frac{-1}{\sqrt{1-x^2}}\\
    \frac{d}{dx}\arcsin x &= \frac{1}{\sqrt{1-x^2}}\\
    \frac{d}{dx}\arctan x &= \frac{1}{1+x^2}
\end{align}
Weil die Werte schon aus oben erwähnten Gründen merkwürdig erschienen, wurden zum Vergleich die von Dr. J. Wagner veröffentlichten Werte aus dem Video übernommen.
Aus analoger Rechnung ergab sich
\begin{table}[ht]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        $\alpha(B_V / B_H)$ &   $\alpha(B_V / B_E)$ &   $\alpha(B_H / B_E)$ &   $\overline{\alpha}$\\
        \hline
        $2.15\pm 0.08$  &   $0.798 \pm 0.026$   &   $49.3 \pm 0.7$  &   $17.42\pm 0.26$\\
        $83.85\pm 0.09$ &   $NAN$   &   $60.47\pm 0.10$ &   $72.16\pm 0.09$\\
        \hline
    \end{tabular}
    \caption{Zusammenfassung der Auswertung zu 3.}
    \label{tab:1}
\end{table}
\\Hier sind alle Werte in Grad angegeben. Weiterhin ist der eine Wert aus der Tabelle nicht berechenbar, da der Quotient größer als 1 ist und die Umkehrfunktionen in diesem Intervall nicht definiert sind.
Die erste Zeile entspricht den von uns aufgenommenen Werten. Hier ist ersichtlich, dass die $B_V$ Komponente falsch bestimmt sein muss. Dies kann unter anderem daran liegen, dass die Stromstärke entweder falsch abgelesen wurde oder die Verkabelung falsch war, da ein Wert 
von $I = (0.94 \pm 0.01)*10^{-3} A$ zu viel gering ist. Die Auswertung der Werte vom Professor sind dafür da, die Rechnung zu verifizieren, da dieselben Rechenschritte gemacht wurden.
Vergleicht man unseren Wert mit dem Literaturwert von $\alpha = 66 $ Grad, so beträgt die Abweichung weniger überraschend $\sigma = 187$.
\section{Diskussion}
Zuerst wurde das Induktionsgesetz überprüft. Aus den Messwerten wurde das Magnetfeld der Spule berechnet und mit dem Tatsächtlichen verglichen
\begin{align*}
    B_{theo} &= (3.02 \pm 0.08)*10^{-3} T\\
    B_{exp} &=  (3.3650 \pm 0.008)*10^{-3} T\\
    \sigma &= 4.5
\end{align*}
Hier wurde die signifikante Abweichung auf die zu geringe Unsicherheit der Messwerte zurückgeführt. Auch wurden die weiteren Diagramme erklärt wobei zusammenfassend die Erwartungen eingetroffen sind.
Im Teil des Lufttransformators wurde die Induktivität der Spule bestimmt. Diese beträgt
\begin{align*}
    L &= (28.3\pm 0.6)*10^{-3} H
\end{align*}
Weil wie schon erwähnt ein Vergleichswert nicht vorlag, kann man keine weiteren Aussagen treffen.\\
Am Ende wurde noch der Inklinationswinkel des Erdmagnetfeldes bestimmt, bzw wurde vergeblich versucht zu bestimmen. Denn wie aus Tabelle \ref{tab:1} zu erkennen ist, sind unsere Messwerte falsch. Dies könnte daran liegen, dass das Amperemeter zwar in Reihe geschaltet wurde, dennoch der Modus eventuell auf DC eingestellt war. Dieser Fehler ist schonmal passiert, weshalb vehement drauf geachtet wurde diesen zu vermeiden, dennoch wäre es möglich das dieser Vorlag.
Das der Fehler in der Umrechnung der Messwerte liegt ist ausgeschlossen, da der Winkel sofort von Radiant in Grad umgerechnet wurde.\\
Diesen Versuch kann man schwer verbessern, da statistische Fehler durch Mittelwertbildung und Ausgleichsgeraden möglichst reduziert werden und systematische Fehler letzendlich nur durch den Menschen als Fehlerquelle passieren können, da Verlustwiderstände vernachlässigbar sind und Rauschen durch ein Bandpassfilter sehr gut vermieden wurde.
Weiterhin wurde bei Umschalten z.B. der Frequenz etwas abgewartet bis dann letzendlich die induzierte Spannung gemessen wurde. Damit wird gewährleistet, dass auch wirklich bei konstanter Stromänderung die Amplitude gemessen wird.
\section{Anhang}
\subsection{Source Code}
\begin{verbatim}
    import uncertainties as unc
    import uncertainties.unumpy as unp
    from uncertainties.umath import atan, asin, acos
    import numpy as np
    import scipy.constants as const
    import matplotlib.pyplot as plt
    from scipy.optimize import curve_fit
    
    plt.style.use('classic')
    plt.rcParams["font.family"] = 'serif'
    plt.rcParams["figure.figsize"][0] = 10
    plt.rcParams["figure.figsize"][1] = 7
    plt.rcParams['errorbar.capsize'] = 2
    plotdata = False
    
    
def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def linear_fit(x,a,b):
    return a*x+b

def cos_fit(x, a, b):
    return a*np.cos(x*2*const.pi / 360 )

def plot_data(arg):
    plt.clf()
    plt.grid(ls='dotted')
    if arg == 1:
        plt.errorbar(unp.nominal_values(omega), unp.nominal_values(U_ind), 
            xerr=unp.std_devs(omega), yerr=unp.std_devs(U_ind), 
            fmt='none', marker='.', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(omega), linear_fit(unp.nominal_values(omega), *popt), 
            marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title(r'$U_{ind}$ in Abhängigkeit von $\omega$', size=20)
        plt.xlabel(r'$\omega$ [Hz]', size=18)
        plt.ylabel('$U_{ind}$ [V]', size=18)
        plt.savefig('results/V245Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.errorbar(unp.nominal_values(I), unp.nominal_values(U_ind), 
            xerr=unp.std_devs(I), yerr=unp.std_devs(U_ind), 
            fmt='none', color='darkred', marker='.', label='Messwerte')
        plt.plot(unp.nominal_values(I), linear_fit(unp.nominal_values(I), *popt), 
            marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title(r'$U_{ind}$ in Abhängigkeit von $I$', size=20)
        plt.xlabel(r'$I$ [A]', size=18)
        plt.ylabel('$U_{ind}$ [V]', size=18)
        plt.savefig('results/V245Diagramm2.pdf', format='PDF')
    if arg == 3:
        plt.errorbar(unp.nominal_values(alpha), unp.nominal_values(U_ind), 
            xerr=unp.std_devs(alpha), yerr=unp.std_devs(U_ind), 
            fmt='none', marker='.', color='darkred', label='Messwerte')
        #plt.plot(unp.nominal_values(alpha), 
            cos_fit(unp.nominal_values(alpha), *popt), 
            marker=None, ls='--', color='darkred', label='Ausgleichsfunktion')
        plt.title(r'$U_{ind}$ in Abhängigeit des Winkels $\alpha$', size=20)
        plt.xlabel(r'$\alpha$ [°]', size=18)
        plt.ylabel(r'$U_{ind}$ [V]', size=18)
        plt.savefig('results/V245Diagramm3.pdf', format='PDF')
    if arg == 4:
        plt.errorbar(unp.nominal_values(omega), unp.nominal_values(verh), 
            xerr=unp.std_devs(omega), yerr=unp.std_devs(verh), 
            ls='--', marker='.', color='darkred', label='Messwerte')
        plt.title(r'$\frac{U_{ind}}{U_{ein}}$ 
            als Funktion der Kreisfrequenz $\omega$', size=20)
        plt.xlabel(r'$\omega$ [Hz]', size=18)
        plt.ylabel(r'Verhältniss $\frac{U_{ind}}{U_{ein}}$', size=18)
        plt.savefig('results/V245Diagramm4.pdf', format='PDF')
    if arg == 5:
        plt.errorbar(unp.nominal_values(omega), unp.nominal_values(R_s), 
            xerr=unp.std_devs(omega), yerr=unp.std_devs(R_s), 
            fmt='none', marker='.', colot='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(omega), 
            linear_fit(unp.nominal_values(omega), *popt), 
            marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title(r'Verhältnis $R_S = \frac{U_{ein}}{I_{ein}}$ 
            als Funktion der Kreisfrequenz $\omega$', size=20)
        plt.xlabel(r'$\omega$ [Hz]', size=18)
        plt.ylabel(r'$R_S = \frac{U_{ein}}{I_{ein}}$ [$\frac{V}{A}$]', size=18)
        plt.savefig('results/V245Diagramm5.pdf', format='PDF')
    plt.tight_layout()
    plt.legend(frameon=True, loc='best')
    plt.show()
    return

# Konstanten
N_Helm = 124
N_Feld = 4000
r_helm = unc.ufloat(295, 1) * const.milli / 2
dist = unc.ufloat(147, 1) * const.milli / 2
area = unc.ufloat(41.7, 0.1) * const.centi ** 2

    # 1. Induktionsgesetz
    print('\n 1. Induktionsgesetz')
    ## U - f
    I = unc.ufloat(4, 0.1)
    frequ = unp.uarray([2.67, 5.9, 9, 12.31, 14.84], 0.015)
    U_ind = unp.uarray([0.86, 2.05, 3.14, 4.28, 5.15], 
        [0.05, 0.1, 0.08, 0.08, 0.06])
    omega = 2 * const.pi * frequ
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(omega), 
        unp.nominal_values(U_ind), sigma=unp.std_devs(U_ind))
    if plotdata : plot_data(1)
    m = unc.ufloat(popt[0], float(pcov[0, 0]**0.5))
    B_exp = m / area / N_Feld
    B_theo = const.mu_0 * 8 * N_Helm * I / np.sqrt(125) / r_helm
    ## U - I
    I = unp.uarray([0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5], 0.1)
    U_ind = unp.uarray([0.6, 1, 1.45, 1.9, 2.44, 2.82, 3.20, 3.75, 4.1, 4.7], 
        [0.05, 0.05, 0.05, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1])
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(I), 
        unp.nominal_values(U_ind), sigma=unp.std_devs(U_ind))
    if plotdata : plot_data(2)
    
    ##Feedback
    print('Die theoretische Magnetfeldstärke beträgt '+str(B_theo)+ ' T')
    print('Die experimentelle Magnetfeldstärke beträgt '+str(B_exp)+ ' T')
    print('Die Abweichung beträgt '+str(calc_sigma(B_theo, B_exp)))
    
    # 2. Lufttransformator
    print('\n 2. Lufttransformator')
    ## U - alpha
    alpha = unp.uarray([0, 30, 60, 90, 120, 150, 180], 2)
    U_ind = unp.uarray([0.94, 0.8, 0.46, 0.06, 0.54, 0.87, 0.98], 0.01)
    frequ = unc.ufloat(118.2, 0.3)
    #popt, pcov = curve_fit(cos_fit, unp.nominal_values(alpha), 
        unp.nominal_values(U_ind), sigma=unp.std_devs(U_ind))
    if plotdata : plot_data(3)
    ## U_ind / U_ein - omega
    frequ = unp.uarray([20.5, 39.8, 59.7, 79.7, 104.7, 123.1, 143.4, 
        164.7, 178.8, 202.0, 393, 600, 803.8, 1002, 1175, 1370, 1590, 1812, 1996], 0.1)
    U_ind = unp.uarray([0.72, 0.85, 0.9, 0.92, 0.94, 0.94, 0.94, 0.95, 
        0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.97, 0.98], 0.01)
    I = unp.uarray([269, 175, 125, 96, 74, 63, 62, 47, 44, 38, 19, 12, 8, 
        7.62, 6.85, 5.82, 5.11, 4.38, 3.84], 
        [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.2, 0.1, 
        0.1, 0.12, 0.02, 0.02, 0.02, 0.02, 0.02]) * const.milli
    U_ein = unp.uarray([1.48, 1.52, 1.46, 1.46, 1.46, 1.46, 1.46, 1.46, 
        1.46, 1.46, 1.46, 1.44, 1.44, 1.42, 1.42, 1.4, 1.38, 1.36, 1.34], 0.05)
    verh = U_ind / U_ein
    omega = 2 * const.pi * frequ
    R_s = U_ein / I
    popt, pcov = curve_fit(linear_fit, unp.nominal_values(omega), 
        unp.nominal_values(R_s), sigma=unp.std_devs(R_s))
    L = unc.ufloat(popt[0], pcov[0,0]**0.5)
    #Die Länge der Spule ist nicht bekannt
    #L_theo = 2 * N_Helm**2 * r_helm * const.mu_0 * 
        (const.pi * r_helm / (0.1 * 2 * r_helm / 2.2) - 4.941 / 4 / const.pi)
    if plotdata : plot_data(4)
    if plotdata : plot_data(5)
    
    ## Feedback
    print('Die Induktivität der Spule beträgt '+str(L)+' H')
    
    # 3. Magnetfeld der Erde
    print('\n 3. Magnetfeld der Erde')
    ## Ohne Kompensation
    omega = 2 * const.pi * unc.ufloat(15.15, 0.04)
    U_ind = unc.ufloat(162, 5) * const.milli / 2
    B = U_ind / area / N_Feld / omega
    I = unc.ufloat(0.94, 0.01) * const.milli
    U_ind_kom = unc.ufloat(60, 2) * const.milli / 2
    B_vert = const.mu_0 * 8 * N_Helm * I / np.sqrt(125) / r_helm
    B_hor = U_ind_kom / area / N_Feld / omega
    
    alpha = atan(B_vert / B_hor) * 360 / (2 * const.pi)
    alpha2 = 360/(2 * const.pi) * asin(B_vert / B)
    alpha3 = 260/(2 * const.pi) * acos(B_hor / B)
    ##Feedback
    print('Ohne Kompensation beträgt das Magnetfeld der Erde '+str(B)+ 'T')
    print('Die vertikale Komponente des Erdmagnetfeldes beträgt '+str(B_vert)+' T')
    print('Die horizontale Komponente des Erdmagnetfeldes beträgt '+str(B_hor)+' T')
    print('Der Inklinationswinkel beträgt mit arctan '+str(alpha))
    print('Der Inklinationswinkel beträgt mit arcsin '+str(alpha2))
    print('Der Inklinationswinkel beträgt mit arccos '+str(alpha3))
    print('Mittelwert '+str((alpha+alpha2+alpha3)/3))
    
    ## Werte von Dr. Wagner
    omega = 2 * const.pi * unc.ufloat(18.39, 0.2)
    U_ind = unc.ufloat(184, 2) * const.milli / 2
    B = U_ind / area / N_Feld / omega
    I = unc.ufloat(64.01, 0.1) * const.milli
    U_ind_kom = unc.ufloat(20.6, 0.2) * const.milli / 2
    omega = 2 * const.pi * unc.ufloat(18.85, 0.2)
    B_vert = const.mu_0 * 8 * N_Helm * I / np.sqrt(125) / r_helm
    B_hor = U_ind_kom / area / N_Feld / omega
    
    alpha = atan(B_vert / B_hor) * 360 / (2 * const.pi)
    #alpha2 = 360/(2 * const.pi) * asin(B_vert / B)
    alpha3 = 260/(2 * const.pi) * acos(B_hor / B)
    
    ##Feedback
    print('\n Ohne Kompensation beträgt das Magnetfeld der Erde '+str(B)+ 'T')
    print('Die vertikale Komponente des Erdmagnetfeldes beträgt '+str(B_vert)+' T')
    print('Die horizontale Komponente des Erdmagnetfeldes beträgt '+str(B_hor)+' T')
    print('Der Inklinationswinkel beträgt mit arctan '+str(alpha))
    #print('Der Inklinationswinkel beträgt mit arcsin '+str(alpha2))
    print('Der Inklinationswinkel beträgt mit arccos '+str(alpha3))
    print('Mittelwert '+str((alpha+alpha3)/2))
\end{verbatim}
\subsection{Plots}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\textwidth]{../results/V245Diagramm1.pdf}
    \caption{Die induzierte Spannung als Funktion der Kreisfrequenz}
    \label{fig:1}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../results/V245Diagramm2.pdf}
    \caption{Die induzierte Spannung als Funktion der Stromstärke}
    \label{fig:2}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../results/V245Diagramm3.pdf}
    \caption{Die induzierte Spannung als Funktion der Auslenkwinkels}
    \label{fig:3}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../results/V245Diagramm4.pdf}
    \caption{Das Verhältnis der Spannungen in Abhängigkeit der Kreisfrequenz}
    \label{fig:4}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../results/V245Diagramm5.pdf}
    \caption{Der Widerstand als Funktion der Kreisfrequenz}
    \label{fig:5}
\end{figure}
\end{document}