\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufgaben und Aufbau}{1}{}%
\contentsline {subsection}{\numberline {2.1}Theorie}{2}{}%
\contentsline {section}{\numberline {3}Durchführung}{3}{}%
\contentsline {subsection}{\numberline {3.1}Vorversuch}{3}{}%
\contentsline {subsection}{\numberline {3.2}Induktionsgesetz}{3}{}%
\contentsline {subsection}{\numberline {3.3}Lufttransformator}{3}{}%
\contentsline {subsection}{\numberline {3.4}Bestimmung des Erdmagnetfeldes}{3}{}%
\contentsline {section}{\numberline {4}Protokoll}{3}{}%
\contentsline {section}{\numberline {5}Auswertung}{7}{}%
\contentsline {subsection}{\numberline {5.1}Induktionsgesetz}{7}{}%
\contentsline {subsection}{\numberline {5.2}Lufttransformator}{8}{}%
\contentsline {subsection}{\numberline {5.3}Bestimmung des Erdmagnetfeldes}{9}{}%
\contentsline {section}{\numberline {6}Diskussion}{10}{}%
\contentsline {section}{\numberline {7}Anhang}{11}{}%
\contentsline {subsection}{\numberline {7.1}Source Code}{11}{}%
\contentsline {subsection}{\numberline {7.2}Plots}{15}{}%
