import numpy as np
import scipy.constants as const
import uncertainties as unc
import pandas as pd
import uncertainties.unumpy as unp
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from scipy.optimize import curve_fit
from scipy.stats import chi2

plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2
plotdata = True


def fit_func(f, V, W1, W2, n1, n2):
    return V / (np.sqrt(1 + 1 / (f / W1) ** (2 * n1)) * np.sqrt(1 + (f / W2) ** (2 * n2)))


def fit_func_square(f,V,W1,W2,n1,n2):
    return fit_func(f,V,W1,W2,n1,n2)**2


def linear_fit(x,c):
    return c*x

def linear_fit_2(x,a,b):
    return a*x+b

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))


def plot_data(arg):
    plt.clf()
    plt.grid('dotted')
    if arg == 1:
        plt.errorbar(unp.nominal_values(frequ), unp.nominal_values(g_aus), xerr=unp.std_devs(frequ), yerr=unp.std_devs(g_aus), color='darkred', fmt='none', label='Messwerte')
        plt.xscale('log')
        plt.yscale('log')
        plt.axis([3e2, 1.5e5, 8, 1.5E3])
        plt.xlabel(r'Frequenz $\nu$ [Hz]', size=18)
        plt.ylabel(r'g($\nu$)', size=18)
        plt.title('Gemessener Frequenzgang', size=20)
        plt.tight_layout()
        plt.legend(frameon=True, borderaxespad = 1, borderpad = 1, loc='best')
        plt.savefig('results/V243Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.errorbar(unp.nominal_values(frequ[start:stop]), unp.nominal_values(g_aus[start:stop]), xerr=unp.std_devs(frequ[start:stop]), yerr=unp.std_devs(g_aus[start:stop]), color='darkred', fmt='none', label='Messwerte')
        plt.plot(unp.nominal_values(frequ), fit_func(unp.nominal_values(frequ), *popt), marker=None, ls='--', label='Ausgleichsgerade')
        plt.xscale('log')
        plt.yscale('log')
        plt.axis([3e2, 1.5e5, 8, 1.5E3])
        plt.xlabel(r'Frequenz $\nu$ [Hz]', size=18)
        plt.ylabel(r'g($\nu$)', size=18)
        plt.title('Gemessener Frequenzgang', size=20)
        plt.tight_layout()
        plt.legend(frameon=True, borderaxespad = 1, borderpad = 1, loc='best')
        plt.savefig('results/V243Diagramm2.pdf', format='PDF')
    if arg == 3:
        plt.errorbar(unp.nominal_values(R), unp.nominal_values(D), xerr=unp.std_devs(R), yerr=unp.std_devs(D), fmt='none', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(R), linear_fit(unp.nominal_values(R), *popt2), marker=None, ls='-', color='darkblue', label='Ausgleichsgerade')
        plt.title('Bestimmung der Boltzmannkonstante $k_B$', size=20)
        plt.xlabel('Widerstand [$\Omega$]', size=18)
        plt.ylabel('$(U_{aus}^2-U_V^2)$ [$mV^2$]', size=18)
        plt.tight_layout()
        plt.legend(frameon=True, loc='best')
        plt.savefig('results/V243Diagramm3.pdf', format='PDF')
    if arg == 4:
        plt.errorbar(unp.nominal_values(T), unp.nominal_values(y), xerr=unp.std_devs(T), yerr=unp.std_devs(y), fmt='none', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(T), linear_fit(unp.nominal_values(T), *popt), marker=None, ls='-', label='Ursprungsausgleichsgerade')
        #plt.plot(unp.nominal_values(T), linear_fit_2(unp.nominal_values(T), *popt2), marker=None, ls='-', label='Ausgleichsgerade')
        plt.title('Boltzmannkonstante anhand der Temperatur', size=20)
        plt.xlabel('Temperatur [$K$]', size=18)
        plt.ylabel(r'$\frac{U_{aus}^2-U_V^2}{R} [\frac{V^2}{\Omega}]$', size=18)
        plt.tight_layout()
        plt.legend(frameon=True, loc='best')
        plt.savefig('results/V243Diagramm4.pdf', format='PDF')
    plt.show()
    return

# 3 Frequenzgang
print('\n 3. Frequenzgang des Verstärkers')
data = pd.read_csv('data/messdaten.txt', skiprows=1, delimiter='\s+')
data.columns = ['f', 'U', 'deg']
D = unc.ufloat(0.001, 0.001*0.002)
U_ein = unc.ufloat(0.2, 0.2*0.03)
U_aus = unp.uarray(data['U'], data['U']*0.03)
frequ = unp.uarray(data['f'], data['f']*50e-6)
g_aus = U_aus /(U_ein * D)

if plotdata : plot_data(1)

start = 20
stop = -50

p0 = [1000, 1000, 50000, 5, 5]
popt, pcov = curve_fit(fit_func, unp.nominal_values(frequ[start:stop]), unp.nominal_values(g_aus[start:stop]), sigma=unp.std_devs(g_aus[start:stop]), p0=p0)
B = integrate.quad(fit_func_square, unp.nominal_values(frequ[start]), unp.nominal_values(frequ[stop]), args=tuple(popt))
print('Das Integral beträgt: {value:.4e}'.format(value=B[0]))

if plotdata : plot_data(2)

# 2. Boltzmannkonstante
R = unp.uarray([5, 10, 15, 20, 25, 30], np.array([5, 10, 15, 20, 25, 30]) * 0.005) * const.kilo
U_aus = unp.uarray([2.386, 3.100, 3.618, 4.190, 4.484, 4.8791], [0.01/np.sqrt(102), 0.011/np.sqrt(102), 0.0143/np.sqrt(102), 0.0171/np.sqrt(102), 0.0176/np.sqrt(102), 0.019/np.sqrt(112)]) * const.milli
U_V = unc.ufloat(1.3032, 0.00531/np.sqrt(102)) * const.milli
D = U_aus**2-U_V**2
popt2, pcov2 = curve_fit(linear_fit, unp.nominal_values(R), unp.nominal_values(D), sigma=unp.std_devs(D))
print('c ist '+str(popt2[0]))

if plotdata : plot_data(3)

chisquare = np.sum((linear_fit(unp.nominal_values(R), *popt2)-unp.nominal_values(D))**2 / unp.std_devs(D)**2)
dof=5
chisquare_red = chisquare/dof
print('chi_square '+str(chisquare))
print('chi_square_red '+str(chisquare_red))

prob = (1-chi2.cdf(chisquare, dof)) * 100
print('Fitwahrscheinlichkeit '+str(prob))

## c = 4kTB
T = unc.ufloat(20.4, 0.3) + 273.15
B = unc.ufloat(B[0], 0.02*B[0])
k = popt2[0] / (4*T*B)
sigma = calc_sigma(k, 1.381e-23)
print('Die Boltzmannkonstante beträgt '+str(k))
print('Der statistische Fehler beträgt ' +str(unp.nominal_values(k)*(pcov2[0, 0]**0.5/popt2[0])))
print('Der Fehler zum Literaturwert ist '+str(sigma))

# 4. Zusatzaufgabe
print('\n 4. Zusatzaufgabe')
T = unp.uarray([50.05, 101.11, 152.15, 204.04, 254.65], [0.3, 0.5, 0.3, 0.2, 0.2]) + 273.15
R = unp.uarray([4777.8, 5557.1, 6325.1, 7093.6, 7831.2], [5, 5, 3, 4, 2])
U_aus = unp.uarray([2.452, 2.724, 3.006, 3.297, 3.585], [0.009/np.sqrt(104), 0.011/np.sqrt(114), 0.0125/np.sqrt(101), 0.013/np.sqrt(102), 0.013/np.sqrt(101)]) * const.milli
D = U_aus**2 - U_V**2
y = D/R

popt, pcov = curve_fit(linear_fit, unp.nominal_values(T), unp.nominal_values(y), sigma=unp.std_devs(y))
popt2, pcov2 = curve_fit(linear_fit_2, unp.nominal_values(T), unp.nominal_values(y), sigma=unp.std_devs(y))

if plotdata : plot_data(4)

c = unc.ufloat(popt, pcov[0, 0])
k = c / (4 * B)
c2 = unc.ufloat(popt2[0], pcov2[0, 0])
k2 = c2 / (4 * B)

chisquare = np.sum((linear_fit(unp.nominal_values(T), *popt) - unp.nominal_values(D))**2 / (unp.std_devs(D))**2)
dof=4
chisquare_red = chisquare/dof
prob = (1-chi2.cdf(chisquare, dof))*100

print('Mit der Ausgleichsgeraden durch den Ursprung ergibt sich k= '+str(k))
print('Mit der Ausgleichsgeraden ergibt sich k= '+str(k2))
print('Der Absolute Temperaturnullpunkt ist '+str(popt2[1])+' +/- '+str(pcov2[1, 1]**0.5)+' K')
print('Die Abweichung zum Literaturwert beträgt '+str(calc_sigma(k, const.k)))
print('Die Abweichung zum Literaturwert beträgt '+str(calc_sigma(k2, const.k)))
print('Die Fitwahrscheinlichkeit beträgt '+str(prob))