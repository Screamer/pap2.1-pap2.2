\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{1}{}%
\contentsline {section}{\numberline {4}Durchführung}{2}{}%
\contentsline {subsection}{\numberline {4.1}Qualitative Untersuchung des Rauschspektrums}{2}{}%
\contentsline {subsection}{\numberline {4.2}Die Rauschspannung als Funktion des Widerstands}{3}{}%
\contentsline {subsection}{\numberline {4.3}Frequenzgang des Verstärkers und Bandpassfilters}{3}{}%
\contentsline {subsection}{\numberline {4.4}Messung der Rauchspannung als Funktion der Temperatur}{3}{}%
\contentsline {subsection}{\numberline {4.5}Messprotokoll}{3}{}%
\contentsline {section}{\numberline {5}Auswertung}{3}{}%
\contentsline {subsection}{\numberline {5.1}Qualitative Untersuchung des Rauschspektrums}{3}{}%
\contentsline {subsection}{\numberline {5.2}Frequenzgang des Verstärkers und Bandpassfilters}{3}{}%
\contentsline {subsection}{\numberline {5.3}Die Rauschspannung als Funktion des Widerstands}{4}{}%
\contentsline {subsection}{\numberline {5.4}Messung der Rauchspannung als Funktion der Temperatur}{4}{}%
\contentsline {section}{\numberline {6}Diskussion}{5}{}%
\contentsline {section}{\numberline {7}Anhang}{6}{}%
\contentsline {subsection}{\numberline {7.1}Protokoll}{6}{}%
\contentsline {subsection}{\numberline {7.2}Source Code}{10}{}%
\contentsline {subsection}{\numberline {7.3}Plots}{14}{}%
