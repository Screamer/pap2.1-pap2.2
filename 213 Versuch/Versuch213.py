import uncertainties as unc
import uncertainties.unumpy as unp
from uncertainties.umath import exp
import numpy as np
import scipy.constants as const
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2
plotdata = True



def calc_sigma(x,y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))

def exponential_fit(x,a,b):
    return a*np.exp(-b*x)


def linear_fit(x,a):
    return a*x

def calc_omega_mean(f,T):
    return const.pi * unp.nominal_values(f) * (1 + np.exp(-popt[1] * unp.nominal_values(T)))
    #return unp.uarray(const.pi * f * (1 + np.exp(-popt[1] * unp.nominal_values(T))), np.sqrt((unp.std_devs(f) * (1 + np.exp(-popt[1] * unp.nominal_values(T))))**2+(popt[1]*const.pi*unp.nominal_values(f)*unp.std_devs(T)*np.exp(-popt[1] * unp.nominal_values(T)))**2))

def calc_omega_mean_err(f, T):
    return np.sqrt((unp.std_devs(f * const.pi) * (1 + np.exp(-popt[1] * unp.nominal_values(T))))**2+(popt[1]*const.pi*unp.nominal_values(f)*unp.std_devs(T)*np.exp(-popt[1] * unp.nominal_values(T)))**2)

def plot_data(arg):
    plt.clf()
    plt.grid('dotted')
    if arg == 1:
        plt.errorbar(unp.nominal_values(t), unp.nominal_values(f), xerr=unp.std_devs(t), yerr=unp.std_devs(f), fmt='none', marker='.', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(t), exponential_fit(unp.nominal_values(t), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsfunktion')
        plt.yscale('log')
        plt.title('Bestimmung der Dämpfungskonstante', size=20)
        plt.xlabel('Zeit T [$s$]', size=18)
        plt.ylabel('Logarithmus der Frequenz '+r'$\nu$'+r' [$\frac{1}{s}$]', size=18)
        plt.tight_layout()
        plt.legend(frameon=True, loc='best')
        plt.savefig('results/V213Diagramm1.pdf', format='PDF')
    if arg == 2:
        plt.errorbar(unp.nominal_values(omega_F), unp.nominal_values(omega_N), xerr=unp.std_devs(omega_F), yerr=unp.std_devs(omega_N), fmt='none', marker='.', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(omega_F), linear_fit(unp.nominal_values(omega_F), *popt), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade')
        plt.title('Nutationsbewegung', size=20)
        plt.ylabel(r'$\omega_N$ [Hz]', size=18)
        plt.xlabel(r'$\omega_F$ [Hz]', size=18)
        plt.tight_layout()
        plt.legend(frameon=True, loc='best')
        plt.savefig('results/V213Diagramm4.pdf', format='PDF')

    if arg == 3:
        plt.errorbar(omega_F[0:4], unp.nominal_values(T[0:4]), yerr=unp.std_devs(T[0:4]), xerr=omega_F_error[0:4], marker='.', fmt='none', ecolor='r', label='Messwerte l=15cm')
        plt.plot(omega_F[0:4], linear_fit(omega_F[0:4], *popt2), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade l=15cm')

        plt.errorbar(omega_F[4:8], unp.nominal_values(T[4:8]), yerr=unp.std_devs(T[4:8]), xerr=omega_F_error[4:8], marker='.', fmt='none', color='b', label='Messwerte l=20cm')
        plt.plot(omega_F[4:8], linear_fit(omega_F[4:8], *popt3), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade l=20cm')

        plt.errorbar(omega_F[8:12], unp.nominal_values(T[8:12]), yerr=unp.std_devs(T[8:12]), xerr=omega_F_error[8:12], marker='.', fmt='none', color='k', label='Messwerte l=15cm, 2 Massen')
        plt.plot(omega_F[8:12], linear_fit(omega_F[8:12], *popt4), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade 2 Massen l=15cm')

        plt.errorbar(omega_F[12:16], unp.nominal_values(T[12:16]), yerr=unp.std_devs(T[12:16]), xerr=omega_F_error[12:16], marker='.', fmt='none', color='k', label='Messwerte l=20cm, 2 Massen')
        plt.plot(omega_F[12:16], linear_fit(omega_F[12:16], *popt5), marker=None, ls='--', color='darkblue', label='Ausgleichsgerade 2 Massen l=15cm')

        plt.title('Kreisfrequenzen', size=20)
        plt.ylabel('Kreisfrequenz '+r'$\omega$ [Hz]', size=18)
        plt.xlabel('Zeit t [s]', size=18)
        plt.tight_layout()
        plt.legend(frameon=True, loc='best')
        plt.savefig('results/V213Diagramm2.pdf', format='PDF')
    if arg == 4:
        plt.errorbar(unp.nominal_values(omega_F), unp.nominal_values(Omega), xerr=unp.std_devs(omega_F), yerr=unp.std_devs(Omega), fmt='none', color='darkred', label='Messwerte')
        plt.plot(unp.nominal_values(omega_F), linear_fit(unp.nominal_values(omega_F), *popt), ls='--', marker=None, color='darkblue', label='Ausgleichsgerade')

        plt.title('Umlauf der momentanen Drehachse', size=20)
        plt.ylabel('Winkelgeschwindigkeit $\Omega$ [Hz]', size=18)
        plt.xlabel('Eigenrotation $\omega_F$ [Hz]', size=18)
        plt.tight_layout()
        plt.legend(frameon=True, loc='best')
        plt.savefig('results/V213Diagramm3.pdf', format='PDF')
    plt.show()


# 2. Dämpfung des Kreisels
print('\n 2. Dämpfung des Kreisels')
t = unp.uarray([0, 120, 240, 360, 480, 600, 720], 1) #s
f = unp.uarray([645, 587, 548, 506, 467, 430, 400], 3) / 60 #Hz
omega = 2*const.pi*f
popt, pcov = curve_fit(exponential_fit, unp.nominal_values(t), unp.nominal_values(f), sigma=unp.std_devs(f), p0=(600, 0.0005))
T_halb = np.log(2)/unc.ufloat(popt[1], pcov[1,1]**0.5)

if plotdata : plot_data(1)

## Feedback
print('Amplitude a = '+str(popt[0]) + '+/-'+str(pcov[0,0]**0.5))
print('Dämpfung b = '+str(popt[1]) + '+/-'+str(pcov[1,1]**0.5)+' Hz')
print('Die Halbwertszeit beträgt '+str(T_halb)+' sec')

# 3. Präzession
print('\n 3. b) Präzession')
## Reihenfolge: 15cm, 20cm, 15cm, 20cm
l = unp.uarray([15, 20, 15, 20], 0.01) * const.centi #m
f = unp.uarray([700, 550, 400, 250,
                700, 550, 400, 250,
                700, 550, 400, 250,
                700, 550, 400, 250], 3) / 60 #Hz
T = unp.uarray([131.93, 105.65, 78.26, 50.70,
                102.09, 81.21, 60.09, 37.45,
                67.89, 53.54, 39.31, 25.00,
                51.70, 40.37, 29.65, 18.92], 1) #s

omega_F = []
omega_F_error = []
for i in range(0, 4):
    omega_F = np.append(omega_F, calc_omega_mean(f[4*i:4*i+4], T[4*i:4*i+4]))
    print(omega_F)
    omega_F_error = np.append(omega_F_error, calc_omega_mean_err(f[4*i:4*i+4], T[4*i:4*i+4]))

popt2, pcov2 = curve_fit(linear_fit, omega_F[0:4], unp.nominal_values(T[0:4]), sigma=unp.std_devs(T[0:4]))
popt3, pcov3 = curve_fit(linear_fit, omega_F[4:8], unp.nominal_values(T[4:8]), sigma=unp.std_devs(T[4:8]))
popt4, pcov4 = curve_fit(linear_fit, omega_F[8:12], unp.nominal_values(T[8:12]), sigma=unp.std_devs(T[8:12]))
popt5, pcov5 = curve_fit(linear_fit, omega_F[12:16], unp.nominal_values(T[12:16]), sigma=unp.std_devs(T[12:16]))


if plotdata : plot_data(3)

S = unp.uarray([float(popt2), popt3, popt4, popt5], [pcov2[0,0]**0.5, pcov3[0,0]**0.5, pcov4[0,0]**0.5, pcov5[0,0]**0.5]) #s^2
m = unp.uarray([9.86, 9.85, 2*9.85, 2*9.85], [0.1, 0.1, 0.2, 0.2]) * const.gram #kg
I_z = (m*const.g*l*S)/(2*const.pi)
I_z_mean = sum(I_z)/len(I_z)
#theorie
M = unc.ufloat(4.164, 0.001) #kg
R = unc.ufloat(5.05, 0.01) * const.centi
I_z_theo = 2/5 * M * R**2
## Feedback
print('Erste Messung b: ' +str(unc.ufloat(popt2, pcov2[0, 0]**0.5)))
print('Zweite Messung b: ' +str(unc.ufloat(popt3, pcov3[0, 0]**0.5)))
print('Dritte Messung b: ' +str(unc.ufloat(popt4, pcov4[0, 0]**0.5)))
print('Vierte Messung b: ' +str(unc.ufloat(popt5, pcov5[0, 0]**0.5)))
print('Demnach beträgtbI_z '+str(I_z))
print('Der Mittelwert beträgt '+str(I_z_mean))
print('Der theorertische Wert beträgt '+str(I_z_theo))
print('Die Abweichung ist '+str(calc_sigma(I_z_theo, I_z_mean)))

# 4.Figurenachse
print('\n 4. Umlauf um Figurenachse')
f = unp.uarray([604, 550, 520, 500, 470, 450, 400, 380, 350, 300], 3) / 60 #Hz
T = unp.uarray([16.98, 18.64, 20.02, 20.28, 22.10, 23.31, 26.24, 27.04, 29.68, 34.23], 1) #s
omega_F = 2 * const.pi * f
Omega = 2 * const.pi * 10 / T

popt, pcov = curve_fit(linear_fit, unp.nominal_values(omega_F), unp.nominal_values(Omega), sigma=unp.std_devs(Omega))

if plotdata : plot_data(4)

s = unc.ufloat(popt, pcov[0, 0]**0.5)
I_x = I_z_mean / (1-s)

## Feedback
print('Das Trägheitsmoment I_x beträgt '+str(I_x))

# 5 Nutation
print('\n 5. Nutation')
f_1 = unp.uarray([800, 750, 700, 650, 600, 550, 500, 450, 400], 3) / 60
f_2 = unp.uarray([770, 720, 675, 630, 585, 535, 490, 450, 400], 3) / 60

omega_F = 2*const.pi * f_1
omega_N = 2*const.pi * f_2

# Geraten die letzten Messwerte zu vernachlässigen
popt, pcov =  curve_fit(linear_fit, unp.nominal_values(omega_F[:-3]), unp.nominal_values(omega_N[:-3]), sigma=unp.std_devs(omega_N[:-3]))

if plotdata : plot_data(2)

s = unc.ufloat(popt, pcov[0, 0]**0.5)
I_x_2 = I_z_mean / s

## Feedback
print('Das Trägheitsmoment I_x beträgt '+str(I_x_2))
print('Die Abweichung zum Vorherigen ist '+str(calc_sigma(I_x_2, I_x)))