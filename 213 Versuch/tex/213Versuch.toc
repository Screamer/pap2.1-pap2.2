\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{2}{}%
\contentsline {subsection}{\numberline {3.1}Kräftefreier Kreisel}{2}{}%
\contentsline {subsection}{\numberline {3.2}Schwere Kreisel}{3}{}%
\contentsline {section}{\numberline {4}Durchführung}{3}{}%
\contentsline {subsection}{\numberline {4.1}Qualitative Beobachtung des Kreisels}{3}{}%
\contentsline {subsection}{\numberline {4.2}Dämpfung des Kreisels}{4}{}%
\contentsline {subsection}{\numberline {4.3}Präzession}{4}{}%
\contentsline {subsection}{\numberline {4.4}Umlauf der momentanen Drehachse um die Figurenachse}{4}{}%
\contentsline {subsection}{\numberline {4.5}Nutation}{4}{}%
\contentsline {section}{\numberline {5}Auswertung}{4}{}%
\contentsline {subsection}{\numberline {5.1}Qualitative Beobachtung des Kreisels}{4}{}%
\contentsline {subsection}{\numberline {5.2}Dämpfung des Kreisels}{5}{}%
\contentsline {subsection}{\numberline {5.3}Präzession}{5}{}%
\contentsline {subsection}{\numberline {5.4}Umlauf der momentanen Drehachse um die Figurenachse}{7}{}%
\contentsline {subsection}{\numberline {5.5}Nutation}{8}{}%
\contentsline {section}{\numberline {6}Diskussion}{8}{}%
\contentsline {section}{\numberline {7}Anhang}{9}{}%
\contentsline {subsection}{\numberline {7.1}Messprotokoll}{9}{}%
\contentsline {subsection}{\numberline {7.2}Source Code}{14}{}%
\contentsline {subsection}{\numberline {7.3}Plots}{18}{}%
