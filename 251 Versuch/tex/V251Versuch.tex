\documentclass{article}

\usepackage[ngerman]{babel}
\usepackage{amsmath, amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{wrapfig}
\usepackage{pdfpages}
\usepackage{xparse}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage[shortlabels]{enumitem}

\pagestyle{fancy}
\fancyhf{}
\rhead{mm268}
\lhead{Dezember 2021}
\cfoot{\thepage}

\begin{document}
\begin{titlepage}
    \title{Versuch 251 \\ Statistik des radioaktiven Zerfalls}
    \author{Jan Bartels}
    \date{Dezember 2021}
    \maketitle
    \tableofcontents
\end{titlepage}
\section{Einleitung}
In diesem Versuch wird die Statistik des radioaktiven Zerfalls eines Präparates untersucht. Hierfür werden Histogramme aufgenommen und anhand der Messwerte Aussagen über die Ähnlichkeit der Gauß- und Poissonverteilung getroffen.
\section{Aufgaben und Aufbau}
Es wurde folgendes aufgetragen:
\begin{enumerate}
    \item Messung der Zählrohrcharakteristik
    \item Untersuchung des Plateauanstiegs
    \item Verifizierung der statistischen Natur des radioaktiven Zerfalls
    \item Vergleich der Poisson- und Gauß- Verteilung bei sehr kleinen Zählraten
\end{enumerate}
Zur Verfügung gestellt wurde
\begin{itemize}
    \item Geiger-Müller Zählrohr mit Betriebsgerät
    \item externer Impulszähler
    \item PC mit Drucker
    \item Präparathalterung mit Bleiabschirmung
    \item Radioaktives Präparat ($^{60}Co$ oder $^{137}Cs$)
\end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../data/aufbau.png}
    \caption{Aufbau des Versuches 251}
\end{figure}
Dieses Bild wurde auf dem von Dr. J. Wagner erstellten Skript zum physikalischen Anfängerpraktikum entnommen.
\section{Theorie}
Hinreichendes Wissen für diesen Versuch ist die Kenntnis der oben erwähnten Verteilungen sowie deren Unterschiede.
Um die Verteilungen auf ihre charakteristischen Kennwerte zu untersuchen wird ein Zufallsereignis benötigt. Hierfür wird der radioaktive Zerfall benutzt, da über den einzelnen Zerfall eines Atomes keine Aussage getroffen werden kann, sehr wohl aber über die Summe aller. Damit wird hervorgehoben, 
dass dieser Prozess binomialverteilt ist mit annäherungsweise konstanter Wahrscheinlichkeit $p$. Fordert man, dass der Mittelwert $n$ Messungen $\langle k \rangle = np$ endlich bleibt, wobei $n$ eine sehr große Anzahl annimmt, sodass im Umkehrschluss die Ereigniswahrscheinlichkeit $p$ gegen Null geht, so lässt sich die Wahrscheinlichkeitsverteilung 
durch die Poisson-Verteilung annähern.
\begin{align}
    P(k,\mu) &= \frac{\mu^ke^{-\mu}}{k!}\\
    \langle k \rangle &= \mu \\
    \sigma &= \sqrt{\mu} 
\end{align}
$\sigma$ bezeichnet die Standardabweichung und ist ein Maß für die Breite der Kurve. Für einen großen Mittelwert $\mu > 30$, lässt sich die Poisson-Verteilung durch eine Gauß-Verteilung approximieren
\begin{align}
    G(k, \mu, \sigma) &= \frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(\mu-k)^2}{2\sigma^2}}\\
    \langle k \rangle &= \mu \\
    \sigma &= \sqrt{\mu}
\end{align}
In der Auswertung werden die Messwerte durch diese beiden Verteilungen extrapoliert. Die jeweiligen Kenngrößen werden miteinander verlgichen und die Stimmigkeit der Approximationen evaluiert.
\section{Durchführung}
\subsection{Messung der Zählrohrcharakteristik}
Das Zählgerät wurde gemäß der Anleitung eingestellt. Das Präparat wurde auf einen Abstand gebracht, sodass die Zählrate zwischen 50 bis 100 Ereignisse / Sekunde betrug. Dann wurde angefangen bei der Einsatzspannung $V_E$ die Zählrate gemessen bis zu einer Spannung von $V_E+150V$ in festen Intervallschritten von $25V$.
\subsection{Untersuchung des Plateauanstiegs}
Das Präparat wurde dicht an das Zählrohr gebracht. Sodann wurde für eine Zeit von 1er Minute und 3 Minuten die Zählrate bei den Spannungen $U_0$ und $U_0 + 100 V$ gemessen.
\subsection{Verifizierung der statistischen Natur des radioaktiven Zerfalls}
Das Präparat wurde dem Zählrohr so genähert, sodass die Zählrate 140-150 Zerfälle / Sekunde entsprach. Dann wurde das Messprogramm $Statistik$ gestartet, eine Torzeit von 0.5s eingestellt und die Messung begonnen. Mit den Messergebnissen wurde eine Gaußkurve sowie eine Poissonkurve erstellt und geplottet. Auch wurde die Anzahl an Messungen, der Mittelwert und die Standardabweichung notiert.
\subsection{Vergleich der Poisson- und Gauß- Verteilung bei sehr kleinen Zählraten}
Diesemal wurde das Präparat so genähert, sodass die Zählrate 40-50 Teilchen / Sekunde betrug. Die Torzeit wurde auf 0.1s eingestellt und die Messung analog zu 3. automatisch durchgeführt.
\section{Protokoll}
\includepdf[pages=-]{../data/protokoll.pdf}
\section{Auswertung}
\subsection{Messung der Zählrohrcharakteristik}
In dem Protokoll sind unsere Messwerte hinterlegt. Diese wurden in ein Diagramm gezeichnet. Für den Fehler der Ereignisrate wurde vom Messwert Wert $N$ die Wurzel als Fehler betrachtet. Anhand der Messwerte wurde eine Ausgleichsgerade extrapoliert. Diese ist mit den Messwerten in Abbildung \ref{fig:1} hinterlegt.
Die Steigung $m$ der Ausgleichsgerade beträgt
\begin{align*}
    m = 0.12 \pm 0.12
\end{align*}
Die Fitwahrscheinlichkeit beträgt $99\%$. Auffällig ist hier, dass die Plateausteigung fast 0 beträgt. 
\subsection{Untersuchung des Plateauanstiegs}
Aus den Messwerten wurde wie vorgegeben die Steigung $a$ berechnet
\begin{align}
    a &= N_2 - N_1\\
    \Delta a &= \Delta N_2^2 + \Delta N_1^2
\end{align}
Hierbei bezeichnet $N_2$ die Zählrate bei einer Spannung von $U_0 + 100V$.
Daraus wurde der prozentuale Anstieg berechnet
\begin{align}
    p &= a / N_1\\
    \Delta p &= \sqrt{(\frac{\Delta a}{N_1})^2+(\frac{a\Delta N_1}{N_1^2})^2}
\end{align}
\begin{table}[ht]
    \centering
    \label{tab1}
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        Steigung $1min$  &   Steigung $3min$   &   Proz. Steigung $1min$    &   Proz. Steigung $3min$   &   $\sigma$\\
        $45 \pm 76$ &   $48\pm 133$ &   $0.016 \pm 0.027$   &   $0.005\pm 0.016$    &   0.4
    \end{tabular}
    \caption{Zusammenfassung der Ergebnisse}
\end{table}
Hier ist sofort auffällig, dass die Fehler sehr groß sind. Die Berechnungen stimmen. Betrachtet man die Gleichung für den Fehler der Steigung $\Delta a$ und berücksichtigt, dass der Fehler der Ereigniszahl gerade der Wurzel derer entspricht, so ist es offentsichtlich, dass
der Fehler größer ist als der Messwert. Hier lässt sich das aber nicht ändern, da ein Fehler von $\sqrt{N}$ vorgegeben wurde im Skript.\\
Um noch abschließen auf die Aufgaben zurückzukommen:
\begin{itemize}
    \item Der Plateauanstieg berechnet sich aus der Differenz beider Zählraten, die direkt proportional zur Messzeit sind. D.h erhöht man die Messzeit um einen Faktor $\tau$, so wird sich die Steigung auch um diesen erhöhen. Der Fehler der Steigung erhöht sich aber nur um den Faktor $\sqrt{\tau}$. Der relative Fehler, also das Verhältnis beider skaliert daher um den Faktor $\frac{\Delta m}{m}=\frac{1}{\sqrt{\tau}}$. Daher muss man die uneren Fehler in Prozent quadrieren, um die Messzeit in Minuten zu erhalten. Das ergibt für uns 20 Tage.
    \item Ein Vertauensberecich von 68\% enspricht einem Intervall von 1$\sigma$. Dies sind gerade die oben angegebenen Intervalle. Bei einem von 95\% dementsprechend beispielsweise für die proz. Steigung bei einer Messzeit von 3min ergibt sich ein Intervall von [-0.038, 0.07]
\end{itemize}
\subsection{Verifizierung der statistischen Natur des radioaktiven Zerfalls}
Die Messwerte wurden durch eine Poisson- und Gaußverteilung extrapoliert. Weiterhin wurde Abbildung \ref{fig:2} erstellt, welche die Ähnlichkeit beider für hohe Mittelwerte unterstreicht.
Hier ergaben sich folgende charakteristischen Werte
\begin{align*}
    \text{Gaußverteilung}\\
    A &= (2.05 \pm 0.04)*10^3 [\frac{1}{s}]\\
    \mu &= (66.17 \pm 0.17) [\frac{1}{s}]\\
    \sigma &= (8.00 \pm 0.13) [\frac{1}{s}]\\
    \text{Poisson}\\
    A &= (2.05 \pm 0.04)*10^3 [\frac{1}{s}]\\
    \mu &= (66.19 \pm 0.16) [\frac{1}{s}]\\
    \sigma &= (8.14\pm 0.4) [\frac{1}{s}]\\
    \text{Vergleich}\\
    \sigma_A &= 0.0016\\
    \sigma_\mu &= 0.09\\
    \sigma_\sigma &= 0.4
\end{align*}
Hier trifft der wie zu erwartende Fall ein, dass der Mittelwert größer als 30 ist und somit die Gaußverteilung eine sichtbar sehr gute Näherung für die Poissonverteilung darstellt.
Die Fitwahrscheinlichkeit für die Gaußkurve beträgt $78\%$ und für die Poissonverteilung $87\%$
\subsection{Vergleich der Poisson- und Gauß- Verteilung bei sehr kleinen Zählraten}
Hier wurde dasselbe gemacht. Abbildung \ref{fig:3} zeigt unser Ergebnis. Hier ergab sich folgendes:
\begin{align*}
    \text{Gaußverteilung}\\
    A &= (5.44 \pm 0.18)*10^3 [\frac{1}{s}]\\
    \mu &= (3.83 \pm 0.08) [\frac{1}{s}]\\
    \sigma &= (2.19 \pm 0.07) [\frac{1}{s}]\\
    \text{Poisson}\\
    A &= (5.24 \pm 0.06)*10^3 [\frac{1}{s}]\\
    \mu &= (4.061 \pm 0.025) [\frac{1}{s}]\\
    \sigma &= (2.02\pm 0.16) [\frac{1}{s}]\\
    \text{Vergleich}\\
    \sigma_A &= 1.2\\
    \sigma_\mu &= 2.6\\
    \sigma_\sigma &= 1
\end{align*}
Hier weichen die Werte deutlich stärker voneinander ab. Das kann man auch gut in der oben erwähnten Abbildung erkennen. Dieses verifiziert daher die Aussage, dass die Annäherung nur für Mittelwerte von 30 und größer geeignet ist. 
Zwar ist immernoch die Gaußverteilung eine gute Näherung, da kein Wert signifikant abweicht, dennoch ist sie nicht mehr so präzise wie zuvor. Hier beträgt die Fitwahrscheinlichkeit $68\%$
\section{Diskussion}
Die erste Aufgabe diente dazu, aus dem Plateaubereich Messwerte zu entnehmen und die ebene Eigenschaft zu beweisen. Dies ist in diesem Versuch sehr gut gelungen. Kritisch betrachtet sollte sich dennoch eine Steigung ergeben, da der Plateaubereich nur annäherungsweise eben ist. In unserem Versuch jedoch liegt die Steigung 0 innerhalb des Fehlerbereiches.\\
In der darauffolgenden Aufgabe sollte dieser Bereich genauer untersucht werden und die prozentuale Steigung erörtert werden. Es kann aus unseren Messwerten geschlossen werden, das die Steigung wie zu erwarten zeit unabhängig ist. Das liegt daran, dass die Gesamtzerfallrate direkt proportional zu der Messzeit ist, jedoch durch das Dividieren durch den Urpsrungswert diese verloren geht.
Auch weichen die beiden Steigungen nicht signifikant voneinander ab.\\
Die letzten beiden Aufgaben ergaben zusammengefasst, dass die Gaußverteilung eine sehr präzise Näherung für die Poissonverteilung ist, solange der Mittelwert $\mu > 30$ ist. Da der Versuch lediglich auf die Aussagen der Statistik aufmerksam machen soll, werden die genauen Ergebnisse nicht weiter vertieft. Lediglich die Abweichungen der charakteristischen Werte sind relevant und diese sind in diesem Versuch ein 
Maß für die Genauigkeit. Mit diesen lässt sich die obige Aussage beweisen. Zwar sind die Abweichungen für die Auswertung mit kleiner mittlerer Ereigniszahl nicht signifikant, dennoch um bis zu einen Faktor von 700 größer als die aus der anderen Messung. Die großen Fitwahrscheinlichkeiten unterstützen diese Thesen, da diese auf eine erhöhte Eindeutigkeit hinweisen.\\
In diesem Versuch kann man nichts verbessern um ein signifikant verschiedenes Ergebnis zu erhalten, weil die Messungen vollständig automatisch verliefen und die Messunsicherheiten dadurch nur noch durch die Geräte bedingt sind. Diese wurden aber schon in den Fehlerrechnungen berücksichtigt.
\section{Anhang}
\subsection{Anmerkung}
Der Code wurde so geschrieben, dass man seine Messwerte sofort auswerten kann. Hierfür muss man seine Messwerte $data3.txt$ und $data4.txt$ benennen und in denselben Ordner wie das Programm stecken. Beim Starten des Programmes wird man weiterhin gefragt, ob die Diagramme angezeigt werden sollen und ob diese auch gespeichert werden sollen.
Auf diese Fragen kann man mit $false$ und $true$ antworten. Zuletzt wird man auch gefragt, ob eine Tabelle erstellt werden soll. Diese fasst alle Ergebnisse zu den Aufgaben 3 und 4 zusammen und wird in einem neu erstellten Ordner gespeichert mit den Diagrammen, falls so erwünscht. Zusätzlich kann man einen Ordner $A3$ und $A4$ erstellen und mehrere Datein zu den jeweiligen Aufgaben plazieren. Diese werden auch automatisch ausgewertet.
Dieses ermöglicht einem, die Messdaten von anderen Gruppen, welche auf dem Desktop hinterlassen wurden, simultan sehr schnell auszuwerten. Dies wurde nebenbei auch von uns erledigt. Diese sind beispielsweise in Abbildung \ref{fig:4} bis \ref{fig:13} zu sehen. Auch werden alle die Ergebnisse in einer Tabelle zusammengefasst.
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../data/toll.png}
    \caption{Beispielsweise Auswertung}
\end{figure}
\\Hier soll nur auf die Existenz und Möglichkeit eingegagen werden, nicht jedoch auf die Werte, weil diese nicht von uns aufgenommen wurden.
\subsection{Source Code}
\begin{verbatim}
    import pandas as pd
import numpy as np
import scipy.constants as const
from scipy.special import gamma
from scipy.stats import chi2
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import uncertainties as unc
import uncertainties.unumpy as unp
import prettytable as pt
import os


def calc_all_data(data_names_A3, data_names_A4, startindex):
    tab2 = pt.PrettyTable()
    tab2.title = 'Ergebnis aus allen Messwerten'
    tab2 = pt.PrettyTable(['A3', 'A Gauss 1/s', 'A Poisson 1/s', 
        'mu Gauss 1/s', 'mu Poisson 1/s', 'sigma A', 'sigma mu', 
        'Prob Guass', 'Prob Poiss'])
    for i in range(len(data_names_A3)):
        data = pd.read_csv(os.path.join('A3', data_names_A3[i]), delimiter='\s+')
        data.columns = ['A', 'N']  # A = Anzahl, N = Häufigkeit

        p = [2000, 75, 8]
        p2 = [2000, 75]
        popt, pcov = curve_fit(gaussian_fit, 
            data['A'][start:-start], 
            data['N'][start:-start], 
            sigma=data['N'][start:-start] ** 0.5, p0=p)
        popt2, pcov2 = curve_fit(poisson_fit, 
            data['A'][start:-start], 
            data['N'][start:-start], 
            sigma=data['N'][start:-start] ** 0.5, p0=p2)

        probability = calc_prob(gaussian_fit, popt, data['A'][start:-start], 
            unp.uarray(data['N'][start:-start], data['N'][start:-start] ** 0.5), 3)
        probability2 = calc_prob(poisson_fit, popt2, data['A'][start:-start], 
            unp.uarray(data['N'][start:-start], data['N'][start:-start] ** 0.5), 2)

        if plotdata: plot_data(2, data['A'], data['N'], gaussian_fit, popt, 
            poisson_fit, popt2, save_fit, 
            'results/V251Diagramm{0}.pdf'.format(startindex + i), 
            extra='hohe Ereignisrate (Gruppe{0})'.format(i))

        A = unc.ufloat(popt[0], pcov[0, 0]**0.5)
        a = unc.ufloat(popt2[0], pcov2[0, 0]**0.5)
        Mu = unc.ufloat(popt[1], pcov[1, 1]**0.5)
        mu = unc.ufloat(popt2[1], pcov2[1, 1]**0.5)
        Sig = calc_sigma(unc.ufloat(popt[0], pcov[0, 0] ** 0.5), 
            unc.ufloat(popt2[0], pcov2[0, 0] ** 0.5))
        sig = calc_sigma(unc.ufloat(popt2[1], pcov2[1, 1] ** 0.5), 
            unc.ufloat(popt[1], pcov[1, 1] ** 0.5))
        tab2.add_row(['Hohe Ereignisrate', A, a, Mu, mu, Sig, sig, 
            probability, probability2])

    for i in range(len(data_names_A4)):
        data = pd.read_csv(os.path.join('A4', data_names_A4[i]), 
            delimiter='\s+')
        data.columns = ['A', 'N']  # A = Anzahl, N = Häufigkeit

        popt, pcov = curve_fit(gaussian_fit, data['A'][:-2], 
            data['N'][:-2], sigma=data['N'][:-2] ** 0.5, p0=[7.6, 4.4, 2])
        popt2, pcov2 = curve_fit(poisson_fit, data['A'][:-2], 
            data['N'][:-2], sigma=data['N'][:-2] ** 0.5)

        probability = calc_prob(gaussian_fit, popt, data['A'][:-2], 
            unp.uarray(data['N'][:-2], data['N'][:-2] ** 0.5), 3)
        probability2 = calc_prob(poisson_fit, popt2, data['A'][:-2], 
            unp.uarray(data['N'][:-2], data['N'][:-2] ** 0.5), 2)

        if plotdata : plot_data(2, data['A'][:-1], data['N'][:-1], gaussian_fit, popt, 
            poisson_fit, popt2, save_fit, 
            'results/V251Diagramm{0}.pdf'.format(startindex + i + len(data_names_A3)), 
            extra='kleine Ereignisrate (Gruppe{0})'.format(i))

        A = unc.ufloat(popt[0], pcov[0, 0]**0.5)
        a = unc.ufloat(popt2[0], pcov2[0, 0]**0.5)
        Mu = unc.ufloat(popt[1], pcov[1, 1]**0.5)
        mu = unc.ufloat(popt2[1], pcov2[1, 1]**0.5)
        Sig = calc_sigma(unc.ufloat(popt[0], pcov[0, 0] ** 0.5), 
            unc.ufloat(popt2[0], pcov2[0, 0] ** 0.5))
        sig = calc_sigma(unc.ufloat(popt2[1], pcov2[1, 1] ** 0.5), 
            unc.ufloat(popt[1], pcov[1, 1] ** 0.5))

        tab2.add_row(['Kleine Ereignisrate', A, a, Mu, mu, Sig, 
            sig, probability, probability2])

    with open('results/AlleMesswerte.txt', 'w') as file:
        file.write(tab2.get_string())


def get_input(prompt):
    while True:
        try:
            return {'true': True, 'false': False}[input(prompt).lower()]
        except KeyError:
            print('Falsche Antwort')

def calc_sigma(x, y):
    return np.abs(unp.nominal_values(x) - unp.nominal_values(y)) / (
        np.sqrt(unp.std_devs(x) ** 2 + unp.std_devs(y) ** 2))


def calc_prob(fit, param, xdata, ydata, num) -> object:
    chi_square = np.sum((fit(unp.nominal_values(xdata), *param) 
        - unp.nominal_values(ydata))**2 / unp.std_devs(ydata) ** 2)
    dof = len(ydata) - num
    chi_square_red = chi_square / dof
    prob = round(1 - chi2.cdf(chi_square, dof), 2) * 100
    return prob

def linear_fit(x, a, b):
    return a*x+b

def gaussian_fit(x, A, mu, sig):
    return A/(np.sqrt(2*np.pi)*sig)*np.exp(-(x-mu)**2/2/sig**2)


def poisson_fit(x, A, mu):
    return A * np.exp(-mu) * mu**x/gamma(x+1)


def plot_data(arg, xdata, ydata, fit = None, popt_ = None, 
    fit2 = None, popt2_ = None, save = False, 
    title = None, extra = None):
    plt.clf()
    plt.grid(ls='dotted')
    if arg == 1:
        plt.errorbar(unp.nominal_values(xdata), unp.nominal_values(ydata), 
            xerr=unp.std_devs(xdata), 
            yerr=unp.std_devs(ydata), 
            fmt='none', marker='.', 
            color='darkred', 
            label='Messwerte')
        plt.plot(unp.nominal_values(xdata), 
            fit(unp.nominal_values(xdata), *popt_), 
            marker=None, ls='--', 
            color='darkblue', label='Ausgleichsgerade')
        plt.title('Zählrohrcharakteristik', size=20)
        plt.xlabel('Spannung / V', size=18)
        plt.ylabel('Ereignisse', size=18)
        if save : plt.savefig(title, format='PDF')

    if arg == 2:
        plt.errorbar(xdata, ydata, xerr=0, yerr=np.sqrt(ydata), 
            fmt='none', marker='.', color='darkred', label='Messwerte')
        plt.plot(xdata, fit(xdata, *popt_), marker=None, ls='--', 
            color='darkblue', label='Gaussfit')
        plt.plot(xdata, fit2(xdata, *popt2_), marker=None, 
            ls='-', color='black', label='Poisson')
        plt.title('Statistik des radioaktiven Zerfalls {0}'.format(extra), size=20)
        plt.xlabel(r'Anzahl der Zerfälle pro Zeiteinheit  [$\frac{1}{s}$]', size=18)
        plt.ylabel('Häufigkeit', size=18)
        plt.yscale('log')
        plt.tight_layout()
        plt.legend(frameon=True, loc=1, borderpad=1, borderaxespad=1)
        if save : plt.savefig(title, format='PDF')
    plt.tight_layout()
    plt.legend(frameon=True, loc='best', borderpad=1, borderaxespad=1)
    plt.show()
    return

# Einstellungen
plt.style.use('classic')
plt.rcParams["font.family"] = 'serif'
plt.rcParams["figure.figsize"][0] = 10
plt.rcParams["figure.figsize"][1] = 7
plt.rcParams['errorbar.capsize'] = 2
save_fit = get_input('Sollen die Diagramme gespeichert werden? ') #Speichern
plotdata = get_input('Sollen die Diagramme angezeigt werden? ') #Sofort zeichnen?
draw_table = get_input('Tabelle erstellen? ')
start = 10
start_index = 4
#Für die Auswertung aller
data_A3_name = os.listdir('A3/')
data_A4_name = os.listdir('A4/')
#calc_all_data(data_A3_name, data_A4_name, 4)


# 1. Zählrohrcharakteristik
print('\n 1. Zählrohrcharakteristik')
U = np.array([450, 470, 490, 510, 530, 
    550, 570, 590, 610, 630]) 
U = unp.uarray(U, 1)
N = np.array([0, 1196, 1440, 1437, 1474, 
    1429, 1458, 1446, 1467, 1456]) 
N = unp.uarray(N, np.sqrt(N)) # Hello there
popt, pcov = curve_fit(linear_fit, unp.nominal_values(U[2:]), 
    unp.nominal_values(N[2:]), sigma=unp.std_devs(N[2:]))
if plotdata : plot_data(1, U, N, linear_fit, popt, 
    save_fit, 'results/V251Diagramm1.pdf')

prob = calc_prob(linear_fit, popt, U[2:], N[2:], 2)
m = unc.ufloat(popt[0], np.sqrt(pcov[0, 0]))
## Feedback
print('Die Fitwahrscheinlichkeit beträgt '+str(prob))
print('Die Ausgleichsgeradensteigung beträgt '+str(m))

# 2. Plateauanstieg
print('\n 2. Untersuchung des Plateauanstiegs')
## Messdauer 60s
N0 = unp.uarray([2801, 8733], [2801**0.5, 8733**0.5]) #Messwerte
##Messdauer 180s
N1 = unp.uarray([2846, 8781], [2846**0.5, 8781**0.5]) #Messwerte

anstieg = N1 - N0
proz_anstieg = anstieg / N0

#Feedback
print('Der Anstieg beträgt '+str(anstieg))
print('Der prozentuale Anstiegt beträgt '+str(proz_anstieg))
print('Die Abweichung beträgt '
    +str(calc_sigma(proz_anstieg[0], proz_anstieg[1])))


# 3. Hoher mittlerer Ereigniszahl
print('\n 3. Hohe mittlere Ereigniszahl')
data = pd.read_csv('data3.txt', delimiter='\s+')
data.columns = ['A', 'N'] # A = Anzahl, N = Häufigkeit

p = [2000, 75, 8]
popt2, pcov2 = curve_fit(gaussian_fit, data['A'][start:-start], 
    data['N'][start:-start], sigma = data['N'][start:-start]**0.5, p0 = p)
p2 = [2000, 75]
popt3, pcov3 = curve_fit(poisson_fit, data['A'][start:-start], 
    data['N'][start:-start], sigma = data['N'][start:-start]**0.5, p0 = p2)

prob = calc_prob(gaussian_fit, popt2, data['A'][start:-start], 
    unp.uarray(data['N'][start:-start], data['N'][start:-start]**0.5), 3)
prob2 = calc_prob(poisson_fit, popt3, data['A'][start:-start], 
    unp.uarray(data['N'][start:-start], data['N'][start:-start]**0.5), 2)

if plotdata : plot_data(2, data['A'], data['N'], gaussian_fit, 
    popt2, poisson_fit, popt3, save_fit, 'results/V251Diagramm2.pdf', 
        extra = ', hoher Ereigniszahl')

## Feedback
print('Für den Gauß-Fit ergibt sich')
print('A = ' +str(unc.ufloat(popt2[0], pcov2[0, 0]**0.5)) + ' 1/s')
print('mu = ' +str(unc.ufloat(popt2[1], pcov2[1, 1]**0.5)) + ' 1/s')
print('sigma = ' +str(unc.ufloat(popt2[2], pcov2[2, 2]**0.5)) + ' 1/s')
print('\nFür Poisson-Fit ergibt sich')
print('A = '+str(unc.ufloat(popt3[0], pcov3[0, 0]**0.5))+ ' 1/s')
print('mu = '+str(unc.ufloat(popt3[1], pcov3[1, 1]**0.5))+ ' 1/s')
print('\nAbweichung A beträgt '+str(calc_sigma(unc.ufloat(popt2[0], pcov2[0, 0]**0.5), 
    unc.ufloat(popt3[0], pcov3[0, 0]**0.5))))
print('Abweichung mu beträgt '+str(calc_sigma(unc.ufloat(popt2[1], pcov2[1, 1]**0.5), 
    unc.ufloat(popt3[1], pcov3[1, 1]**0.5))))
print('\nDie Fitwahrscheinlichkeit beträgt für Gauss '+str(prob))
print('\nDie Fitwahrscheinlichkeit beträgt für Poisson '+str(prob2))

# 4. kleiner mittlerer Ereignisrate
print('\n4. kleine mittlere Ereignisrate')
data2 = pd.read_csv('data4.txt', delimiter='\s+')
data2.columns = ['A', 'N'] # A = Anzahl, N = Häufigkeit


popt4, pcov4 = curve_fit(gaussian_fit, data2['A'][:-2], 
    data2['N'][:-2], sigma = data2['N'][:-2]**0.5, p0 = [7.6, 4.4, 2])
popt5, pcov5 = curve_fit(poisson_fit, data2['A'][:-2], 
    data2['N'][:-2], sigma = data2['N'][:-2]**0.5)

prob = calc_prob(gaussian_fit, popt4, data2['A'][:-2], 
    unp.uarray(data2['N'][:-2], data2['N'][:-2]**0.5), 3)
prob2 = calc_prob(poisson_fit, popt5, data2['A'][:-2], 
    unp.uarray(data2['N'][:-2], data2['N'][:-2]**0.5), 2)

if plotdata : plot_data(2, data2['A'][:-1], data2['N'][:-1], gaussian_fit, 
    popt4, poisson_fit, popt5, save_fit, 'results/V251Diagramm3.pdf', 
        extra = 'kleine Ereigniszahl')

## Feedback
print('Für den Gauß-Fit ergibt sich')
print('A = ' +str(unc.ufloat(popt4[0], pcov4[0, 0]**0.5)) + ' 1/s')
print('mu = ' +str(unc.ufloat(popt4[1], pcov4[1, 1]**0.5)) + ' 1/s')
print('sigma = ' +str(unc.ufloat(popt4[2], pcov4[2, 2]**0.5)) + ' 1/s')
print('\nFür Poisson-Fit ergibt sich')
print('A = '+str(unc.ufloat(popt5[0], pcov5[0, 0]**0.5))+ ' 1/s')
print('mu = '+str(unc.ufloat(popt5[1], pcov5[1, 1]**0.5))+ ' 1/s')
print('\nAbweichung A beträgt '+str(calc_sigma(unc.ufloat(popt4[0], pcov4[0, 0]**0.5), 
    unc.ufloat(popt5[0], pcov5[0, 0]**0.5))))
print('Abweichung mu beträgt '+str(calc_sigma(unc.ufloat(popt4[1], pcov4[1, 1]**0.5), 
    unc.ufloat(popt5[1], pcov5[1, 1]**0.5))))
print('\n Die Fitwahrscheinlichkeit beträgt für Gauss '+str(prob))
print('\n Die Fitwahrscheinlichkeit beträgt für Poisson '+str(prob2))

## Tabelle
if draw_table:

    a31 = unc.ufloat(popt2[0], pcov2[0, 0]**0.5)
    a32 = unc.ufloat(popt3[0], pcov3[0, 0]**0.5)
    mu31 = unc.ufloat(popt2[1], pcov2[1, 1]**0.5)
    mu32 = unc.ufloat(popt3[1], pcov3[1, 1]**0.5)
    sig31 = calc_sigma(unc.ufloat(popt2[0], pcov2[0, 0]**0.5), 
        unc.ufloat(popt3[0], pcov3[0, 0]**0.5))
    sig32 = calc_sigma(unc.ufloat(popt2[1], pcov2[1, 1]**0.5), 
        unc.ufloat(popt3[1], pcov3[1, 1]**0.5))

    a41 = unc.ufloat(popt4[0], pcov4[0, 0]**0.5)
    a42 = unc.ufloat(popt5[0], pcov5[0, 0]**0.5)
    mu41 = unc.ufloat(popt4[1], pcov4[1, 1]**0.5)
    mu42 = unc.ufloat(popt5[1], pcov5[1, 1]**0.5)
    sig41 = calc_sigma(unc.ufloat(popt4[0], pcov4[0, 0]**0.5), 
        unc.ufloat(popt5[0], pcov5[0, 0]**0.5))
    sig42 = calc_sigma(unc.ufloat(popt4[1], pcov4[1, 1]**0.5), 
        unc.ufloat(popt5[1], pcov5[1, 1]**0.5))

    tab = pt.PrettyTable(['A3', 'A Gauss 1/s', 'A Poisson 1/s', 'mu Gauss 1/s', 
        'mu Poisson 1/s', 'sigma A', 'sigma mu'])
    tab.title = 'Zusammenfassung der Ergebnisse zu 3 und 4'
    tab.add_row(['Hohe Ereigniszahl', a31, a32, mu31, mu32, sig31, sig32])
    tab.add_row(['Kleine Ereigniszahl', a41, a42, mu41, mu42, sig41, sig42])

    with open('results/Zusammenfassung.txt', 'w') as file:
        file.write(tab.get_string())
\end{verbatim}
\subsection{Plots}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm1.pdf}
    \caption{Zählrohrcharakteristik}
    \label{fig:1}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm2.pdf}
    \caption{Statistik des Zerfalls bei hoher Ereignisrate}
    \label{fig:2}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm3.pdf}
    \caption{Statistik des Zerfalls bei mittlerer Ereignisrate}
    \label{fig:3}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm4.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:4}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm5.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:5}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm6.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:6}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm7.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:7}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm8.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:8}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm9.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:9}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm10.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:10}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm11.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:11}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm12.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:12}
\end{figure}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\textwidth]{../results/V251Diagramm13.pdf}
    \caption{Auswertung anhand der Messdaten anderer Gruppen}
    \label{fig:13}
\end{figure}
\end{document}
