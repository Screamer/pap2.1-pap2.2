\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufgaben und Aufbau}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{2}{}%
\contentsline {section}{\numberline {4}Durchführung}{2}{}%
\contentsline {subsection}{\numberline {4.1}Messung der Zählrohrcharakteristik}{2}{}%
\contentsline {subsection}{\numberline {4.2}Untersuchung des Plateauanstiegs}{2}{}%
\contentsline {subsection}{\numberline {4.3}Verifizierung der statistischen Natur des radioaktiven Zerfalls}{3}{}%
\contentsline {subsection}{\numberline {4.4}Vergleich der Poisson- und Gauß- Verteilung bei sehr kleinen Zählraten}{3}{}%
\contentsline {section}{\numberline {5}Protokoll}{3}{}%
\contentsline {section}{\numberline {6}Auswertung}{8}{}%
\contentsline {subsection}{\numberline {6.1}Messung der Zählrohrcharakteristik}{8}{}%
\contentsline {subsection}{\numberline {6.2}Untersuchung des Plateauanstiegs}{8}{}%
\contentsline {subsection}{\numberline {6.3}Verifizierung der statistischen Natur des radioaktiven Zerfalls}{9}{}%
\contentsline {subsection}{\numberline {6.4}Vergleich der Poisson- und Gauß- Verteilung bei sehr kleinen Zählraten}{10}{}%
\contentsline {section}{\numberline {7}Diskussion}{10}{}%
\contentsline {section}{\numberline {8}Anhang}{11}{}%
\contentsline {subsection}{\numberline {8.1}Anmerkung}{11}{}%
\contentsline {subsection}{\numberline {8.2}Source Code}{12}{}%
\contentsline {subsection}{\numberline {8.3}Plots}{18}{}%
