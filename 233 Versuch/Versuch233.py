import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy as sp
from scipy import signal
from scipy.optimize import curve_fit
from scipy.signal import argrelextrema
import uncertainties as unc
import uncertainties.unumpy as unp
import scipy.constants as const
from scipy.integrate import quad

plt.style.use('seaborn-white')
plt.rcParams['errorbar.capsize'] = 2
plt.rcParams['font.family'] = 'serif'


# Funktionen
def beugung_spalt(x):
    return pow(np.sinc(x), 2)


def linear(x, a, b):
    return a * x + b


def beugung_doppelspalt(x):
    return np.sinc(x) ** 2 * np.cos(const.pi * unp.nominal_values(verh) * x) ** 2


def fit_doppelspalt(x, I0):
    return I0 * np.sinc(x) ** 2 * np.cos(const.pi * unp.nominal_values(verh) * x) ** 2


def distance(pos):
    Pos = []
    if len(pos) < 2:
        True
    else:
        for i in range(len(pos) - 1):
            Pos.append(pos[i + 1] - pos[i])
    return np.round(Pos, 1)


def distance_err(pos_err):
    Pos_err = []
    if len(pos_err) < 2:
        True
    else:
        for i in range(len(pos_err) - 1):
            Pos_err.append(np.sqrt(pos_err[i + 1] ** 2 + pos_err[i] ** 2))
    return np.round(Pos_err, 1)


def plot_data(arg):
    plt.grid('dotted')
    if arg == 1:
        plt.clf()
        plt.plot(doppelspalt['Pixel'], doppelspalt['Intensität'], marker='.', ls='--', linewidth=0.8,
                 label='Messwerte Doppelspalt', color='darkred')
        plt.grid(ls='dotted')
        plt.title('Diagramm1a: Messwerte des Doppelspalts')
        plt.xlabel('Abstand [px]')
        plt.ylabel('Intensität [counts]')
        plt.xlim(800, 1500)
        plt.ylim(0, 4000)
        plt.grid(ls='dotted')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=2)
        plt.savefig('Results/Diagramm1a.pdf', format='PDF')
    if arg == 2:
        plt.clf()
        plt.plot(einzelspalt['Pixel'], einzelspalt['Intensität'], marker='.', ls='--', linewidth=0.8,
                 label='Messwerte Einzelspalt', color='darkred')
        plt.grid(ls='dotted')
        plt.title('Diagramm1b: Messwerte des Einzelspalt')
        plt.xlabel('Abstand [px]')
        plt.ylabel('Intensität [counts]')
        plt.xlim(800, 1500)
        plt.ylim(0, 4000)
        plt.grid(ls='dotted')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=2)
        plt.savefig('Results/Diagramm1b.pdf', format='PDF')
    if arg == 3:
        plt.clf()
        plt.plot(np.linspace(0.5, 6, 2), linear(np.linspace(0.5, 6, 2), *popt), color='black',
                 label='linearer Fit $\Delta x_{min}=a \cdot N$')
        plt.errorbar(min_order, unp.nominal_values(dist_min), unp.std_devs(dist_min), linestyle=' ', color='green',
                     marker='o', markersize=6, label='Messwerte Minima')
        plt.plot(unp.nominal_values(n_max_calc), unp.nominal_values(dist_max), linestyle=' ', color='blue', marker='o',
                 markersize=6, label='Messwerte Maxima')
        plt.title('Diagramm 2: Abstand der Minima für die ersten 5 Ordnungen')
        plt.xlabel('Beugungsordnung N')
        plt.ylabel('Abstand der Extrema $\Delta x [px]$')
        plt.grid(ls='dotted')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=2)
        plt.savefig('Results/Diagramm2.pdf', format='PDF')
    if arg == 4:
        plt.clf()
        plt.plot(x, beugung_spalt(x), color='darkred', label='Theoretische Figur')
        plt.title('Diagramm 3: Beugung am theo. Spalt')
        plt.ylabel('genormte Intensität')
        plt.xlabel('Beugungsordnung N')
        plt.grid(ls='dotted')
        plt.tight_layout()
        plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=2)
        plt.savefig('Results/Diagramm3.pdf', format='PDF')
    if arg == 5:
        plt.clf()
        plt.plot(x, beugung_spalt(x), color='darkred', label='Spalt')
        plt.plot(x, beugung_doppelspalt(x), color='darkblue', label='Doppelspalt')
        plt.title('Diagramm 4: Spalt und Doppelspalt')
        plt.ylabel('genormte Intensität')
        plt.xlabel(r'$\pi \cdot x$')
        plt.xlim(-3, 3)
        plt.ylim(0, 1.1)
        plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=1)
        plt.text(-2, 0.92, r'$V = 2.42$', bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10}, size=13)
        plt.savefig('Results/Diagramm4.pdf', format='PDF')
    if arg == 6:
        plt.clf()
        plt.plot(Y, f_modifiziert3, color='darkred', linestyle='--', linewidth=0.6,
                 label=r'Vergleichswert $k_y=1.94 \ \frac{1}{mm}$')
        plt.plot(Y, f_modifiziert2, color='darkblue', linestyle='--', linewidth=0.6,
                 label=r'Vergleichswert $k_y=1.84 \ \frac{1}{mm}$')
        plt.plot(Y, f_modifiziert, color='darkgreen', label=r'num. bestimmt: $k_y=1.7862 \ \frac{1}{mm}$')
        plt.title('Diagramm 19: Verschwinden der Doppelspaltstruktur', size=15)
        plt.xlabel('y/d', size=15)
        plt.ylabel('rel. Intensität [b.E.]', size=15)
        plt.ylim(0.25, 1.05)
        plt.legend(frameon=True)
        plt.savefig('Tests/Diagramm19.pdf', format='PDF')

    plt.show()


# Einlesen der Daten
doppelspalt = pd.read_csv('Messdaten/Doppelspalt.ccd', skiprows=14, delimiter='\s+')
einzelspalt = pd.read_csv('Messdaten/Einzelpalt.ccd', skiprows=14, delimiter='\s+')

doppelspalt.columns = ['Pixel', 'Intensität']
einzelspalt.columns = ['Pixel', 'Intensität']

spalt0 = pd.read_csv('Messdaten/Spalt00.ccd', skiprows=14, delimiter='\s+')
spalt1 = pd.read_csv('Messdaten/Spalt01.ccd', skiprows=14, delimiter='\s+')
spalt2 = pd.read_csv('Messdaten/Spalt02.ccd', skiprows=14, delimiter='\s+')
spalt3 = pd.read_csv('Messdaten/Spalt03.ccd', skiprows=14, delimiter='\s+')
spalt4 = pd.read_csv('Messdaten/Spalt04.ccd', skiprows=14, delimiter='\s+')
spalt5 = pd.read_csv('Messdaten/Spalt05.ccd', skiprows=14, delimiter='\s+')
spalt9 = pd.read_csv('Messdaten/Spalt09.ccd', skiprows=14, delimiter='\s+')

spalt0.columns = ['Pixel', 'Intensität']
spalt1.columns = ['Pixel', 'Intensität']
spalt2.columns = ['Pixel', 'Intensität']
spalt3.columns = ['Pixel', 'Intensität']
spalt4.columns = ['Pixel', 'Intensität']
spalt5.columns = ['Pixel', 'Intensität']
spalt9.columns = ['Pixel', 'Intensität']

doppelspalt_fourier = pd.read_csv('Messdaten/Doppel2.ccd', skiprows=14, delimiter='\s+')
doppelspalt_fourier.columns = ['Pixel', 'Intensität']

# Teil 1 : Einzelspalt
## Bestimmung der Abstände der Minima
min_order = np.array([1, 2, 3, 4, 5])
max_pos_left = unp.uarray([1052, 988, 920, 853, 787], 5)
max_pos_right = unp.uarray([1142, 1309, 1377, 1444, 1510], 5)
min_pos_left = unp.uarray([1082, 1015, 952, 884, 816], 5)
min_pos_right = unp.uarray([1214, 1280, 1374, 1413, 1479], 5)

dist_max = np.abs(max_pos_right - max_pos_left)
dist_min = np.abs(min_pos_right - min_pos_left)

# Eichung der Abszisse
pix_0 = unp.uarray([1662, 1542, 1477], 5)
pix_1 = unp.uarray([1542, 1477, 1412], 5)
d_0 = unp.uarray([1.69, 1.47, 1.24], 0.01)  # mm
d_1 = unp.uarray([1.47, 1.24, 1.02], 0.01)  # mm

dpix = pix_0 - pix_1
dist = (d_0 - d_1)

gauge = dist / dpix
gauge_mean = sum(gauge) / len(gauge)

popt, pcov = curve_fit(linear, min_order, unp.nominal_values(dist_min))

c = unc.ufloat(popt[0], pcov[0][0])
b = unc.ufloat(popt[1], 0)
a_mm = gauge_mean * c
# Einfügen der Maxima in das Diagramm (plot_data(3))
n_max_calc = (dist_max - b) / c
n_max_theo = np.arange(1.5, 6.5, 1)
n_diff = np.abs(n_max_theo - n_max_calc) / unp.std_devs(n_max_calc)

# Bestimmung der Spaltbreite
lambda_laser = 635 * const.nano
f1 = 80 * const.milli
breite = 2 * lambda_laser * f1 / (a_mm * const.milli)

# Intensitätsverhältnisse aus beiden Messungen
# Umrechnung der ersten Messung von der Intensität 0 Ordnung für 2te Messung, wo in Sättigung
max_0_intens = unc.ufloat(3704, 3704 * 0.015) - unc.ufloat(144, 3)
max_1_intens = unc.ufloat(296, 296 * 0.015) - unc.ufloat(144, 3)
verh = max_0_intens / max_1_intens
# jetzt mit 2ten Messwerten mit 0 Ordnung in Sättigung unter Abzug des Untergrundes
max_0_intens = verh * unc.ufloat(3455, 2455 * 0.015) - unc.ufloat(144, 144 * 0.015)
max_intensity_left = unp.uarray([3455, 1288, 720, 511, 392],
                                np.array([3455, 1288, 720, 511, 392]) * 0.015) - unc.ufloat(146, 146 * 0.015)
max_intensity_right = unp.uarray([3503, 1313, 816, 582, 441],
                                 np.array([3503, 1313, 816, 582, 441]) * 0.015) - unc.ufloat(144, 144 * 0.015)
max_intens = max_0_intens * np.ones(5)
rel_max_left = max_intensity_left / max_intens
rel_max_right = max_intensity_right / max_intens

# Folge nun dem Skriptbeispiel für die Beugung
a = 5
x = np.linspace(-(a + 1), a + 1, 200001)
theo = argrelextrema(beugung_spalt(x), np.greater_equal, order=1)
max_theo = np.array(beugung_spalt(x[theo]))
max_theo = max_theo[4::-1]
diff_theo1 = np.abs(max_theo - rel_max_right)
diff_theo2 = np.abs(max_theo - rel_max_left)
sigm_right = diff_theo1 / unp.std_devs(rel_max_right)
sigm_left = diff_theo2 / unp.std_devs(rel_max_left)

## Feedback
print('Teil 1: Einzelspalt')
print('Der Umrechnungsfaktor berechnet sich zu:' + str(gauge_mean))
print('Die Steigung der Ausgleichsgeraden beträgt: ' + str(c) + ' [px]')
print('Der y-Abschnitt beträgt: ' + str(unc.ufloat(popt[1], pcov[1][1])) + ' [px]')
print('Die Steigung in mm/N beträgt ' + str(a_mm))
print('Die Spaltbreite berechnet sich zu ' + str(breite) + ' [m]')
print('Die berechnete Ordnungsanzahl anhand der Steigung der Maxima beträgt: ' + str(n_max_calc))
print('Der Fehler zur erwarteten Ordnung beträgt' + str(n_diff))
print('Durch Skallierung besitzt das Maximum 0ter Ordnung die Intensität ' + str(max_0_intens))
print('Die Verhältnisse der Nebenmaxima links zum Hauptmaximum betragen:\n ' + str(rel_max_left))
print('Die Verhältnisse der Nebenmaxima rechts zum Hauptmaximum betragen:\n ' + str(rel_max_right))
print('Die theo. berechneten Verhältnisse sind:\n ' + str(max_theo))
print('Die Fehler links sind: \n' + str(sigm_left))
print('Die Fehler rechts sind: \n' + str(sigm_right))

# Teil 2: Doppelspalt
print('\n Teil 2: Beugungsstruktur des Doppelspaltes')
# Position der Ränder aus 4)
x_1 = unc.ufloat(920, 5)
x_2 = unc.ufloat(1044, 5)
x_3 = unc.ufloat(1213, 5)
x_4 = unc.ufloat(1327, 5)
## Berechnung der Spaltbreiten
d_l = np.abs(x_1 - x_2)
d_r = np.abs(x_3 - x_4)
d_m = np.append(d_l, d_r)
d_mean = sum(d_m) / len(d_m)

## Berechnung der Spaltweite
m1 = (x_2 + x_1) / 2
m2 = (x_4 + x_3) / 2
diff = np.abs(m1 - m2)
verh = diff / d_mean
## Vergrößerungsfaktor
dist2 = unc.ufloat(63, 0.3) * const.centi
verg = dist2 / f1
breite2 = d_mean * 14 * const.micro / verg
spaltabstand = diff * 14 * const.micro / verg

## Feedback
print('Teil 2: Doppelspalt')
print('Die Spaltbreite beträgt im Mittel d= ' + str(d_mean) + ' px')
print('Der Spaltabstand beträgt: ' + str(diff) + ' px')
print('Das Verhältnis beträgt: ' + str(verh))
print('Der Vergrößerungsfaktor beträgt: ' + str(verg))
print('Das ergibt eine Spaltbreite von: ' + str(breite2) + ' m')
print('Der Spaltabstand berträgt '+str(spaltabstand) + ' m')

##  Vergleich der Verhältnisse
### Theoretische Verhältnisse
theo2 = argrelextrema(beugung_doppelspalt(x), np.greater_equal, order=2000)
max_theo2 = np.array(beugung_doppelspalt(x[theo2]))
max_theo2 = max_theo2[14:8:-1]
print('\nDie theoretisch zu erwartenden Verhältnisse sind:\n' + str(max_theo2))
print('\nWeil dies auf das Maximum 0ter Ordnung bezogen ist, muss umgerechnet werden, da der Messwert fehlt..\n')

max_theo2 = np.delete(max_theo2, 0)
theo_verh = max_theo2 / max_theo2[0]
print('theoretisches Verhältnis ' + str(theo_verh))

### Experimentelle Werte
pos_max_rechts = unp.uarray([1162, 1189, 1224, 1241, 1307], 5)
pos_max_links = unp.uarray([1109, 1082, 1046, 1022, 964], 5)
intens_max_rechts = unp.uarray([2462, 380, 216, 226, 162], np.array([2462., 380., 216., 226., 162.]) * 0.015)
intens_max_links = unp.uarray([2256, 384, 202, 218, 150], np.array([2256., 384., 202., 218., 150.]) * 0.015)

pos_min_rechts = unp.uarray([1150, 1179], 5)
pos_min_links = unp.uarray([1121, 1092], 5)
intens_min_rechts = unp.uarray([244, 159], np.array([244., 159.]) * 0.015)
intens_min_links = unp.uarray([142, 117], np.array([142., 117.]) * 0.015)

untergrund_pos_rechts = unc.ufloat(1147, 5)
untergrund_intens_rechts = unc.ufloat(101, 101 * 0.015)
untergrund_pos_links = unc.ufloat(947, 5)
untergrund_intens_links = unc.ufloat(105, 105 * 0.015)

intens_max_links_corr = intens_max_links - untergrund_intens_links
intens_max_rechts_corr = intens_max_rechts - untergrund_intens_rechts

verh_links = intens_max_links_corr / intens_max_links_corr[0]
verh_rechts = intens_max_rechts_corr / intens_max_rechts_corr[0]

print('Die Verhältnisse sind links ' + str(verh_links))
print('Die Verhältnisse sind rechts ' + str(verh_rechts))

### Vergleich mit theoretischen Werten
sigma_rechts = np.abs(theo_verh - unp.nominal_values(verh_rechts)) / unp.std_devs(verh_rechts)
sigma_links = np.abs(theo_verh - unp.nominal_values(verh_links)) / unp.std_devs(verh_links)

print('\nDas bedeutet einen Fehler von ' + str(sigma_rechts) + ' Sigma zur rechten Seite')
print('\nDas bedeutet einen Fehler von ' + str(sigma_links) + ' Sigma zur linken Seite')
print('Damit sind 3 von 4 Wertepaaren signifikant verschieden!')

# Teil 3: Das Objektbild als Fouriersynthese
## a)
d = unp.nominal_values(breite)


def spalt(k):
    return d / np.pi * np.sin(k * d / 2) / (k * d / 2) * np.cos(yi * k)


def doppelspalt(k):
    return d / np.pi * np.cos(k * g / 2) * np.sin(k * d / 2) / (k * d / 2) * np.cos(yi * k)


# Funktion zur berechnung und zum plot des resultierenden Objektbildes
d = unp.nominal_values(breite)  # bestimmt aus Messung mit n=9, f=80mm und dist=63cm bestimmt


def picture(function, b, dateiname, speichername, spaltdaten=None):
    # Definieren der Funktionsvariable
    global n
    global g
    g = 2 * d
    n = b
    if function == spalt:
        y = np.linspace(-d, d, 301)
    else:
        y = np.linspace(-g, g, 501)

    # Berechnung des Objektbiles
    f_mod = []
    for i in range(len(y)):  # numerische Inegration
        global yi
        yi = y[i]
        result, error = quad(function, 0, 2 * np.pi * n / d)
        f_mod.append(result ** 2)
    f_mod = f_mod / np.max(f_mod)

    # Lage der Maxima
    max1 = argrelextrema(f_mod, np.greater_equal, order=1)
    f_max1 = f_mod[max1]
    y_max1 = y[max1]
    max2 = (f_max1 > 0.05)
    f_max = f_max1[max2]
    y_max = y_max1[max2]

    # Lage der Minima
    min1 = argrelextrema(f_mod, np.less_equal, order=1)
    f_min1 = f_mod[min1]
    y_min1 = y[min1]
    min2 = (f_min1 > 0.05)
    f_min = f_min1[min2]
    y_min = y_min1[min2]

    # Max der Spaltdaten
    if spaltdaten is not None:
        pos_rechts = 950
        pos_links = 1078.4
        mittelpunkt = pos_rechts + (pos_links - pos_rechts) / 2
        maxIntens = np.max(spaltdaten['Intensität'])
        x_norm = (spaltdaten['Pixel'] - mittelpunkt) / (pos_links - pos_rechts) * unp.nominal_values(breite)
        y_norm = spaltdaten['Intensität'] / maxIntens

    # Plot
    plt.figure(dateiname)
    if spaltdaten is not None: plt.plot(x_norm, y_norm, color='black', label='Messwerte normiert')
    plt.plot(y, f_mod, color='darkgreen', linewidth=0.8)
    plt.plot(y_max, f_max, linestyle='', marker='x', color='darkblue', label='Maxima')
    plt.plot(y_min, f_min, linestyle='', marker='x', color='darkred', label='Minima')
    plt.xlabel('y / d', size=15)
    plt.ylabel('rel. Intensität [b. E.]', size=15)
    plt.title('Diagramm %s' % dateiname, size=13)
    plt.ylim((0, 1.1))
    if spaltdaten is not None: plt.xlim(-d, d)
    plt.legend(frameon=True, fontsize='small', borderpad=1, borderaxespad=1, loc=1)
    plt.savefig('Tests/%s.pdf' % speichername)

    # Ausgabe der Lage der Maxima und Minima
    return y_max, f_max, y_min, f_min


# Plotten der Einzelspaltbilder
maxPos_spalt1, maxInt_spalt1, minPos_spalt1, minInt_spalt1 = picture(spalt, 1,
                                                                     '5: mod. Objektbild Einzelspalt, ein Maximum',
                                                                     'V233Diagramm5',
                                                                     spalt0)
maxPos_spalt2, maxInt_spalt2, minPos_spalt2, minInt_spalt2 = picture(spalt, 2,
                                                                     '6: mod. Objektbild Einzelspalt, zwei Maxima',
                                                                     'V233Diagramm6',
                                                                     spalt1)
maxPos_spalt3, maxInt_spalt3, minPos_spalt3, minInt_spalt3 = picture(spalt, 3,
                                                                     '7: mod. Objektbild Einzelspalt, drei Maxima',
                                                                     'V233Diagramm7',
                                                                     spalt2)
maxPos_spalt4, maxInt_spalt4, minPos_spalt4, minInt_spalt4 = picture(spalt, 4,
                                                                     '8: mod. Objektbild Einzelspalt, vier Maxima',
                                                                     'V233Diagramm8',
                                                                     spalt3)
maxPos_spalt5, maxInt_spalt5, minPos_spalt5, minInt_spalt5 = picture(spalt, 5,
                                                                     '9: mod. Objektbild Einzelspalt, fünf Maxima',
                                                                     'V233Diagramm9')
maxPos_spalt10, maxInt_spalt10, minPos_spalt10, minInt_spalt10 = picture(spalt, 10,
                                                                         '10: mod. Objektbild Einzelspalt, 10 Maxima',
                                                                         'V233Diagramm10',
                                                                         spalt9)

# Doppelspalt
d = unp.nominal_values(d_mean)
g = unp.nominal_values(diff)
# Plotten der Doppelspaltbilder mit zuver berechnetem d und g
maxPos_dspalt1, maxInt_dspalt1, minPos_dspalt1, minInt_dspalt1 = picture(doppelspalt, 1,
                                                                         '11: mod. Objektbild Doppelspalt, ein Maximum',
                                                                         'V233Diagramm11')
maxPos_dspalt2, maxInt_dspalt2, minPos_dspalt2, minInt_dspalt2 = picture(doppelspalt, 2,
                                                                         '12: mod. Objektbild Doppelspalt, zwei Maxima',
                                                                         'V233Diagramm12')
maxPos_dspalt3, maxInt_dspalt3, minPos_dspalt3, minInt_dspalt3 = picture(doppelspalt, 3,
                                                                         '13: mod. Objektbild Doppelspalt, drei Maxima',
                                                                         'V233Diagramm13')
maxPos_dspalt4, maxInt_dspalt4, minPos_dspalt4, minInt_dspalt4 = picture(doppelspalt, 4,
                                                                         '14: mod. Objektbild Doppelspalt, vier Maxima',
                                                                         'V233Diagramm14')
maxPos_dspalt5, maxInt_dspalt5, minPos_dspalt5, minInt_dspalt5 = picture(doppelspalt, 5,
                                                                         '15: mod. Objektbild Doppelspalt, fünf Maxima',
                                                                         'V233Diagramm15')
maxPos_dspalt15, maxInt_dspalt15, minPos_dspalt15, minInt_dspalt15 = picture(doppelspalt, 15,
                                                                             '16: mod. Objektbild Doppelspalt, 15 Maxima',
                                                                             'V233Diagramm16')
## Bestimmung der theoretischen Abstände
pos_rechts = 950
pos_links = 1078.4
dist_spalt1_max = distance(maxPos_spalt1 * (pos_links - pos_rechts) / unp.nominal_values(breite))
dist_spalt2_max = distance(maxPos_spalt2 * (pos_links - pos_rechts) / unp.nominal_values(breite))
dist_spalt3_max = distance(maxPos_spalt3 * (pos_links - pos_rechts) / unp.nominal_values(breite))
dist_spalt10_max = distance(maxPos_spalt10 * (pos_links - pos_rechts) / unp.nominal_values(breite))

dist_spalt1_min = distance(minPos_spalt1 * (pos_links - pos_rechts) / unp.nominal_values(breite))
dist_spalt2_min = distance(minPos_spalt2 * (pos_links - pos_rechts) / unp.nominal_values(breite))
dist_spalt3_min = distance(minPos_spalt3 * (pos_links - pos_rechts) / unp.nominal_values(breite))
dist_spalt10_min = distance(minPos_spalt10 * (pos_links - pos_rechts) / unp.nominal_values(breite))

## Werte aus Protokoll
maxPos_spalt1_ex = np.array([1018])
maxPos_spalt2_ex = np.array([986, 1053])
maxPos_spalt3_ex = np.array([976, 1016, 1055])
maxPos_spalt10_ex = np.array([959, 971, 986, 997, 1008, 1021, 1034, 1047, 1060, 1070])

maxPos_spalt1_ex_err = np.array([1])
maxPos_spalt2_ex_err = np.array([1, 1])
maxPos_spalt3_ex_err = np.array([1, 1, 1])
maxPos_spalt10_ex_err = np.ones(10)

minPos_spalt1_ex = np.array([])
minPos_spalt2_ex = np.array([1016])
minPos_spalt3_ex = np.array([994, 1034])
minPos_spalt10_ex = np.array([965, 976, 991, 1003, 1015, 1028, 1040, 1055, 1066])

minPos_spalt1_ex_err = np.array([])
minPos_spalt2_ex_err = np.array([1])
minPos_spalt3_ex_err = np.array([1, 1])
minPos_spalt10_ex_err = np.ones(9)

## Experimentelle Abstände
dist_spalt1_max_ex = distance(maxPos_spalt1_ex)
dist_spalt2_max_ex = distance(maxPos_spalt2_ex)
dist_spalt3_max_ex = distance(maxPos_spalt3_ex)
dist_spalt10_max_ex = distance(maxPos_spalt10_ex)

dist_spalt1_max_ex_err = distance_err(maxPos_spalt1_ex_err)
dist_spalt2_max_ex_err = distance_err(maxPos_spalt2_ex_err)
dist_spalt3_max_ex_err = distance_err(maxPos_spalt3_ex_err)
dist_spalt10_max_ex_err = distance_err(maxPos_spalt10_ex_err)

dist_spalt1_min_ex = distance(minPos_spalt1_ex)
dist_spalt2_min_ex = distance(minPos_spalt2_ex)
dist_spalt3_min_ex = distance(minPos_spalt3_ex)
dist_spalt10_min_ex = distance(minPos_spalt10_ex)

dist_spalt1_min_ex_err = distance_err(minPos_spalt1_ex_err)
dist_spalt2_min_ex_err = distance_err(minPos_spalt2_ex_err)
dist_spalt3_min_ex_err = distance_err(minPos_spalt3_ex_err)
dist_spalt10_min_ex_err = distance_err(minPos_spalt10_ex_err)

## Fehler anhand von Mittelwert
daten = unp.uarray(np.append(dist_spalt10_max_ex, dist_spalt10_min_ex),
                   np.append(dist_spalt10_max_ex_err, dist_spalt10_min_ex_err))
mittel = sum(daten) / len(daten)
sigma = (dist_spalt10_max[0] - unp.nominal_values(mittel)) / unp.std_devs(mittel)
## Feedback
print('Teil 3: Objektbild als Fouriersynthese')
print('Theoretische Abstände der Maxima:')
print('ein Maximum:', dist_spalt1_max)
print('zwei Maxima:', dist_spalt2_max)
print('drei Maxima:', dist_spalt3_max)
print('10 Maxima:', dist_spalt10_max)

print('Theoretische Abstände der Minima:')
print('kein Minimum:', dist_spalt1_min)
print('ein Minimum:', dist_spalt2_min)
print('zwei Minima:', dist_spalt3_min)
print('9 Minima:', dist_spalt10_min)

print('Experimentelle Abstände der Maxima:')
print('ein Maximum:', dist_spalt1_max_ex)
print('+-', dist_spalt1_max_ex_err)
print('zwei Maxima:', dist_spalt2_max_ex)
print('+-', dist_spalt2_max_ex_err)
print('drei Maxima:', dist_spalt3_max_ex)
print('+-', dist_spalt3_max_ex_err)
print('10 Maxima:', dist_spalt10_max_ex)
print('+-', dist_spalt10_max_ex_err)

print('Experimentelle Abstände der Minima:')
print('kein Minimum:', dist_spalt1_min_ex)
print('+-', dist_spalt1_min_ex_err)
print('ein Minimum:', dist_spalt2_min_ex)
print('+-', dist_spalt2_min_ex_err)
print('zwei Minima:', dist_spalt3_min_ex)
print('+-', dist_spalt3_min_ex_err)
print('9 Minima:', dist_spalt10_min_ex)
print('+-', dist_spalt10_min_ex_err)

print('Der mittlere Abstand beträgt: ' + str(mittel))
print('Sigmaabweichung zum theoretischen Wert: ' + str(sigma))

## b)
I_0 = unc.ufloat(2674, 2674 * 0.015)  # Entnommen aus dem Protokoll N=0
ug_links = unc.ufloat(131, 131 * 0.015)
ug_rechts = unc.ufloat(120, 120 * 0.015)
ug_mittel = (ug_links + ug_rechts) / 2
I0_max_corr = I_0 - ug_mittel
### n=1
ug_links = unc.ufloat(164, 164 * 0.015)
ug_rechts = unc.ufloat(171, 171 * 0.015)
I_max_01_1 = unc.ufloat(2326, 2326 * 0.015)
I_max_02_1 = unc.ufloat(2326, 2685 * 0.015)
I_max_01_1_corr = I_max_01_1 - (ug_rechts + ug_rechts) / 2
I_max_02_1_corr = I_max_02_1 - (ug_rechts + ug_links) / 2
### n=2
ug_rechts = unc.ufloat(176, 176 * 0.015)
ug_links = unc.ufloat(128, 128 * 0.015)
I_max_01_2 = unc.ufloat(2237, 2237 * 0.015)
I_max_02_2 = unc.ufloat(2242, 2242 * 0.015)
I_max_03_2 = unc.ufloat(2696, 2696 * 0.015)
I_max_01_2_corr = I_max_01_2 - (ug_rechts + ug_links) / 2
I_max_02_2_corr = I_max_02_2 - (ug_rechts + ug_links) / 2
I_max_03_2_corr = I_max_03_2 - (ug_rechts + ug_links) / 2
### Vergleich zu I_0
print('\n n=1:')
print('Das Verhältniss Max1 zum Max0 beträgt: ' + str(I_max_01_1_corr / I0_max_corr))
print('Das Verhältniss Max2 zum Max0 beträgt: ' + str(I_max_02_1_corr / I0_max_corr))
print('\n n=2')
print('Das Verhältniss Max2 zum Max0 beträgt: ' + str(I_max_01_2_corr / I0_max_corr))
print('Das Verhältniss Max2 zum Max0 beträgt: ' + str(I_max_02_2_corr / I0_max_corr))
print('Das Verhältniss Max2 zum Max0 beträgt: ' + str(I_max_03_2_corr / I0_max_corr))

# Teil 4: Fourierbild des Doppelspaltes
n_max_a = 1 / verh
print('Fall a) ist erfüllt für n = ', n_max_a)
maxPos_dspalt0, maxInt_dspalt0, minPos_dspalt0, minInt_dspalt0 = picture(doppelspalt, unp.nominal_values(n_max_a),
                                                                         '17: mod. Objektbild, Doppelspalt: Fall a)',
                                                                         'V233Diagramm17')
plt.text(-55, 0.9, '$n = 0.426$', bbox={'facecolor': 'white', 'alpha': 0.5, 'pad': 10}, size=13)


# Spaltfunktion definieren
def doppelspalt2(k):
    return d / np.pi * np.cos(k * g / 2) * np.sin(k * d / 2) / (k * d / 2) * np.cos(y * k)


d = 1
g = unp.nominal_values(verh) * d


x1 = [1, 2]
k_y = 2 * np.pi * 1 / g
Y = np.linspace(-d, d, 501)

# k_y stets verkleinern und Maxima zählen. Wenn nur noch ein Maximum da ist, dann k_y ausgeben
while len(x1) > 1:
    k_y = k_y - 0.001
    f_modifiziert = []
    for i in range(len(Y)):
        y = Y[i]
        result, error = quad(doppelspalt2, 0, k_y)
        f_modifiziert.append(result ** 2)
    C = np.max(f_modifiziert)
    f_modifiziert /= C
    max_mask = np.r_[f_modifiziert[1:] < f_modifiziert[:-1], True] & np.r_[
        True, f_modifiziert[:-1] < f_modifiziert[1:]];
    x1 = Y[np.where(max_mask)]
print(k_y)

k_y = 1.7341772623416603
k_y2 = 1.84
k_y3 = 1.94

Y = np.linspace(-2, 2, 10001) * d

# für dieses k_y und zwei Vergleichswerte das Objektbild plotten
f_modifiziert = []
for i in range(len(Y)):
    y = Y[i]
    result, error = quad(doppelspalt2, 0, k_y)
    f_modifiziert.append(result ** 2)
C = np.max(f_modifiziert)
f_modifiziert /= C

f_modifiziert2 = []
for i in range(len(Y)):
    y = Y[i]
    result, error = quad(doppelspalt2, 0, k_y2)
    f_modifiziert2.append(result ** 2)
C = np.max(f_modifiziert2)
f_modifiziert2 /= C

f_modifiziert3 = []
for i in range(len(Y)):
    y = Y[i]
    result, error = quad(doppelspalt2, 0, k_y3)
    f_modifiziert3.append(result ** 2)
C = np.max(f_modifiziert3)
f_modifiziert3 /= C
plot_data(6)

## Berechnung der k_y Werte
## a)
dist1 = unc.ufloat(0.24, 0.01)*const.milli
dist2 = unc.ufloat(0.03, 0.01)*const.milli
wavelength = 635 * const.nano
ky_theo_a = 2*const.pi / (breite2)
ky_exp_a = 2*const.pi / (wavelength*f1) * 2*dist1
sigma1 = np.abs(ky_theo_a-unp.nominal_values(ky_exp_a))/np.sqrt(unp.std_devs(ky_exp_a)**2+unp.std_devs(ky_theo_a)**2)
print('Für Fall a) erhält man:' +str(ky_theo_a) + ' 1/m')
print('Experimentell ergibt sich '+str(ky_exp_a) + ' 1/m')
print('Der Fehler beträgt ' + str(sigma1))
## b)
ky_theo_b = 2*const.pi * n_max_a / breite2
ky_exp_b = 2*const.pi / (wavelength*f1) * 2*dist2
sigma2 = np.abs(ky_theo_b-unp.nominal_values(ky_exp_b))/np.sqrt(unp.std_devs(ky_exp_b)**2+unp.std_devs(ky_theo_b)**2)
print('Für Fall b) erhält man: '+str(ky_theo_b) + ' 1/m')
print('Experimentell ergibt sich '+str(ky_exp_b) + ' 1/m')
print('Der Fehler beträgt '+str(sigma2))