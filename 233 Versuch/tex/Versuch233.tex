\documentclass{article}

\usepackage[ngerman]{babel}
\usepackage{amsmath, amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{wrapfig}
\usepackage{pdfpages}
\usepackage{xparse}
\usepackage[utf8]{inputenc}



\pagestyle{fancy}
\fancyhf{}
\rhead{mm268}
\lhead{Oktober 2021}
\cfoot{\thepage}

\begin{document}
    
\begin{titlepage}
    \title{Versuch 233 - Fourieroptik}
    \author{Jan Bartels}
    \date{Oktober 2021}
    \maketitle
    \tableofcontents
\end{titlepage}

\section{Einleitung}
In diesem Versuch untersuchen wir simultan das Beugungsbild und das übliche Bild 
eines Gegenstandes (hier Spalt, Gitter, ect.), welches uns durch einen 
geschickten Aufbau von optischen Geräten möglich ist. 
Durch geziehlte Eingriffe in die Fourierebene können wir weiterhin den Einfluss 
einzelner Beugungsordnungen auf das Objektbild sichtbar gemacht.

\section{Aufbau und Durchführung}
Daher hatten wir folgende Aufgaben:
\begin{enumerate}
    \item Qantitative Beobachtungen am Einfachspalt durchführen
    \item Beugungsstrukturen des Doppelspaltes untersuchen
    \item Das Objektbild als Fouriersynthese des Beugungsbildes betrachten
    \item Das Fourierbild des Doppelspaltes beobachten
\end{enumerate}
Folgendes wurde uns daher zur Verfügung gestellt:  
\begin{itemize}
    \item Singlemode Glasfaser-gekoppelter Diodenlaser mit regelbarer Stromversorgung und Justieroptik für einen Parallelstral
    \item Ein Satz Beugungsobjekte: Spalte, Doppelspalt, Gitter, Kreuzgitter, Spalt mit gaussförmigen Transmissionsprofil
    \item Verschiedene sphärische und Zylinder-Linsen
    \item Strahlteiler
    \item Graufilter
    \item Spiegel
    \item Verschiedene Fest-Reiter
    \item Feinjustierbare Verschiebereiter
    \item Symmetrisch öffnender Analysierspalt
    \item Verschiedene schmale Matallstriefen als Modenblende
    \item SSCD-Zeilenkamera mit PC und Monitor zur Aufnahme vom Beugungsbildern und Objektbildern, sowie Drucker  
\end{itemize}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{../Resources/aufbau.png}
    \caption{Aufbau}
\end{figure}
\section{Theorie}
Ziel dieses Versuches ist ein tieferes Verständis über die Fourierdarstellung und Rechnung zu gewinnen. 
Um der Anforderung "kurz und knapp" gerecht zu werden, lässt sich die Theorie auf folgendes reduzieren:\\
Nach der Fouriertransformation lässt sich jede periodische Funtkion $f(x)$ mit Periode $L$ approximieren:
\begin{align}
    F(k)&=\frac{1}{2\pi}\int_{-\infty}^{\infty}f(x)e^{-ikx}\,dx\\
    f(x) &= \int_{-\infty}^{\infty}F(k)e^{ikx}\,dx
\end{align}
Die zweite Formel ist gleichbedeutend mit der Rücktransformation.
Weil in diesem Versuch der Doppelspalt und der Einzelspalt untersucht werden, muss das Integral für 
beide gelöst werden.
Um die Fouriertransformation des Spaltes berechnen zu können, bietet sich folgende Annahme für die Spaltfunktion
$f(x)$ an:
\begin{equation}
    f(x) =
    \begin{cases}
        1 & \text{für $\left|{y}\right| \leq \frac{d}{2}$}\\
        0 & \textit{sonst}
    \end{cases}
\end{equation}
Einsetzen ergibt:

\begin{align}\label{eq:8}
    F(k_y) &= \int_{-\infty}^{\infty}f(y)e^{-ik_y y}\,dy\\
    F(k_y) &= \int_{-\frac{d}{2}}^{\frac{d}{2}}e^{-ik_y y}\,dy\\
    F(k_y) &= \frac{-1}{ik_y}(e^{ik_y\frac{d}{2}}-e^{-ik_y\frac{d}{2}})\\
    F(k_y) &= d\frac{\sin{(k_y\frac{d}{2})}}{k_y\frac{d}{2}}\\
    k_y &= \frac{2\pi n}{d}
\end{align}
Wenn man die letzte Gleichung wieder in $F(k_y)$ einsetzt, so erhält man 0. Das bedeutet, dass für diese Werte die 
Fouriertransformation Nullstellen annimmt.\\
Beim Doppelspalt muss man analog vorgehen.
\begin{equation}
    f(x) =
    \begin{cases}
        1 &  \left|{y}\right| \in \left[g-\frac{d}{2},g+\frac{d}{2}\right]\\
        0 & \text{sonst}
    \end{cases}
\end{equation}
Hier beschreibt $g$ den Spaltabstand und $d$ die Spaltbreite. Es folgt:
\begin{align}
    F(k_y) &= \int_{-g-\frac{d}{2}}^{-g+\frac{d}{2}}e^{-ik_yy}\,dy + \int_{g-\frac{d}{2}}^{g+\frac{d}{2}}e^{-ik_yy}\,dy\\
    F(k_y) &= \frac{-1}{ik_y}(e^{-ik_yy}\Big|_{-g-\frac{d}{2}}^{-g+\frac{d}{2}}+e^{-ik_yy}\Big|_{g-\frac{d}{2}}^{g+\frac{d}{2}})\\
    F(k_y) &= 2\cos{(k_y\frac{g}{2})}d\frac{\sin{(k_y\frac{d}{2})}}{k_y\frac{d}{2}}
\end{align}
Neben der Integraldarstellung bietet sich auch die Summendarstellung ein, sodass man die Einflüsse einzelner periodischer Funktionen 
auf die Gesamtfunktion veranschaulichen kann. Hierfür wird lediglich nicht mehr integriert, sondern bis zur 
Ordnung $n$ summiert:
\begin{align}
    f(x)_n &= \frac{a_0}{2} + \sum_{i=1}^{n}a_i\cos{(\frac{2\pi i}{L}x)}+b_n\sin{(\frac{2\pi i}{L}x)}\\
    a_i &= \frac{2}{L}\int_{-\frac{L}{2}}^{\frac{L}{2}}f(x)\sin{(\frac{2\pi i}{L}x)}\,dx
\end{align}
Letzendlich wird die Intensitätsverteilung betrachtet, weshalb folgende normierte Formeln von Interesse sind:
\begin{align}
    I(x)_1 &= \frac{\sin(x)^2}{x^2}\\
    I(x)_2 &= \frac{\sin(x)^2}{x^2}\cos(\frac{g}{d}x)^2
\end{align}
Der $b_i$ Koeffizient kann vernachlässigt werden, da dieser nur für ungerade Funktionen ungleich 0 ist. 
Weil der Spalt und Doppelspalt allerdings symmetrisch angeordnet sind, sind hier alle Terme 0.
Diese Summe wird angewand, wenn die Ordnungszahl nicht mehr ins Kontinuum fällt. Dies kann realisiert werden,
indem man aus der Fourierebene gezielt Ordnungen entfernt. Dazu bietet sich dieser Versuchsaufbau
perfekt an. Wie in Abbildung 1 zu sehen ist, wird Licht auf ein Objekt treffen (Spalt, Doppelspalt)
und gemäß des Huygenschen Prinzips gebrochen. Die Lichtstrahlen werden durch die Linse zur Interferenz im 
Brennpunkt gezwungen, weshalb dort die Fourierebene liegt. Danach trifft das Licht auf einen Strahlenteiler, 
der das Licht zu 50\% reflektiert. Das durchgelassene Licht trifft auf einen Schirm und das Objektbild ist erkennbar.
Das reflektierte Licht hingegen wird durch eine weitere Sammellinse gebündelt und trifft dann auf die 
ccd-Kamera. Dort ist das Beugungsbild sichtbar.\\
Aufgrund der Linse muss man noch einen Vergrößerungsmaßstab berücksichtigen.
Die Vergrößerung berechnet sich aus der tatsächlichen Objektgröße $G$, der reellen 
Bildgröße $B$ und den Abständen Gegenstand-Linse $g$ und Linse-Bild $b$ gemäß:
\begin{align}\label{eq:17}
    \frac{B}{G} &= \frac{b}{g}
\end{align}
Letzendlich gelten noch folgende Gleichungen für die Bedingung eines Maximums der Ordnung n:
\begin{align}\label{eq:19}
    n\lambda &= g\sin(\alpha)   &   \text{Doppelspalt mit Spaltabstand g}\\
    (n+\frac{1}{2})\lambda &= b\sin(\alpha) &   \text{Einzelspalt mit Spaltbreite b}\label{eq=minspalt}
\end{align}
$\lambda$ bezeichnet die verwendete Wellenlänge des Lichtes.
\section{Messprotokoll}
\subsection{Quantitative Beobachtungen am Einzelspalt}

Anstelle des Schirmes am Beugungsbild, plazierten wir die zur Verfügung 
gestelle CCD-Kamera. Diese wird die Intensität des Beugungsbildes als Funktion 
des Ortes wiedergeben. Hierbei handelt es sich um eine Auflösung von 2048 Pixel 
von jeweils $14\mu m$ Breite und $56\mu m $ Höhe. Weiterhin wurde eine 
Zylinderlinse vor die Kamera gestellt um das Beugungsbild vertikal aufzufächern. 
Nach justierung der Stromstärke wurde eine Beugungsstruktur sichtbar. Von dieser 
wurden die Werte der Maxima und Minima aufgenommen sowie der Untergrund, welcher 
links und rechts sichtbar wurde. Um die Intensität der Maxima höhere Ordnungen zu 
messen wurde die Stromstärke hochgeregelt, wobei das Maximum 0ter Ordnung in Sättigung 
ging. Danach wurde die Abszisse geeicht, wobei wir die Spaltbreite der 
Modenblende variierten und die Verschiebung mithilfe des Messfühlers am Monitor 
aufnahmen. Weiterhin haben wird das im Programm angezeigte Diagramm als .ccd Datei 
gespeichert, sodass wir dieses plotten lassen konnten. Der Plot sowie der zum 
Doppelspalt sind im Anhang zu finden.


\subsection{Beugungsstruktur des Doppelspaltes}

Der Einzelspalt wurde durch einen Doppelspalt ersetzt. Danach wurde der 
Doppelspalt "B" des Dia-Rähmchen verwendet und analog zu 1. die Lage der 
Maxima, Minima und Untergrund vermessen. Auch hier wurden die Daten innerhalb einer 
.ccd Datei gespeichert.

\subsection{Das Objektbild als Fouriersynthese}

Die Auswirkung der Manipulationen in der Fourierebene wurde untersucht. 
Durch Nachjustieren der Einstellungen und der 
Stromstärke sowie unter der Verwendung eines Graufilters wurde die 
Rechteckfunktion sichtbar. Danach wurde die Analysierspaltbreite verringert und 
die Änderung auf das Objektbild sowie das Beugungsbild beobachtet. Der Graufilter 
wurde entfernt um eine bessere Auflösung zu ermöglichen. Für die Bilder der 
ersten fünf Einstellungen wurde wieder die Lage der Maxima und Minima sowie des 
Untergrundes aufgenommen. Weiterhin wurde das für ein Bild hoher Wiggels gemacht, 
wobei auch die Brennweite $f$ und Bildweite $b$ mit einem Zollstock bestimmt wurde. 
Auch wurde die Spaltbreite dem Bild entnommen.

\subsection{Fourierbild des Doppelspaltes}

Das Bild des Doppelspaltes wurde betrachtet. Anhand dessen wurde die 
Breite und der Abstand der beiden Spalte vermessen. Auch hier wurde die 
Analysierspaltbreite verändert und die Auswirkung beobachtet. Dabei wurden 2 Fälle unterschieden:
\begin{enumerate}
    \item 2 Gaußprofile werden sichtbar
    \item Diese gehen in ein ebenes Plateu über
\end{enumerate}
Für diese Fälle wurde jeweils die Breite des Analysierspaltes aufgenommen.

\section{Auswertung}
\subsection{Quantitative Beobachtungen am Einzelspalt}
Die Messwerte zur Aufgabe 1 wurden aus dem Messprotokoll in uarrays gespeichert. Dabei handelt es sich um ein 
Objekt, welches aus dem Uncertainties Packet importiert wurde. Das Packet ist eine Erweiterung von Numpy und ermöglicht präzises Rechnen 
mit Fehlern, welche simultan in die Arrays als 2tes Argument übergeben werde.
Von den Daten aus Tabelle 3 wurde die Differenz berechnet und nach Division der Differenz der Spaltöffnung durch die Differenz der Pixel wurde 
der Umrechnungsfaktor von Millimeter pro Pixel berechnet. Dieser beträgt nach unserer Messung
\begin{align}
    m = (0.00292 \pm 0.00021) [\frac{mm}{px}]
\end{align}
Dann wurden die Werte der Abstände von Minimum zu Minimum über die Ordnungsanzahl 
geplottet sowie eine gerade Extrapoliert. Anhand der Ausgleichsgeraden wurden folgender Wert abgelesen:
\begin{align}
    a &= (0.39 \pm 0.04)[\frac{mm}{N}]
\end{align}
Danach wurden die Werte für die Maxima implementiert. Mithife der Ausgleichsgeraden wurden deren Ordnung bestimmt.
Die berechneten Werte sind:
\begin{align}
    n &= (0.65 \pm 0.1) &   \Delta &= 9\sigma\\
    n &= (2.39 \pm 0.3) &   \Delta &= 0.4\sigma\\
    n &= (3.4 \pm 0.5)  &   \Delta &= 0.2\sigma\\
    n &= (4.4 \pm 0.6)  &   \Delta &= 0.14\sigma\\
    n &= (5.4 \pm 0.7)  &   \Delta &= 0.12\sigma\\
\end{align}
Folglich weicht nur der erste Messwert von der zu erwartenden Ordnung signifikant ab.
Das kann unter anderem daran liegen, dass dort ein Messfehler (Fehlerhaftes Ablesen der Spaltbreite, ect.)
passiert sein kann. Das Diagramm ist im Anhang als Abbildung (\ref{fig:2}) zu finden.\\
Sodann musste die Spaltbreite berechnet werden. Dafür wurde Gleichung \ref{eq:19} angewendet.
\begin{align}
    m*\lambda &= b*\sin\alpha   &   \text{Kriterium für Minimum}\\
    b &= \frac{m*\lambda}{\sin\alpha}   &   m=2\\
    \sin\alpha &= a/f   &   \text{f beschreibt die Linsenbrennweite}\\
    b &= \frac{2\lambda f}{a}\\
    \Delta b &= \frac{\Delta a}{a} b
\end{align}
Dadurch ergab sich eine Spaltbreite $d$ von

\begin{align}
    d &= (0.26 \pm 0.06) [mm]
\end{align}
Danach wurden die Messwerte der Intensitäten übernommen. Davor wurde jedoch der Faktor der Skalierung berechnet, welcher sich einstellte 
als das Maximum 0ter Ordnung in Sättigung ging. Dafür wurden die Messwerte der Intensität der Maximums 
1ster Ordnung dividiert und die Intensität des Hauptmaximums damit multipliziert. Bevor die relativen Verhältnisse 
zueinander bestimmt wurden, wurde noch der Untergrund von den jeweiligen Messwerten subtrahiert. Sodann
wurde mit der Funktion welche im Skript steht die Figur des zu erwartenden Spaltes geplottet. Dieser ist als Abbildung 
(\ref{fig:4}) im Anhang zu finden.\\


Danach wurden die Intensitätsverhältnisse verglichen.
Diese Abweichungen zu den theoretischen Werten betragen:

\begin{align}
    v_1^l &= (41 \pm 1.8)*10^{-3}   &   v_1^r &= (41.6\pm 1.9)*10^{-3}\\
    v_2^l &= (14.1\pm 0.7)*10^{-3}  &   v_2^r &= (14.5\pm 0.7)*10^{-3}\\
    v_3^l &= (7.1\pm 0.3)*10^{-3}   &   v_3^r &= (8.3\pm 0.4)*10^{-3}\\
    v_4^l &= (4.5 \pm 0.4)*10^{-3}  &   v_4^r &= (5.4\pm 0.3)*10^{-3}\\
    v_5^l &= (3.05\pm 0.15)*10^{-3} &   v_5^r &= (3.68\pm 0.18)*10^{-3}\\
\end{align}
Wobei $l$ zu den Verhältnissen der linken Seite korrespondiert und $r$ daher rechts.
\begin{align}
    \sigma_1^l &= 4   &   \sigma_1^r &= 3\\
    \sigma_2^l &= 4  &   \sigma_2^r &= 3\\
    \sigma_3^l &= 4   &   \sigma_3^r &= 0.06\\
    \sigma_4^l &= 2.5  &   \sigma_4^r &= 1.6\\
    \sigma_5^l &= 2.2 &   \sigma_5^r &= 1.9\\
\end{align}
Anhand der Abweichungen zu den theoretischen Werten und den Differenzen untereinander kann man rückschließen, 
dass die Vermessung an der rechten Seite präziser erfolgte und es zu einer Fehlerquelle auf der linken Seite gekommen 
sein muss. Aus analoger Rechnung und gleichem Vorgehen muss man daher schlussfolgern, dass die Auslesung am Pc nicht genau genug erfolgte 
oder die Strahlen nicht den Kameramittelpunkt trafen. Letzteres ist sehr wahrscheinlich, da eine genaue Ausrichtung schwer zu realisieren ist 
mit bloßem Auge. Dennoch muss ein Teil der Fehler von der Auslesung der Daten gekommen sein. Weil die Daten auch gespeichert wurden, kann man auch die 
Messwerte plotten lassen. Dieser Plot ist Abbildung (\ref{fig:2})


\subsection{Beugungsstruktur des Doppelspaltes}
Zuerst wurden die Messwerte aus Aufgabe 4 genommen und geplottet (Abbildung \ref{fig:1}) Dabei handelt es sich um die Positionen, an welchen der 
Doppelspalt in der Fourierebene beginnt und aufhört. Anhand der Differenzen kann mann man die Spaltbreite $b$ und den 
Spaltabstand $g$ in Pixel bestimmen, sowie das Verhältnis $v=\frac{g}{d}$. Um die Breite in Meter ausdrücken zu können, 
muss man noch den Abstand der Linse zur Ebene berücksichtigen, weil aufgrund des Abstandes das Bild größer erscheint.
Anhand der Gleichung (\ref{eq:17}) und dem Abstand  $b=63.0\pm0.3$cm und $g=80mm$, kann man die Vergrößerung $v$ bestimmen.
Diese beträgt hier:
\begin{align*}
    v &= 7.88\pm0.04
\end{align*}
Wenn man also die Breite in Pixel kennt und die Umrechnung von Pixel in Meter sowie den Vergrößerungsfaktor, so kann man
die Breite berechnen. Es wurde von den senkrechten Stellen der Doppelspaltfigur gemessen. Also kann man aus der Differenz der äußeren Messpunkte die Spaltbreite berechnen und aus der Differenz der 
Mittelpunkte der Spalte auch den Spaltabstand:
\begin{align}
    b &= \frac{b^{pixel}14\mu m}{v}\\
    \Delta b &= \sqrt{(\frac{\Delta b^{pixel}}{b^{pixel}})^2+(\frac{\Delta v}{v})}b\\
    g &= \frac{d 14 \mu m}{v}\\
    \Delta g &= \sqrt{(\frac{\Delta d}{d})^2+(\frac{\Delta v}{v})^2}g
\end{align}
ergibt:
\begin{align*}
    b &= 0.212 \pm 0.009 [mm]\\
    g &= 0.512 \pm 0.009 [mm]
\end{align*}
Wenn man annimmt, dass die Spaltbreite des Doppelspaltes genauso groß ist wie die des Spaltes aus 1), so ergibt sich:
\begin{align*}
    b_1 &= 0.26\pm 0.06 [mm]\\
    b_2 &= 0.212 \pm 0.009\\
    \rightarrow \sigma &= 0.8 \rightarrow \text{Nicht signifikant} 
\end{align*}
Wobei hier die übliche Fehlerrechnung angewendet wurde. Weil jeglicher Vergleichswert fehlt, kann man nichts weiteres 
daraus schließen.\\
Sodann wurden die Verhältnisse der Intensitäten der Maxima mit den theoretisch zu erwartenden verglichen.
Weil jedoch unsere Gruppe nicht die Intensität des Maximums 0ter Ordnung aufgenommen hat, mussten die Intensitätsverhältnisse 
bezüglich des Maximums 1ster Ordnung veglichen werden. Die Werte für den theoretischen Doppelspalt wurden berechnet und sind in Abbildung (\ref{fig:5}) zu finden. Dann wurde statt 
auf den Wert des erstenMaximums 0ter Ordnung zu normieren, auf das erster Ordnung normiert. Die Werte aus dem Protokoll wurden wieder in 
uarrays gespeichert und dann analog zur 1) die Intensitäten korrigiert, indem der Untergrund abgezogen wurde. Dann wurde auch von jenen das Verhältnis 
berechnet.
\begin{align*}
    n_{theo}&=1  &   v&=1\\
    n_{theo}&=2  &   v&=0.11041792\\
    n_{theo}&=3  &   v&=0.05861479\\
    n_{theo}&=4  &   v&=0.05484013\\
    n_{theo}&=5  &   v&=0.00509329\\
    \\
    n_{exp}^{links}&=1   &   v&=1\\
    n_{exp}^{links}&=2   &   v&=0.130  \pm 0.004\\
    n_{exp}^{links}&=3   &   v&=0.0451 \pm 0.0018\\
    n_{exp}^{links}&=4   &   v&=0.0525 \pm 0.0019\\
    n_{exp}^{links}&=5   &   v&=0.0209 \pm 0.0014\\
    \\
    n_{exp}^{rechts}&=1  &   v&= 1\\
    n_{exp}^{rechts}&=2  &   v&= 0.118  \pm 0.003\\
    n_{exp}^{rechts}&=3  &   v&= 0.0487 \pm 0.0019\\
    n_{exp}^{rechts}&=4  &   v&= 0.0529 \pm 0.0018\\
    n_{exp}^{rechts}&=5  &   v&= 0.0258 \pm 0.0013\\
\end{align*}
\begin{align*}
    \sigma_{rechts}&=0   &   \sigma_links&=0\\
    \sigma_{rechts}&=2.5 &   \sigma_links&=6\\
    \sigma_{rechts}&=6   &   \sigma_links&=8\\
    \sigma_{rechts}&=1,1 &   \sigma_links&=1.3\\
    \sigma_{rechts}&=17  &   \sigma_links&=13\\
\end{align*}
Daraus folgt, dass bis auf den Messwert zum 4ten Maximum alle Verhältnisse signifikant vom theoretisch zu erwartenden Verhältnis abweichen.
Auch hier fällt anhand der Fehler auf, dass an der rechten Seite genauer gemessen wurde (bis auf Wert n=5).
Auch hier wurden die Messwerte gespeichert und im Programm geplottet (Abbildung \ref{fig:1}).
\subsection{Fouriersynthese des Beugungsbildes}
Weil hier die theoretisch zu erwartenden Bilder des Spaltes analysiert werden sollen, muss man zuerst diese berechnen.
Dafür wurden die Spaltfunktionen (Spalt, Doppelspalt) definiert, wie es im Skript steht. Dann wurde die Funktion $picture(func, b, dateiname, speichername, spaltdaten=None)$ definiert.
Dieser übergibt man die jeweilige Spaltfunktion, die Ordnung b, den Dateinamen welcher den Plottitel festlegt, den Speichernamen und optional die Messwerte, welche wichtig sind.
Die Funktion berechnet numerisch das Integral der übergebenen Spaltfunktion, plottet diese unter den festgesetzen Paramentern (Ordnung, Spaltbreite) und 
berechnet weiterhin die Position der Maxima sowie der Minima und zeichnet diese auch ein. Dann gibt sie die Position der Maxima und Minima 
sowie deren Intensitäten wieder. Wenn zusätzlich der Parameter $spaltdaten$ nicht leer ist, bedeutet dass, dass es eine ccd-Datei mit Messwerten dazu gibt.
Diese Datei ließt sie dann aus, verschiebt den Plot und skaliert ihn, sodass die Messwerte und die theoretischen Werte beide im Plot zu erkennen sind.
Diese Funktion ist sehr leistungsaufwendig und deshalb dauert es etwas, bis das Skript fertig ist. Die Plots sind im Anhang hinterlegt.\\
Dann wurde die Funktion für die ausgemessenen Ordnungszahlen (0,1,2,3,4,5,10) ausgeführt, sodass die theoretischen Werte gespeichert werden konnten.
Dasselbe wurde für den Doppelspalt gemacht, wobei hier keine Messswerte vorlagen, weshalb diese Plots auch als "Cherry-on-top" bezeichnet werden können.\\
Jedenfalls wurden die Messwerte der Positionen der Maxima und Minima sowie deren Intensitäten aus dem Protokoll eingegeben.
Mithilfe der $Distance$- und $DistanceError$-Methoden wurde der Abstand aller Extrema sowohl der der theoretischen als auch der experimentellen berechnet.
Die Werte wurde mit den theoretischen Verglichen und für die 10te Ordnung wurde eine Fehlerrechnung durchgeführt. Für die Distanz der Maxima ergib sich:
\begin{align*}
    n_1^{theo} &= 1 &   d_1^{theo}&=0[px]\\
    n_2^{theo} &= 2 &   d_1^{theo}&=63.3[px]\\
    n_3^{theo} &= 3 &   d_1^{theo}&=42.8[px]\\
    n_{10}^{theo} &= 10 &   d_1^{theo}&=12.8[px]\\
    \\
    n_1^{exp} &= 1 &   d_1^{exp}&=0[px]\\
    n_2^{exp} &= 2 &   d_1^{exp}&=67.0\pm1.4[px]\\
    n_3^{exp} &= 3 &   d_1^{exp}&=40.0 \pm 1.4 [px]\\
    n_{10}^{exp} &= 10 &   d_1^{exp}&=12.47\pm0.34[px]\\
\end{align*}
Und für die der Minima:
\begin{align*}
    n_1^{theo} &= 1 &   d_1^{theo}&=0[px]\\
    n_2^{theo} &= 2 &   d_1^{theo}&=0[px]\\
    n_3^{theo} &= 3 &   d_1^{theo}&=42.8[px]\\
    n_{10}^{theo} &= 10 &   d_1^{theo}&=12.8[px]\\
    \\
    n_1^{exp} &= 1 &   d_1^{exp}&=0[px]\\
    n_2^{exp} &= 2 &   d_1^{exp}&=0[px]\\
    n_3^{exp} &= 3 &   d_1^{exp}&=40.0 \pm 1.4 [px]\\
    n_{10}^{exp} &= 10 &   d_1^{exp}&=12.33\pm1.4[px]\\
\end{align*}
Die Abweichung der Distanzen der Extrema der Ordnung 10 zu den Theoretischen ergab sich zu:
\begin{align*}
    \sigma &= 1
\end{align*}
Betrachtet man hier also die Messwerte und die sehr geringe Abweichung, so kann man daraus schließen, dass sehr genau gemessen wurde. Das hier im Gegenteil zu der Aufgabe 2 die Werte genauer sind, kann daran liegen, dass
dieser Teil eine Woche später gemacht wurde und währenddessen an der Aparatur hantiert wurde.\\
Diese Aussage wird auch von den Plots unterstrichen (Abbildung \ref{fig:6}-\ref{fig:16}), da die Gemeinsamkeiten sofort ins Auge fallen.\\
Teil b) erfortete ein Vergleich der Intensitäten. Hierfür wurden die Werte der Intensitäten aus dem Protokoll übernommen und wiederum der Untergrund abgezogen. 
Danach wurden alle Intensitäten durch die des Maximums 0ter Ordnung dividiert. Das ergab für die folgenden Ordnungen die Verhältnisse:
\begin{align*}
    n &= 1\\
    max_1 &= 0.846 \pm 0.019\\
    max_2 &= 0.847 \pm 0.021\\
    n &= 2\\
    max_1 &= 0.828 \pm 0.018\\
    max_2 &= 0.820 \pm 0.018\\
    max_3 &= 0.998 \pm 0.022
\end{align*}
Daraus lässt sich schließen, dass das das Bild erster Ordnung sehr symmetrisch verteilt ist, doch für höhere Beugungsordnungen 
das Maximum an der rechten Seite $(max_3)$ immer weiter aus dem Gleichgewicht gerät und mehr skaliert.\\
Schließt man den Spalt fast, so nimmt die Intensität in der Bildmitte zu. Das liegt daran, dass der Analysierspalt selber als Einzelspalt fungiert mit eigener Spaltbreite und ein neues Interfernzbild entseht.


\subsection{Fourierbild des Doppelspaltes}
Das Verhältnis von Spaltabstand zu Spaltbreite berechnet sich aus:
\begin{align}
    v &= \frac{g}{d}\\
    \Delta v &= \sqrt{(\frac{\Delta g}{g})^2+(\frac{\Delta d}{d})^2}*v
\end{align}
Dies wurde schon im Teil 2 berechnet. Aus dem Kehrwert kann man die Minimale Ordnung $n$ berechnen, sodass 2 Gaußkurven noch erkennbar sind:
\begin{align}
    n_{min} &= \frac{1}{v}\\
    \Delta n_{min} &= \frac{\Delta v}{v^2}
\end{align}
Hierfür wurde auch ein Plot erstellt (Abbildung \ref{fig:17})
\begin{align*}
    n_{min} &= 0.413 \pm 0.018
\end{align*}
Dafür wurde auch ein weitere theoretischer Doppelspalt geplottet. Danach wurde der Spaltabstand und die Spaltbreite normiert ($d=1$) und durch eine 
$while$-Loop wurde numerisch berechnet, ab welchem $k_{y,n}$ die Gaußprofile in ein Plateu übergehen. Diese Rechnung dauert auch sehr lange, weshalb für ein 
späteres Kompilieren der Wert deklariert wurde, sodass die Rechnung übersprungen werden konnte.
\begin{align*}
    k_{y,n} &= 1.7341772623416603[\frac{1}{mm}]
\end{align*}
Weiterhin wurden Vergleichswerte mit größeren $k$ erstellt und diese geplottet (Abbildung \ref{fig:18}). Anhand des Plottes lässt sich der Übergang verdeutlichen und die 
These validieren. Dann wurden noch die für diesen Versuch relevanten $k_y$ berechnet. Diese wurden mit (\ref{eq:8}) berechnet:
\begin{align}
    k_{y,theo} &= 2\pi n / b\\
    \Delta k_{y,theo} &= \frac{2\pi n \Delta b }{b^2}\\
    k_{y,exp} &= \frac{2\pi d}{\lambda f}\\
    \frac{\Delta k}{k} &=\frac{\Delta d}{d}
\end{align}
Wobei für $n$ jeweils der Minimalwert und einmal $1$ eingesetzt wurde.
Das ergab folgende Ergebnisse mit folgenden Abweichungen:
\begin{align*}
    k_a^{theo} &= 29.7 \pm 1.3 [\frac{1}{mm}]\\
    k_a^{exp} &= 59.4 \pm 2.5 [\frac{1}{mm}]\\
    \sigma_a &= 11\\
    k_b^{theo} &= 12.27 \pm 0.22 [\frac{1}{mm}]\\
    k_b^{exp} &= 7.4 \pm 2.5 [\frac{1}{mm}]\\
    \sigma_b &= 2
\end{align*} 
Das bedeutet, dass die erste Messung sehr ungenau verlaufen ist und die zweite deutlich genauer. 
\section{Diskussion}
Zuerst wurde der Einzelspalt untersucht. Der Abstand der Minima über die Ordnungszahl wurde geplottet und dann anhand der Ausgleichsgeraden der Fehler der Maxima berechnet.
Lediglich die Ordnung des ersten Maxima weicht signifikant vom theoretischen ab, alle weiteren besitzen einen Fehler von kleiner als 1 und weisen daher auf eine sehr genaue Messung und
Nähe zur Theorie hin. Dann wurde die Spaltbreite als
\begin{align*}
    d &= (0.26 \pm 0.06)[mm]
\end{align*}
bestimmt. Das bedeutet eine Unsicherheit von $24\%$. Sodann wurde anhand der Verhältnisse der Intensitäten und der der Literaturwerte geschlossen, dass die rechte Seite genauer vermessen wurde.
Dies kann unter anderem daran liegen, dass die ccd-Kamera nicht vollständig symmetrisch ausgeleuchtet wurde und daher jene Seite genauer als die Andere wiedergegeben wurde. Andere Fehlerquellen darf man hierbei nicht 
berücksichtigen, da weiterhin gleiche Bedingungen für die Messungen galten und man sonst gleichzeitig beide kritisiert. Dazu später mehr.\\
Dann wurde der Doppelspalt untersucht. Die Spaltbreite und der Spaltabstand ergab sich zu
\begin{align*}
    b &= 0.212 \pm 0.009 [mm]\\
    g &= 0.512 \pm 0.009 [mm]
\end{align*}
Das bedeutet Unsicherheiten von weniger als $5\%$. Folglich ist diese Methode der Messung sehr genau. 
Daraufhin wurden die Intensitäten verglichen. Die Fehler fielen allesamt signifikant aus. Das bedeutet dass im Gegensatz 
zum Einzelspalt hier sehr ungenau gemessen wurde.\\
Im dritten Teil wurden die Fourierbilder untersucht und die Abstände der Extrema zu den theoretischen Abständen verglichen. Hier wurde wiederum sehr genau vermessen was 
die Abweichung von 1 Sigma beweist.\\
Schließlich wurde noch der Übergang der Gaußprofile in das Plateu am Beispiel vom Doppelspalt untersucht und die 
Werte $k_y$ bestimmt:
\begin{align*}
    k_a^{theo} &= 29.7 \pm 1.3 [\frac{1}{mm}]\\
    k_a^{exp} &= 59.4 \pm 2.5 [\frac{1}{mm}]\\
    \sigma_a &= 11\\
    k_b^{theo} &= 12.27 \pm 0.22 [\frac{1}{mm}]\\
    k_b^{exp} &= 7.4 \pm 2.5 [\frac{1}{mm}]\\
    \sigma_b &= 2
\end{align*} 
Die erste Unsicherheit ist fällt so groß aus, weil es sehr schwer war zu determinieren, ab wann Fall a) anfängt. In der Theorie ist dies zwar eindeutig, 
doch beim Betrachten des Bildschirmes unter vorsichtigem Variieren der Analysierspaltbreite war es schwer zu entscheiden, ob auf dem betrachteten Bild schon die Gaußkurven sind 
oder noch leichte Profile der anderen Beugungsordnungen. Der zweite Wert ist viel genauer, weil es dort eindeutiger war, ab wann die Kurven vollständig verschwammen.\\
Dieser Versuch ist sehr fehleranfällig aufgrund der Messaparatur: Schon geringe Vibrationen und Unterschiede der Helligkeit übten gravierenden Einfluss auf die Messwerte ein. Berücksichtig man zusäzlich, dass 
man mit einer anderen Gruppe im selben Raum misst, scheint diese statistische Fehlerquelle nicht eliminierbar zu sein. Weiterhin kommt es zu einem systematischen Fehler, wenn die Kamera nicht vollständig ausgeleutet ist. 
Dieser Fehler wurde möglichst gut versucht zu reduzieren, indem man die Kamera nach Augenmaß möglichst zentral plazierte und auf Hilfsmittel wie eine Zylinderlinse zurückgriff, doch eine ganze Vermeidung schien unmöglich.\\
Aufgrund dessen ist eine Verbesserung des Versuches schwer realisierbar. Wenn man noch bedenkt, dass begrenzt Zeit zur Verfügung steht und der Versuch schon als 2-Tages Versuch sehr ausgelastet ist, kann man nicht argumentieren, dass die Anzahl an 
unabhängigen Messungen erhöht werden muss. Zusätzlich lässt sich die menschliche Fehlerquelle nicht entfernen.
\section{Anhang}
\subsection{Messprotokoll}
\includepdf[pages=-]{../Resources/Fourieroptik.pdf}
\includepdf[pages=-]{../Resources/protokoll2.pdf}
\subsection{Plots}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Results/Diagramm1a.pdf}
    \caption{Messwerte des Doppelspaltes geplottet}
    \label{fig:1}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Results/Diagramm1b.pdf}
    \caption{Messwerte des Spaltes geplottet}
    \label{fig:2}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Results/Diagramm2.pdf}
    \caption{Abstand der Minima über die Ordnung}
    \label{fig:3}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Results/Diagramm3.pdf}
    \caption{Beugung am theoretischen Spalt}
    \label{fig:4}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Results/Diagramm4.pdf}
    \caption{Beugung am Spalt und Doppelspalt}
    \label{fig:5}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm5.pdf}
    \caption{Einzelspalt, 1 Maxima}
    \label{fig:6}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm6.pdf}
    \caption{Einzelspalt, 2 Maxima}
    \label{fig:7}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm7.pdf}
    \caption{Einzelspalt, 3 Maxima}
    \label{fig:8}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm8.pdf}
    \caption{Einzelspalt, 4 Maxima}
    \label{fig:9}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm9.pdf}
    \caption{Einzelspalt, 5 Maxima}
    \label{fig:10}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm10.pdf}
    \caption{Einzelspalt, 10 Maxima}
    \label{fig:11}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm11.pdf}
    \caption{Doppelspalt, 1 Maxima auf jeder Seite}
    \label{fig:12}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm12.pdf}
    \caption{Doppelspalt, 2 Maxima auf jeder Seite}
    \label{fig:13}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm13.pdf}
    \caption{Doppelspalt, 3 Maxima auf jeder Seite}
    \label{fig:14}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm14.pdf}
    \caption{Doppelspalt, 4 Maxima auf jeder Seite}
    \label{fig:14}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm15.pdf}
    \caption{Doppelspalt, 5 Maxima auf jeder Seite}
    \label{fig:15}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm16.pdf}
    \caption{Doppelspalt, 15 Maxima auf jeder Seite}
    \label{fig:16}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/V233Diagramm17.pdf}
    \caption{Doppelspalt für kritisches n}
    \label{fig:17}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{../Tests/Diagramm19.pdf}
    \caption{Übergang von Gauß zu Plateu}
    \label{fig:18}
\end{figure}


\end{document}