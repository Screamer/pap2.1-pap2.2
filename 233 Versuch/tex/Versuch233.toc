\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{}%
\contentsline {section}{\numberline {2}Aufbau und Durchführung}{1}{}%
\contentsline {section}{\numberline {3}Theorie}{2}{}%
\contentsline {section}{\numberline {4}Messprotokoll}{4}{}%
\contentsline {subsection}{\numberline {4.1}Quantitative Beobachtungen am Einzelspalt}{4}{}%
\contentsline {subsection}{\numberline {4.2}Beugungsstruktur des Doppelspaltes}{4}{}%
\contentsline {subsection}{\numberline {4.3}Das Objektbild als Fouriersynthese}{5}{}%
\contentsline {subsection}{\numberline {4.4}Fourierbild des Doppelspaltes}{5}{}%
\contentsline {section}{\numberline {5}Auswertung}{5}{}%
\contentsline {subsection}{\numberline {5.1}Quantitative Beobachtungen am Einzelspalt}{5}{}%
\contentsline {subsection}{\numberline {5.2}Beugungsstruktur des Doppelspaltes}{7}{}%
\contentsline {subsection}{\numberline {5.3}Fouriersynthese des Beugungsbildes}{9}{}%
\contentsline {subsection}{\numberline {5.4}Fourierbild des Doppelspaltes}{12}{}%
\contentsline {section}{\numberline {6}Diskussion}{13}{}%
\contentsline {section}{\numberline {7}Anhang}{14}{}%
\contentsline {subsection}{\numberline {7.1}Messprotokoll}{14}{}%
\contentsline {subsection}{\numberline {7.2}Plots}{25}{}%
