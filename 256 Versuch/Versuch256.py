import numpy as np
import uncertainties as unc
import matplotlib.pyplot as plt
import scipy as sp
from uncertainties import unumpy as unp
from scipy import constants as const
from scipy.optimize import curve_fit

plt.style.use('classic')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['errorbar.capsize'] = 2


def moseley(z, sqrt_e_r, sig12):
    return sqrt_e_r * (z - sig12) * np.sqrt(1 / pow(n1, 2) - 1 / pow(n2, 2))


def plot_data(arg):
    plt.clf()
    if arg == 1:
        plt.errorbar(Z, np.sqrt(unp.nominal_values(K_a)), xerr=0, yerr=yerr, linestyle=' ', marker='.', color='darkred',
                     label='Messwerte mit Fehler')
        plt.plot(Z, moseley(Z, *popt), color='darkblue', linestyle='-', linewidth=0.6, label='Ausgleichsgerade')
        plt.grid('dotted')
        plt.title(r'Diagramm 1: $\sqrt{E_\alpha}$ als Funktion der Kernladungszahl mit Fitkurve', size=15)
        plt.ylabel(r'$\sqrt{E_\alpha}/\sqrt{keV}$')
        plt.xlabel('Kernladungszahl Z')
        plt.legend(frameon=True, fontsize='large', borderpad=1, borderaxespad=1, loc='best')
        plt.tight_layout()
        plt.savefig('Diagramm1.pdf', format='PDF')
        plt.show()
    if arg == 2:
        plt.errorbar(Z, np.sqrt(unp.nominal_values(K_b)), xerr=0, yerr=yerr2, linestyle=' ', marker='.',
                     color='darkred', label='Messwerte mit Fehler')
        plt.plot(Z, moseley(Z, *popt2), color='darkblue', linestyle='-', linewidth=0.6, label='Ausgleichsgerade')
        plt.grid('dotted')
        plt.title(r'Diagramm 2: $\sqrt{E_\beta}$ als Funktion der Kernladungszahl mit Fitkurve', size=15)
        plt.ylabel(r'$\sqrt{E_\beta}/\sqrt{keV}$')
        plt.xlabel('Kernladungszahl Z')
        plt.legend(frameon=True, fontsize='large', borderpad=1, borderaxespad=1, loc='best')
        plt.tight_layout()
        plt.savefig('Diagramm2.pdf', format='PDF')
        plt.show()


n1 = 1
n2 = 2
# Reihenfolge (Fe,mo,Zn,Cu,Ni,Zr,Ti,Ag)
Z = np.array([26, 42, 30, 29, 28, 40, 22, 47])
K_a = unp.uarray([6.41, 17.47, 8.68, 8.09, 7.5, 15.8, 4.5, 21.9], [0.16, 0.17, 0.16, 0.16, 0.16, 0.17, 0.16, 0.22])
K_b = unp.uarray([7.08, 19.58, 9.61, 8.95, 8.3, 17.67, 5.05, 25.01], [0.16, 0.17, 0.18, 0.16, 0.2, 0.19, 0.1, 0.11])
# Berechnung von E_r
E_r = const.speed_of_light * const.h * const.Rydberg / const.e
# Fehlerrechnung gemäß Auswertung
yerr = 1 / (2 * np.sqrt(unp.nominal_values(K_a))) * unp.std_devs(K_a)
yerr2 = 1 / (2 * np.sqrt(unp.nominal_values(K_b))) * unp.std_devs(K_b)

popt, pcov = curve_fit(moseley, Z, np.sqrt(unp.nominal_values(K_a)), sigma=yerr)
n2 = 3
popt2, pcov2 = curve_fit(moseley, Z, np.sqrt(unp.nominal_values(K_b)), sigma=yerr2)

E_R_Alpha = unc.ufloat(popt[0] ** 2 * const.kilo, 2 * popt[0] * pcov[0, 0] ** 0.5 * const.kilo)
sigma = np.abs(unp.nominal_values(E_R_Alpha) - E_r) / unp.std_devs(E_R_Alpha)
E_R_Beta = unc.ufloat(popt2[0] ** 2 * const.kilo, 2 * popt2[0] * pcov2[0, 0] ** 0.5 * const.kilo)
sigma2 = np.abs(unp.nominal_values(E_R_Beta) - E_r) / unp.std_devs(E_R_Beta)

sigma12 = unc.ufloat(popt[1], pcov[1, 1] ** 0.5)
sigma12_beta = unc.ufloat(popt2[1], pcov2[1, 1] ** 0.5)
print('Der Energiewert beträgt: ' + str(E_R_Alpha))
print('Die Abweichung zum Literaturwert beträgt: ' + str(sigma))
print('Sigma12 beträgt: ' + str(sigma12))

print('Der Energiewert für beta ergibt: ' + str(E_R_Beta))
print('Die Abweichung zum Literaturwert ist: ' + str(sigma2))
print('Sigma 12 beträgt für beta: ' + str(sigma12_beta))
